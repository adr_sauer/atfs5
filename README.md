# README #

### Automatic Tank Filling System ###

* Quick summary

This is a demonstration version of the program I wrote for a system controlling filling of a process tank with medium and water.
It lacks a few features of the original and most parts of the code are different. 
The purpose of this code repository is to present my skills in C# and .NET Micro Framework programming.

* Version 5.*

### Technologies used ###

* .NET Micro Framework 

	.NET Framework platform for resource-constrained devices

	Files where used: Whole project

* DPWS (Devices Profile for Web Services)

	Web service providing mediums data.

	Files where used: MediumsList.cs, service.cs, serviceClientProxy.cs

* Serialization

	NETMF binary serialization of backup mediums data file.

	Files where used: MediumsListNETMFBinarySerializedFile.cs

* XML

	File storing pouring profiles data.

	Files where used: PouringProfilesListXmlFile.cs

* XSD

	File in documentation describing the XML schema to which the file storing pouring profiles data has to conform.

	Files where used: PouringProfilesListData.xsd
* Unit testing

	Simple unit tests implemented using MFUnit library (https://github.com/ducas/MFUnit). 
	
	Files where used: All files in UnitTests folder.
	
* Implementation according to specification

	Modbus data structures were implemented according to MODBUS Protocol Specification V1.1b3. http://modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf

	Files where used: All Modbus data structures files.

### Contact ###

* Adrian Sauer

My LinkedIn profile: https://pl.linkedin.com/in/adrian-sauer-692b86138