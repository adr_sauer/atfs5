using System;
using Microsoft.SPOT;
using System.IO;

namespace ATFS5.src.OperationProgressTrackerLib
{
    /// <summary>
    /// Represents a NET Micro Framework binary serialization file containing operation progress data.
    /// </summary>
    class OperationProgressTrackNETMFBinarySerializedFile : OperationProgressTrackFile
    {
        /// <summary>
        /// Initializes a new instance of the OperationProgressTrackNETMFBinarySerializedFile class using the specified operation progress file pathname.
        /// </summary>
        /// <param name="filePathname"></param>
        public OperationProgressTrackNETMFBinarySerializedFile(string filePathname)
            : base(filePathname)
        {
        }

        /// <summary>
        /// Reads operation progress data from NET Micro Framework binary serialization file.
        /// </summary>
        /// <returns>An OperationProgressTrack object.</returns>
        public override OperationProgressTrack ReadFromFile()
        {            
            byte[] operationProgressTrackByteData = null;
            using (var operationProgressTrackFileStream = File.Open(_filePathname, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                if (operationProgressTrackFileStream.Length > Int32.MaxValue)
                    throw new IOException("Mediums list file too long.");
                operationProgressTrackByteData = new byte[(int)operationProgressTrackFileStream.Length];
                operationProgressTrackFileStream.Read(operationProgressTrackByteData, 0, operationProgressTrackByteData.Length);
            }
            return Reflection.Deserialize(operationProgressTrackByteData, typeof(OperationProgressTrack)) as OperationProgressTrack;
        }

        /// <summary>
        /// Saves operation progress data to NET Micro Framework binary serialization file.
        /// </summary>
        /// <param name="progressTrack">An OperationProgressTrack object to save.</param>
        public override void SaveToFile(OperationProgressTrack progressTrack)
        {            
            byte[] operationProgressTrackByteData = Reflection.Serialize(progressTrack, typeof(OperationProgressTrack));

            using (var operationProgressTrackFileStream = File.Open(_filePathname, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                operationProgressTrackFileStream.Write(operationProgressTrackByteData, 0, operationProgressTrackByteData.Length);
                operationProgressTrackFileStream.Flush();
            }
        }
    }
}
