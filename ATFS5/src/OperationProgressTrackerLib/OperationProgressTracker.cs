using System;
using Microsoft.SPOT;
using System.IO;

namespace ATFS5.src.OperationProgressTrackerLib
{
    /// <summary>
    /// Represents a tracker of operation progress which allows to resume it if it is interrupted.
    /// </summary>
    public class OperationProgressTracker
    {
        private OperationProgressTrackFile _trackFile;
        private OperationProgressTrack _operationProgressTrack = null;
        private object _lockObject;

        /// <summary>
        ///<b>True</b> if infinished operation progress track exist. <b>False</b> otherwise.
        /// </summary>
        public bool TrackExists
        {
            get
            {
                lock (_lockObject)
                    return !(_operationProgressTrack == null);
            }
        }

        /// <summary>
        /// Unfinished operation progress track.
        /// </summary>
        public OperationProgressTrack ProgressTrack
        {
            get
            {
                lock (_lockObject)
                    return (OperationProgressTrack)_operationProgressTrack.Clone();
            }
        }

        /// <summary>
        /// Initializes a new instance of the OperationProgressTracker class using the specified unfinished operation progress track data file pathname.
        /// </summary>
        /// <param name="filePathname"></param>
        public OperationProgressTracker(string filePathname)
        {
            _lockObject = new object();
            _trackFile = new OperationProgressTrackNETMFBinarySerializedFile(filePathname);
            try
            {
                if(File.Exists(filePathname))
                    _operationProgressTrack = _trackFile.ReadFromFile();
            }
            catch 
            {
                //Assume exception was caused by file not existing.
                _operationProgressTrack = null;
            }

        }

        /// <summary>
        /// Saves a snapshot of operation progress.
        /// </summary>
        /// <param name="track">Operation progress data.</param>
        public void SaveProgress(OperationProgressTrack track)
        {
            lock (_lockObject)
            {
                _operationProgressTrack = (OperationProgressTrack)track.Clone();
                _trackFile.SaveToFile(_operationProgressTrack);
            }
        }

        /// <summary>
        /// Deletes infinished operation progress track if it exists.
        /// </summary>
        public void DeleteTrack()
        {
            lock (_lockObject)
            {
                if (_operationProgressTrack != null)
                {
                    _trackFile.DeleteFile();
                    _operationProgressTrack = null;
                }
            }
        }

    }
}
