using System;
using Microsoft.SPOT;
using System.IO;

namespace ATFS5.src.OperationProgressTrackerLib
{
    /// <summary>
    /// Represents an abstract base class for classes representing various file formats containing operation progress data.
    /// </summary>
    abstract class OperationProgressTrackFile
    {
        /// <summary>
        /// Pathname to file containing operation progress data.
        /// </summary>
        protected string _filePathname;


        /// <summary>
        /// Initializes a new instance of the OperationProgressTrackFile class using the specified operation progress file pathname.
        /// </summary>
        /// <param name="filePathname">Pathname to file containing operation progress data.</param>
        public OperationProgressTrackFile(string filePathname)
        {
            _filePathname = filePathname;
        }

        /// <summary>
        /// Reads operation progress data from file.
        /// </summary>
        /// <returns>An OperationProgressTrack object.</returns>
        public abstract OperationProgressTrack ReadFromFile();

        /// <summary>
        /// Saves operation progress data to file.
        /// </summary>
        /// <param name="progressTrack">An OperationProgressTrack object to save.</param>
        public abstract void SaveToFile(OperationProgressTrack progressTrack);

        /// <summary>
        /// Deletes operation progress data file.
        /// </summary>
        public void DeleteFile()
        {
            File.Delete(_filePathname);
        }
    }
}
