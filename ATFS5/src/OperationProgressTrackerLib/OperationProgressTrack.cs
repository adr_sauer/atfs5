using System;
using Microsoft.SPOT;
using ATFS5.src.PouringProfilesListLib;

namespace ATFS5.src.OperationProgressTrackerLib
{
    /// <summary>
    /// Represents an operation progress snapshot.
    /// </summary>
    [Serializable]
    public class OperationProgressTrack : ICloneable
    {
        
        private int _waterMassToPourInKg;
        private int _mediumMassToPourInKg;
        private int _waterMassPouredInKg;
        private int _mediumMassPouredInKg;        
        private int _timeToMixInSeconds;
        private int _timeMixedInSeconds;

        private int _mediumId;
        private uint _seriesId;
        private uint _operationId;
        private OperationOrderInSeries _operationOrderInSeries;
        private int _productId;

        /// <summary>
        /// Gets or sets mass of water in kilograms that was supposed to have been poured.
        /// </summary>
        public int WaterMassToPourKg
        {
            get
            {
                return _waterMassToPourInKg;
            }
            set
            {
                if (value + _mediumMassToPourInKg > PouringProfile.MaxTotalMassKg)
                    throw new ArgumentException("Total mass above allowed.");
                if (value < 0)
                    throw new ArgumentException("Water mass must not be negative");
                _waterMassToPourInKg = value;
            }
        }

        /// <summary>
        /// Gets or sets mass of medium in kilograms that was supposed to have been poured.
        /// </summary>
        public int MediumMassToPourKg
        {
            get
            {
                return _mediumMassToPourInKg;
            }
            set
            {
                if (value + _waterMassToPourInKg > PouringProfile.MaxTotalMassKg)
                    throw new ArgumentException("Total mass above allowed.");
                if (value < 0)
                    throw new ArgumentException("Medium mass must not be negative");
                _mediumMassToPourInKg = value;
            }
        }

        /// <summary>
        /// Gets or sets mass of water in kilograms that was poured.
        /// </summary>
        public int WaterMassPouredKg
        {
            get
            {
                return _waterMassPouredInKg;
            }
            set
            {
                if (value > _waterMassToPourInKg)
                    _waterMassPouredInKg = _waterMassToPourInKg;
                if (value < 0)
                    throw new ArgumentException("Water mass must not be negative");
                _waterMassPouredInKg = value; 
            }
        }

        /// <summary>
        /// Gets or sets mass of medium in kilograms that was poured.
        /// </summary>
        public int MediumMassPouredKg
        {
            get
            {
                return _mediumMassPouredInKg;
            }
            set
            {
                if (value > _mediumMassToPourInKg)
                    _mediumMassPouredInKg = _mediumMassToPourInKg;
                if (value < 0)
                    throw new ArgumentException("Medium mass must not be negative");
                _mediumMassPouredInKg = value; 
            }
        }

        /// <summary>
        /// Gets or sets time that mixing was supposed to elapse in seconds.
        /// </summary>
        public int TimeToMixInSeconds
        {
            get
            {
                return _timeToMixInSeconds;
            }
            set
            {
                if (value > PouringProfile.MaxMixingTimeInSeconds)
                    throw new ArgumentException("Mixing time above maximum allowed.");
                if (value < 0)
                    throw new ArgumentException("Mixing time must not be negative.");
                _timeToMixInSeconds = value;
            }
        }

        /// <summary>
        /// Gets or sets time mixing elapsed in seconds.
        /// </summary>
        public int TimeMixedInSeconds
        {
            get
            {
                return _timeMixedInSeconds;
            }
            set
            {
                if (value > _timeToMixInSeconds)
                    _timeMixedInSeconds = _timeToMixInSeconds;
                if (value < 0)
                    throw new ArgumentException("Mixing time must not be negative.");
                _timeMixedInSeconds = value;
            }
        }

        /// <summary>
        /// Gets or sets ID of medium that was supposed to have been poured.
        /// </summary>
        public int MediumId
        {
            get
            {
                return _mediumId;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("Medium ID must be a positive integer.");
                _mediumId = value;
            }
        }

        /// <summary>
        /// Gets or sets operation series ID.
        /// </summary>
        public uint SeriesId
        {
            get
            {
                return _seriesId;
            }
            set
            {                
                _seriesId = value;
            }
        }

        /// <summary>
        /// Gets or sets operation ID.
        /// </summary>
        public uint OperationId
        {
            get
            {
                return _operationId;
            }
            set
            {
                _operationId = value;
            }
        }

        /// <summary>
        /// Gets or sets operation position in operations series.
        /// </summary>
        public OperationOrderInSeries OperationOrderInSeriesTrack
        {
            get
            {
                return _operationOrderInSeries;
            }
            set
            {
                _operationOrderInSeries = value;
            }
        }

        /// <summary>
        /// Gets or sets ID of product that was supposed to have been produced.
        /// </summary>
        public int ProductId
        {
            get
            {
                return _productId;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("Product ID must be a positive integer.");
                _productId = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the OperationProgressTrack class.
        /// </summary>
        public OperationProgressTrack()
        {
            _waterMassToPourInKg = 0;
            _mediumMassToPourInKg = 0;
            _waterMassPouredInKg = 0;
            _mediumMassPouredInKg = 0;
            _timeToMixInSeconds = 0;
            _timeMixedInSeconds = 0;
            _mediumId = 0;
            _seriesId = 0;
            _operationId = 0;
            _operationOrderInSeries = OperationOrderInSeries.Begin;
            _productId = 0;
        }

        /// <summary>
        /// Initializes a new instance of the OperationProgressTrack class using the specified operation progress data.
        /// </summary>
        /// <param name="waterMassToPourInKg">Mass of water in kilograms that was supposed to have been poured.</param>
        /// <param name="mediumMassToPourInKg">Mass of medium in kilograms that was supposed to have been poured.</param>
        /// <param name="waterMassPouredInKg">Mass of water in kilograms that was poured.</param>
        /// <param name="mediumMassPouredInKg">Mass of medium in kilograms that was poured.</param>
        /// <param name="timeToMixInSeconds">Time that mixing was supposed to elapse in seconds.</param>
        /// <param name="timeMixedInSeconds">Time mixing elapsed in seconds.</param>
        /// <param name="mediumId">ID of medium that was supposed to have been poured.</param>
        /// <param name="seriesId">Operation series ID.</param>
        /// <param name="operationId">Operation ID.</param>
        /// <param name="operationOrderInSeries">Operation position in operations series.</param>
        /// <param name="productId">ID of product that was supposed to have been produced.</param>
        public OperationProgressTrack(int waterMassToPourInKg, int mediumMassToPourInKg, int waterMassPouredInKg,
            int mediumMassPouredInKg, int timeToMixInSeconds, int timeMixedInSeconds, int mediumId,uint seriesId,
            uint operationId, OperationOrderInSeries operationOrderInSeries, int productId)
        {
            WaterMassToPourKg = waterMassToPourInKg;
            MediumMassToPourKg = mediumMassToPourInKg;
            WaterMassPouredKg = waterMassPouredInKg;
            MediumMassPouredKg = mediumMassPouredInKg;
            TimeToMixInSeconds = timeToMixInSeconds;
            TimeMixedInSeconds = timeMixedInSeconds;
            MediumId = mediumId;
            SeriesId = seriesId;
            OperationId = operationId;
            OperationOrderInSeriesTrack = operationOrderInSeries;
            ProductId = productId;
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            return new OperationProgressTrack(_waterMassToPourInKg, _mediumMassToPourInKg, _waterMassPouredInKg,
                _mediumMassPouredInKg, _timeToMixInSeconds, _timeMixedInSeconds, _mediumId,
                _seriesId,_operationId,_operationOrderInSeries,_productId);
        }
    }
}
