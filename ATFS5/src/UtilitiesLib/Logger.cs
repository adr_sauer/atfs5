using System;
using System.IO;
using System.Text;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using NetMf.CommonExtensions;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Provides general interface for logger.
    /// </summary>
    public abstract class Logger : IDisposable
    {
        /// <summary>
        /// Logs information.
        /// </summary>
        /// <param name="information">Information to log.</param>
        public abstract void LogInformation(string information);

        /// <summary>
        /// Logs exception occurence.
        /// </summary>
        /// <param name="e">Exception to log.</param>
        public abstract void LogException(Exception e);

        /// <summary>
        /// Logs error message.
        /// </summary>
        /// <param name="errorMessage">Error message to log.</param>
        public abstract void LogError(string errorMessage);

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        protected abstract void Dispose(bool disposing);

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
