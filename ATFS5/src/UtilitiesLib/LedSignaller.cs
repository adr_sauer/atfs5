using System;
using System.Threading;
using Microsoft.SPOT.Hardware;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Class provides LED blinking functionality to signal errors which can't be logged. Class implements Singleton pattern.
    /// Class is thread-safe.
    /// </summary>
    public class LedSignaller : IDisposable
    {
        /// <summary>
        /// 3 blinks signalize SD card error.
        /// </summary>
        public const int CardErrorTimesToBlink = 3;

        /// <summary>
        /// 4 blinks signalize Logger error.
        /// </summary>
        public const int LoggerErrorTimesToBlink = 4;

        private const int _ledBlinkTime = 300;        
        private const int _waitBetweenBlinks = 2000;

        private OutputPort _ledOutput=null;
        private static LedSignaller _ledSignallerSingleInstance = null;
        private static object _ledSignallerSingleInstanceLockObject;
        private bool _disposed = false;
        private object _lockObject;

        static LedSignaller()
        {
            _ledSignallerSingleInstanceLockObject = new object();
        }

        private LedSignaller(Cpu.Pin ledpin)
        {
            _lockObject = new object();
            _disposed = false;
            _ledOutput = new OutputPort(ledpin, false);
        }

        /// <summary>
        /// Returns the single and only instance of LedSignaller.
        /// </summary>
        /// <param name="ledPin">Cpu pin to which led is connected.</param>
        /// <returns>Single instance of LedSignaller.</returns>
        public static LedSignaller ConstructSingleLEDSignaller(Cpu.Pin ledPin)
        {
            lock (_ledSignallerSingleInstanceLockObject)
            {
                if (_ledSignallerSingleInstance == null)
                    _ledSignallerSingleInstance = new LedSignaller(ledPin);

                return _ledSignallerSingleInstance;
            }
        }
       
        /// <summary>
        /// Blinks board LED given number of times in one or two cycles.
        /// </summary>
        /// <param name="times">Number of times LED is going to blink.</param>
        /// <param name="repeat">If <b>True</b> repeat blinking cycle second time.</param>
        public void BlinkLED(int times, bool repeat)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                BlinkLED(times);
                if (repeat)
                {
                    Thread.Sleep(_waitBetweenBlinks);
                    BlinkLED(times);
                }
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);  
        }

        /// <summary>
        /// Disposes of LED output port and sets single instance reference to null.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        protected virtual void Dispose(bool disposing)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    return;
                if (disposing)
                {
                    if (_ledOutput != null)
                        _ledOutput.Dispose();
                    lock (_ledSignallerSingleInstanceLockObject)
                        _ledSignallerSingleInstance = null;
                }
                _disposed = true;
            }
        }  

        private void BlinkLED(int times)
        {
            for (int i = 0; i < times; i++)
            {               
                _ledOutput.Write(true);
                Thread.Sleep(_ledBlinkTime);
                _ledOutput.Write(false);
                Thread.Sleep(_ledBlinkTime);
            }
        }        
    }
}
