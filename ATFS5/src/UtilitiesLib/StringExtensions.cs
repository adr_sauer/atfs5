using System;
using Microsoft.SPOT;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Class declares extension methods for String objects.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Returns a new string that left-aligns the characters in this string by padding them with spaces on the right, for a specified total length.
        /// </summary>
        /// <param name="stringToPad">String to pad.</param>
        /// <param name="totalWidth">The number of characters in the resulting string, equal to the number of original characters plus any additional padding characters.</param>
        /// <returns>A new string that is equivalent to this instance, but left-aligned and padded on the right with as many spaces as needed to create a length of totalWidth. 
        /// However, if totalWidth is less than the length of this instance, the method returns a reference to the existing instance. 
        /// If totalWidth is equal to the length of this instance, the method returns a new string that is identical to this instance.</returns>
        /// <exception cref="ArgumentException">totalWidth is less than zero.</exception>
        public static string PadRight(this string stringToPad, int totalWidth)
        {
            if (totalWidth < 0)
                throw new ArgumentException("totalWidth is less than zero.");
            char[] buffer = new char[totalWidth];

            if(totalWidth <= stringToPad.Length)
                return stringToPad;
            int i = 0;
            for (; i < stringToPad.Length; i++)
                buffer[i] = stringToPad[i];
            for (; i < totalWidth; i++)
                buffer[i] = ' ';
            return new String(buffer);
        }
    }
}
