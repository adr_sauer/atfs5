using System;
using System.Text;
using Microsoft.SPOT;
using System.Collections;


namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Represents Windows 1250 character encoding. 
    /// </summary>
    public class Windows1250Encoding : Encoding
    {
        static Hashtable _hashCharToByte;
        static Windows1250Encoding()
        {
            _hashCharToByte = new Hashtable(256);
            _hashCharToByte.Add(0, 0);
            _hashCharToByte.Add(1, 1);
            _hashCharToByte.Add(2, 2);
            _hashCharToByte.Add(3, 3);
            _hashCharToByte.Add(4, 4);
            _hashCharToByte.Add(5, 5);
            _hashCharToByte.Add(6, 6);
            _hashCharToByte.Add(7, 7);
            _hashCharToByte.Add(8, 8);
            _hashCharToByte.Add(9, 9);
            _hashCharToByte.Add(10, 10);
            _hashCharToByte.Add(11, 11);
            _hashCharToByte.Add(12, 12);
            _hashCharToByte.Add(13, 13);
            _hashCharToByte.Add(14, 14);
            _hashCharToByte.Add(15, 15);
            _hashCharToByte.Add(16, 16);
            _hashCharToByte.Add(17, 17);
            _hashCharToByte.Add(18, 18);
            _hashCharToByte.Add(19, 19);
            _hashCharToByte.Add(20, 20);
            _hashCharToByte.Add(21, 21);
            _hashCharToByte.Add(22, 22);
            _hashCharToByte.Add(23, 23);
            _hashCharToByte.Add(24, 24);
            _hashCharToByte.Add(25, 25);
            _hashCharToByte.Add(26, 26);
            _hashCharToByte.Add(27, 27);
            _hashCharToByte.Add(28, 28);
            _hashCharToByte.Add(29, 29);
            _hashCharToByte.Add(30, 30);
            _hashCharToByte.Add(31, 31);
            _hashCharToByte.Add(32, 32);
            _hashCharToByte.Add(33, 33);
            _hashCharToByte.Add(34, 34);
            _hashCharToByte.Add(35, 35);
            _hashCharToByte.Add(36, 36);
            _hashCharToByte.Add(37, 37);
            _hashCharToByte.Add(38, 38);
            _hashCharToByte.Add(39, 39);
            _hashCharToByte.Add(40, 40);
            _hashCharToByte.Add(41, 41);
            _hashCharToByte.Add(42, 42);
            _hashCharToByte.Add(43, 43);
            _hashCharToByte.Add(44, 44);
            _hashCharToByte.Add(45, 45);
            _hashCharToByte.Add(46, 46);
            _hashCharToByte.Add(47, 47);
            _hashCharToByte.Add(48, 48);
            _hashCharToByte.Add(49, 49);
            _hashCharToByte.Add(50, 50);
            _hashCharToByte.Add(51, 51);
            _hashCharToByte.Add(52, 52);
            _hashCharToByte.Add(53, 53);
            _hashCharToByte.Add(54, 54);
            _hashCharToByte.Add(55, 55);
            _hashCharToByte.Add(56, 56);
            _hashCharToByte.Add(57, 57);
            _hashCharToByte.Add(58, 58);
            _hashCharToByte.Add(59, 59);
            _hashCharToByte.Add(60, 60);
            _hashCharToByte.Add(61, 61);
            _hashCharToByte.Add(62, 62);
            _hashCharToByte.Add(63, 63);
            _hashCharToByte.Add(64, 64);
            _hashCharToByte.Add(65, 65);
            _hashCharToByte.Add(66, 66);
            _hashCharToByte.Add(67, 67);
            _hashCharToByte.Add(68, 68);
            _hashCharToByte.Add(69, 69);
            _hashCharToByte.Add(70, 70);
            _hashCharToByte.Add(71, 71);
            _hashCharToByte.Add(72, 72);
            _hashCharToByte.Add(73, 73);
            _hashCharToByte.Add(74, 74);
            _hashCharToByte.Add(75, 75);
            _hashCharToByte.Add(76, 76);
            _hashCharToByte.Add(77, 77);
            _hashCharToByte.Add(78, 78);
            _hashCharToByte.Add(79, 79);
            _hashCharToByte.Add(80, 80);
            _hashCharToByte.Add(81, 81);
            _hashCharToByte.Add(82, 82);
            _hashCharToByte.Add(83, 83);
            _hashCharToByte.Add(84, 84);
            _hashCharToByte.Add(85, 85);
            _hashCharToByte.Add(86, 86);
            _hashCharToByte.Add(87, 87);
            _hashCharToByte.Add(88, 88);
            _hashCharToByte.Add(89, 89);
            _hashCharToByte.Add(90, 90);
            _hashCharToByte.Add(91, 91);
            _hashCharToByte.Add(92, 92);
            _hashCharToByte.Add(93, 93);
            _hashCharToByte.Add(94, 94);
            _hashCharToByte.Add(95, 95);
            _hashCharToByte.Add(96, 96);
            _hashCharToByte.Add(97, 97);
            _hashCharToByte.Add(98, 98);
            _hashCharToByte.Add(99, 99);
            _hashCharToByte.Add(100, 100);
            _hashCharToByte.Add(101, 101);
            _hashCharToByte.Add(102, 102);
            _hashCharToByte.Add(103, 103);
            _hashCharToByte.Add(104, 104);
            _hashCharToByte.Add(105, 105);
            _hashCharToByte.Add(106, 106);
            _hashCharToByte.Add(107, 107);
            _hashCharToByte.Add(108, 108);
            _hashCharToByte.Add(109, 109);
            _hashCharToByte.Add(110, 110);
            _hashCharToByte.Add(111, 111);
            _hashCharToByte.Add(112, 112);
            _hashCharToByte.Add(113, 113);
            _hashCharToByte.Add(114, 114);
            _hashCharToByte.Add(115, 115);
            _hashCharToByte.Add(116, 116);
            _hashCharToByte.Add(117, 117);
            _hashCharToByte.Add(118, 118);
            _hashCharToByte.Add(119, 119);
            _hashCharToByte.Add(120, 120);
            _hashCharToByte.Add(121, 121);
            _hashCharToByte.Add(122, 122);
            _hashCharToByte.Add(123, 123);
            _hashCharToByte.Add(124, 124);
            _hashCharToByte.Add(125, 125);
            _hashCharToByte.Add(126, 126);
            _hashCharToByte.Add(127, 127);
            _hashCharToByte.Add(8364, 128);
            _hashCharToByte.Add(129, 129);
            _hashCharToByte.Add(8218, 130);
            _hashCharToByte.Add(131, 131);
            _hashCharToByte.Add(8222, 132);
            _hashCharToByte.Add(8230, 133);
            _hashCharToByte.Add(8224, 134);
            _hashCharToByte.Add(8225, 135);
            _hashCharToByte.Add(136, 136);
            _hashCharToByte.Add(8240, 137);
            _hashCharToByte.Add(352, 138);
            _hashCharToByte.Add(8249, 139);
            _hashCharToByte.Add(346, 140);
            _hashCharToByte.Add(356, 141);
            _hashCharToByte.Add(381, 142);
            _hashCharToByte.Add(377, 143);
            _hashCharToByte.Add(144, 144);
            _hashCharToByte.Add(8216, 145);
            _hashCharToByte.Add(8217, 146);
            _hashCharToByte.Add(8220, 147);
            _hashCharToByte.Add(8221, 148);
            _hashCharToByte.Add(8226, 149);
            _hashCharToByte.Add(8211, 150);
            _hashCharToByte.Add(8212, 151);
            _hashCharToByte.Add(152, 152);
            _hashCharToByte.Add(8482, 153);
            _hashCharToByte.Add(353, 154);
            _hashCharToByte.Add(8250, 155);
            _hashCharToByte.Add(347, 156);
            _hashCharToByte.Add(357, 157);
            _hashCharToByte.Add(382, 158);
            _hashCharToByte.Add(378, 159);
            _hashCharToByte.Add(160, 160);
            _hashCharToByte.Add(711, 161);
            _hashCharToByte.Add(728, 162);
            _hashCharToByte.Add(321, 163);
            _hashCharToByte.Add(164, 164);
            _hashCharToByte.Add(260, 165);
            _hashCharToByte.Add(166, 166);
            _hashCharToByte.Add(167, 167);
            _hashCharToByte.Add(168, 168);
            _hashCharToByte.Add(169, 169);
            _hashCharToByte.Add(350, 170);
            _hashCharToByte.Add(171, 171);
            _hashCharToByte.Add(172, 172);
            _hashCharToByte.Add(173, 173);
            _hashCharToByte.Add(174, 174);
            _hashCharToByte.Add(379, 175);
            _hashCharToByte.Add(176, 176);
            _hashCharToByte.Add(177, 177);
            _hashCharToByte.Add(731, 178);
            _hashCharToByte.Add(322, 179);
            _hashCharToByte.Add(180, 180);
            _hashCharToByte.Add(181, 181);
            _hashCharToByte.Add(182, 182);
            _hashCharToByte.Add(183, 183);
            _hashCharToByte.Add(184, 184);
            _hashCharToByte.Add(261, 185);
            _hashCharToByte.Add(351, 186);
            _hashCharToByte.Add(187, 187);
            _hashCharToByte.Add(317, 188);
            _hashCharToByte.Add(733, 189);
            _hashCharToByte.Add(318, 190);
            _hashCharToByte.Add(380, 191);
            _hashCharToByte.Add(340, 192);
            _hashCharToByte.Add(193, 193);
            _hashCharToByte.Add(194, 194);
            _hashCharToByte.Add(258, 195);
            _hashCharToByte.Add(196, 196);
            _hashCharToByte.Add(313, 197);
            _hashCharToByte.Add(262, 198);
            _hashCharToByte.Add(199, 199);
            _hashCharToByte.Add(268, 200);
            _hashCharToByte.Add(201, 201);
            _hashCharToByte.Add(280, 202);
            _hashCharToByte.Add(203, 203);
            _hashCharToByte.Add(282, 204);
            _hashCharToByte.Add(205, 205);
            _hashCharToByte.Add(206, 206);
            _hashCharToByte.Add(270, 207);
            _hashCharToByte.Add(272, 208);
            _hashCharToByte.Add(323, 209);
            _hashCharToByte.Add(327, 210);
            _hashCharToByte.Add(211, 211);
            _hashCharToByte.Add(212, 212);
            _hashCharToByte.Add(336, 213);
            _hashCharToByte.Add(214, 214);
            _hashCharToByte.Add(215, 215);
            _hashCharToByte.Add(344, 216);
            _hashCharToByte.Add(366, 217);
            _hashCharToByte.Add(218, 218);
            _hashCharToByte.Add(368, 219);
            _hashCharToByte.Add(220, 220);
            _hashCharToByte.Add(221, 221);
            _hashCharToByte.Add(354, 222);
            _hashCharToByte.Add(223, 223);
            _hashCharToByte.Add(341, 224);
            _hashCharToByte.Add(225, 225);
            _hashCharToByte.Add(226, 226);
            _hashCharToByte.Add(259, 227);
            _hashCharToByte.Add(228, 228);
            _hashCharToByte.Add(314, 229);
            _hashCharToByte.Add(263, 230);
            _hashCharToByte.Add(231, 231);
            _hashCharToByte.Add(269, 232);
            _hashCharToByte.Add(233, 233);
            _hashCharToByte.Add(281, 234);
            _hashCharToByte.Add(235, 235);
            _hashCharToByte.Add(283, 236);
            _hashCharToByte.Add(237, 237);
            _hashCharToByte.Add(238, 238);
            _hashCharToByte.Add(271, 239);
            _hashCharToByte.Add(273, 240);
            _hashCharToByte.Add(324, 241);
            _hashCharToByte.Add(328, 242);
            _hashCharToByte.Add(243, 243);
            _hashCharToByte.Add(244, 244);
            _hashCharToByte.Add(337, 245);
            _hashCharToByte.Add(246, 246);
            _hashCharToByte.Add(247, 247);
            _hashCharToByte.Add(345, 248);
            _hashCharToByte.Add(367, 249);
            _hashCharToByte.Add(250, 250);
            _hashCharToByte.Add(369, 251);
            _hashCharToByte.Add(252, 252);
            _hashCharToByte.Add(253, 253);
            _hashCharToByte.Add(355, 254);
            _hashCharToByte.Add(729, 255);
        }

        /// <summary>
        /// Gets an encoding for the Windows 1250 character format.
        /// </summary>
        public static Encoding Windows1250
        {
            get
            {
                return new Windows1250Encoding();
            }
        }

        /// <summary>
        /// Encodes a specified String object into an array of bytes. 
        /// </summary>
        /// <param name="s">The String object you want to encode.</param>
        /// <returns>A byte array that contains the encoded form of the specified String object.</returns>
        public override byte[] GetBytes(string s)
        {
            var bytes = new byte[s.Length];

            for (int i = 0; i < s.Length; i++)
            {
                bytes[i] = ConvertCharToByte(s[i]);
            }

            return bytes;
        }

        /// <summary>
        /// Encodes a specified String object into an array of bytes. 
        /// </summary>
        /// <param name="s">The String object you want to encode.</param>
        /// <param name="charIndex">The index of the first char in string s to convert.</param>
        /// <param name="charCount">The number of chars to convert to bytes.</param>
        /// <param name="bytes">An array in which to store the converted bytes.</param>
        /// <param name="byteIndex">The index of the first byte in bytes in which to store a converted byte.</param>
        /// <returns>Number of converted chars.</returns>
        public override int GetBytes(string s, int charIndex, int charCount, byte[] bytes, int byteIndex)
        {
            int i = 0;
            for (i = 0; i < charCount; i++)
            {
                bytes[i + byteIndex] = ConvertCharToByte(s[i + charIndex]);
            }
            return i;
        }

        /// <summary>
        /// Decodes all the bytes in the specified byte array into a set of characters. 
        /// </summary>
        /// <param name="bytes">The byte array containing the sequence of bytes to decode. </param>
        /// <returns>A character array containing the results of decoding the specified sequence of bytes.</returns>
        public override char[] GetChars(byte[] bytes)
        {
            char[] chars = new char[bytes.Length];
            Decoder decoder = new Windows1250Decoder();
            int bytesUsed;
            int charsUsed;
            bool completed;
            decoder.Convert(bytes, 0, bytes.Length, chars, 0, chars.Length, true, out bytesUsed, out charsUsed, out completed);
            return chars;
        }

        /// <summary>
        /// Decodes all the bytes in the specified byte array into a set of characters. 
        /// </summary>
        /// <param name="bytes">The byte array containing the sequence of bytes to decode. </param>
        /// <param name="byteIndex">The index of the first byte in bytes to convert.</param>
        /// <param name="byteCount">The number of bytes to convert in bytes.</param>
        /// <returns></returns>
        public override char[] GetChars(byte[] bytes, int byteIndex, int byteCount)
        {
            char[] chars = new char[byteCount];
            Decoder decoder = new Windows1250Decoder();
            int bytesUsed;
            int charsUsed;
            bool completed;
            decoder.Convert(bytes, byteIndex, byteCount, chars, 0, chars.Length, true, out bytesUsed, out charsUsed, out completed);
            return chars;
        }

        /// <summary>
        /// Obtains a Decoder object that can convert a sequence encoded in Windows 1250 encoding into a sequence of characters. 
        /// </summary>
        /// <returns>A Decoder object that can convert a sequence encoded in Windows 1250 encoding into a sequence of characters.</returns>
        public override Decoder GetDecoder()
        {
            return new Windows1250Decoder();
        }
               
        

        private byte ConvertCharToByte(char c)
        {
            if (!_hashCharToByte.Contains((int)c))
                throw new ArgumentException("Character not supported");
            int b=(int)_hashCharToByte[((int)c)];
            
            return (byte)b;
        }

    }
}
