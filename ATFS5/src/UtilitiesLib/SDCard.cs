using System;
using System.IO;
using System.Threading;
//using GHIElectronics.NETMF.IO;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.IO;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Class provides mounting of SD card in NETMF file system. Class implements Singleton pattern.
    /// </summary>
    public class SDCard
    {
        const int _waitForCardRegistrationDelayInMiliseconds = 10;
        const int _waitForCardToBootUpDelayInMiliseconds = 200;

        /*
        //private C_CommonUtilities common_utilities;
        //private C_LEDSignaller ledsignaller;
        private bool open = false;
        //private PersistentStorage per_stor = null;
        private VolumeInfo sd_vol_info = null;
        private string rootdir = "\\SD";
        private object lockobject;

        private static SDCard _sdcard_single_instance = null;
        private static object sdcard_single_instance_lock_obj;
        */

        /*
         *  // ...
        // assume SD Card is inserted
        // Create a new storage device
      	// NETMF only allows one SD card
      	// to be supported at the same time.


        SDCard sd_card = new SDCard();

        // this is a non-blocking call 
        // it fires the RemovableMedia.Insert event after 
        // the mount is finished. 
        sd_card.Mount();

        // for some cases, a simple sleep might suffice
        // This example just waits on the event before proceeding
        // (After first time firing of the event, you may want
        // to disable the handler or re-assign it
        bool fs_ready = false;
        RemovableMedia.Insert += (a, b) =>
        {
            fs_ready = true;
        };
        while (! fs_ready ) {
            System.Threading.Thread.Sleep(50);
        }
         */

        /// <summary>
        /// Mounts card filesystem if run on hardware.
        /// </summary>
        public static void OpenCard()
        {
            // TODO Hardware card loading
            /*
            foreach (var volume in VolumeInfo.GetVolumes())
            {
                Debug.Print(volume.RootDirectory);
            }
             */

        }
    }
}
