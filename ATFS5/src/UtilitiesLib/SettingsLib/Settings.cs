using System;
using Microsoft.SPOT;

namespace ATFS5.src.UtilitiesLib.SettingsLib
{

    /// <summary>
    /// Acts as a base class for deriving concrete wrapper classes to implement the application settings feature.
    /// </summary>
    public abstract class Settings
    {
        private const string _debugModeSettingKey="DebugMode";
        private SettingsData _data;

        /// <summary>
        /// Initializes a new instance of the UtilityUninitializedException class with specyfied SettingsData object.
        /// </summary>
        /// <param name="data">SettingsData object providing access to settings data.</param>
        public Settings(SettingsData data)
        {
            _data = data;
        }

        /// <summary>
        /// Gets or sets if debug mode is enabled.
        /// </summary>
        public bool DebugModeSetting
        {
            get 
            {
                return ParseBooleanString(_data.ReadSettingValueAsString(_debugModeSettingKey));
            }
            set
            {
                _data.SaveSettingValue(_debugModeSettingKey, value.ToString());
            }
        }

        /// <summary>
        /// Gets SettingsData object.
        /// </summary>
        protected SettingsData Data
        {
            get { return _data; }
        }


        /// <summary>
        /// Parses string value representing a boolean to bool value.
        /// </summary>
        /// <param name="boolString">String to parse.</param>
        /// <returns>Boolean value parsed from string.</returns>
        /// <exception cref="ArgumentException">Thrown when the passed string cannot be parsed.</exception>
        protected static bool ParseBooleanString(string boolString)
        {
            if (boolString == Boolean.TrueString)
                return true;
            else if (boolString == Boolean.FalseString)
                return false;
            else
                throw new ArgumentException("Boolean setting parsing failure.","boolString");
        }
    }

    


    

    
}
