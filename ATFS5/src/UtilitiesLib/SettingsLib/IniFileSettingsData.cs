using System;
using Microsoft.SPOT;
using System.IO;
using System.Collections;

namespace ATFS5.src.UtilitiesLib.SettingsLib
{
    /// <summary>
    /// This class provides access to settings data saved in ini file format.
    /// Details of INI file implementation:
    /// The key cannot contain the characters equal sign ( = ) or semi colon ( ; ) as these are reserved characters. The value can contain any character.
    /// Sections are not implemented.
    /// Keys are case sensitive.
    /// Semicolons (;) at the beginning of the line indicate a comment. Comment lines are ignored.
    /// Blank lines are not allowed.
    /// Duplicate keys are not allowed.
    /// Whitespaces are not ignored.
    /// Empty keys and values not allowed.
    /// </summary>
    public class IniFileSettingsData : SettingsData
    {
        private readonly char[] _valueKeySeparator = { '=' };
        private const char _commentLineIndicator = ';';
        private string _settingsFilePathname;
        private Hashtable _hashtable;

        /// <summary>
        /// Initializes a new instance of the IniFileSettingsData class using the specified ini file path.
        /// </summary>
        /// <param name="settingsFilePathname">Pathname of the ini file.</param>
        public IniFileSettingsData(string settingsFilePathname)
        {
            _settingsFilePathname = settingsFilePathname;
            _hashtable = new Hashtable();

            ReadSettingsFile();

        }

        /// <summary>
        /// Reads the value of a setting assigned to given key from ini file.
        /// </summary>
        /// <param name="key">Setting key.</param>
        /// <returns>String representing the value of setting.</returns>
        public override string ReadSettingValueAsString(string key)
        {
            if (!_hashtable.Contains(key))
                throw new ArgumentException("Key does not exist");

            return (string)_hashtable[key];
        }

        /// <summary>
        /// Saves the value of a setting to ini file.
        /// </summary>
        /// <param name="key">Setting key.</param>
        /// <param name="value">String representing the value of setting.</param>
        public override void SaveSettingValue(string key, string value)
        {
            _hashtable[key] = value;
            SaveSettingsFile();
        }

        private void SaveSettingsFile()
        {
            using (var settingsFileStream = File.Open(_settingsFilePathname, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (var settingsStreamWriter = new StreamWriter(settingsFileStream))
                {
                    object[] keys = new object[_hashtable.Keys.Count];
                    _hashtable.Keys.CopyTo(keys, 0);

                    for (int i = 0; i < keys.Length - 1; i++)
                        settingsStreamWriter.WriteLine(keys[i].ToString() + _valueKeySeparator[0] + _hashtable[keys[i]].ToString());
                    settingsStreamWriter.Write(keys[keys.Length - 1].ToString() + _valueKeySeparator[0] + _hashtable[keys[keys.Length - 1]].ToString());

                    settingsStreamWriter.Flush();
                }
            }
        }

        private void ReadSettingsFile()
        {
            using (var settingsFileStream = File.Open(_settingsFilePathname, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                using (var settingsStreamReader = new StreamReader(settingsFileStream))
                {
                    string line = settingsStreamReader.ReadLine();
                    while (line != null)
                    {
                        if (line[0] != _commentLineIndicator)
                        {
                            //If line is not a comment line

                            //Separate line into key and the rest of the line (ignore more than one _valueKeySeparator)
                            string[] separatedKeyValue = line.Split(_valueKeySeparator, 2);
                            if (separatedKeyValue.Length < 2)
                                throw new ApplicationException("Lines without key-value separator not allowed");
                            if (separatedKeyValue[0].Length < 1)
                                throw new ApplicationException("Empty keys not allowed");
                            if (separatedKeyValue[1].Length < 1)
                                throw new ApplicationException("Empty values not allowed");

                            string key = separatedKeyValue[0];
                            string value = separatedKeyValue[1];
                            _hashtable.Add(key, value);
                            line = settingsStreamReader.ReadLine();
                        }
                    }


                }
            }
        }


    }
}
