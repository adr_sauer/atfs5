using System;
using Microsoft.SPOT;

namespace ATFS5.src.UtilitiesLib.SettingsLib
{
    /// <summary>
    /// This abstract class provides general interface for classes which provide access to settings data saved in key-value form.
    /// Both the key and the setting value are stored as strings.
    /// </summary>
    public abstract class SettingsData
    {
        /// <summary>
        /// Reads the value of a setting assigned to given key.
        /// </summary>
        /// <param name="key">Setting key.</param>
        /// <returns>String representing the value of setting.</returns>
        public abstract string ReadSettingValueAsString(string key);

        /// <summary>
        /// Saves the value of a setting to storage.
        /// </summary>
        /// <param name="key">Setting key.</param>
        /// <param name="value">String representing the value of setting.</param>
        public abstract void SaveSettingValue(string key, string value);
    }
}
