using System;
using System.IO;
using System.Text;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using NetMf.CommonExtensions;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Class provides logging to file. Class implements Singleton pattern.
    /// Class is thread-safe.
    /// </summary>
    public sealed class FileLogger : Logger
    {
        private const string _errorLogFormat = "ERROR {0} Error occurred! Message: {1}\r\n";
        private const string _informationLogFormat = "INFO {0} {1}\r\n";
        private const string _exceptionLogFormat = "EXCEPTION {0} Exception occured! Message: {1}\r\n{2}\r\n";

        private FileStream _logFileStream = null;
        private LedSignaller _ledSignaller;
        private int _maxLogLengthInBytes;
        private static FileLogger _loggerSingleInstance = null;
        private static object _loggerSingleInstanceLockObject;
        private string _logFilePath;
        private bool _disposed = false;
        private object _lockObject;

        static FileLogger()
        {
            _loggerSingleInstanceLockObject = new object();
        }

        private FileLogger(int maxLogLengthInMegabytes, string logFilePath)
        {
            _lockObject = new object();
            _disposed = false;
            _logFilePath = logFilePath;
            _ledSignaller = Utilities.ProgramLedSignaller;
            _maxLogLengthInBytes = maxLogLengthInMegabytes * 1024 * 1024;

            try
            {
                _logFileStream = File.Open(_logFilePath, FileMode.Append, FileAccess.Write, FileShare.None);
                if (_logFileStream.Length > _maxLogLengthInBytes)
                    ClearLogFile();
            }
            catch (Exception e)
            {                
                LoggerError(e);
            }
        }

        /// <summary>
        /// Returns the single and only instance of FileLogger.
        /// </summary>
        /// <param name="maxLogLengthInMegabytes">Maximum size of log file in megabytes.</param>
        /// <param name="logFilePathname">Log file pathname.</param>
        /// <returns></returns>
        public static FileLogger ConstructSingleLogger(int maxLogLengthInMegabytes, string logFilePathname)
        {
            lock (_loggerSingleInstanceLockObject)
            {
                if (_loggerSingleInstance == null)
                    _loggerSingleInstance = new FileLogger(maxLogLengthInMegabytes, logFilePathname);

                return _loggerSingleInstance;
            }
        }

        /// <summary>
        /// Logs information to file.
        /// </summary>
        /// <param name="information">Information to log.</param>
        public override void LogInformation(string information)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                try
                {
                    string log = StringUtility.Format(_informationLogFormat, DateTime.Now, information);
                    byte[] logBytes = Encoding.UTF8.GetBytes(log);
                    FileStreamWriteLocked(logBytes);
                }
                catch (Exception e1)
                {
                    LoggerError(e1);
                }
            }
        }

        /// <summary>
        /// Logs exception occurence to file.
        /// </summary>
        /// <param name="e">Exception to log.</param>
        public override void LogException(Exception e)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                try
                {
                    string log = StringUtility.Format(_exceptionLogFormat, DateTime.Now, e.Message, e.StackTrace);
                    byte[] logBytes = Encoding.UTF8.GetBytes(log);
                    FileStreamWriteLocked(logBytes);
                }
                catch (Exception e1)
                {
                    LoggerError(e1);
                }

            }
        }

        /// <summary>
        /// Logs error message to file.
        /// </summary>
        /// <param name="errorMessage">Error message to log.</param>
        public override void LogError(string errorMessage)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                try
                {
                    string log = StringUtility.Format(_errorLogFormat, DateTime.Now, errorMessage);
                    byte[] logBytes = Encoding.UTF8.GetBytes(log);
                    FileStreamWriteLocked(logBytes);
                }
                catch (Exception e1)
                {
                    LoggerError(e1);
                }
            }
        }

        /// <summary>
        /// Disposes of file stream and sets single instance reference to null.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        protected override void Dispose(bool disposing)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    return;
                if (disposing)
                {
                    if (_logFileStream != null)
                        _logFileStream.Dispose();
                }
                lock (_loggerSingleInstanceLockObject)
                    _loggerSingleInstance = null;
                _disposed = true;
            }
        }

        private void FileStreamWriteLocked(byte[] data)
        {
            //Lock protects from multiple logs being logged at once

            lock (_logFileStream)
            {
                if (_logFileStream.Length + data.Length > _maxLogLengthInBytes)
                    ClearLogFile();
                _logFileStream.Write(data, 0, data.Length);
                _logFileStream.Flush();
            }
        }

        private void ClearLogFile()
        {
            lock (_logFileStream)
            {
                _logFileStream.Close();
                File.Delete(_logFilePath);
                _logFileStream = File.Open(_logFilePath, FileMode.Append, FileAccess.Write, FileShare.None);
            }
        }

        private void LoggerError(Exception e)
        {
            Debug.Print(e.Message);
            Debug.Print(e.StackTrace);
            _ledSignaller.BlinkLED(LedSignaller.LoggerErrorTimesToBlink, true);
            PowerState.RebootDevice(false);
        }


    }
}
