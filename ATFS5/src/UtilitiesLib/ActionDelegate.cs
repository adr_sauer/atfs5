using System;
using Microsoft.SPOT;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Encapsulates a method that has no parameters and does not return a value.
    /// </summary>
    public delegate void Action();
}
