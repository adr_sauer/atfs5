using System;
using Microsoft.SPOT;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Class provides thread safe way to signal threads and timers to stop.
    /// </summary>
    public class StopFlag
    {
        private static bool _keepWorking = true;
        private static object _keepWorkingLockObj = new object();

        /// <summary>
        /// <b>True</b> if thread is supposed to continue working. <b>False</b> otherwise.
        /// </summary>
        public bool KeepWorking
        {
            get
            {
                lock (_keepWorkingLockObj)
                    return _keepWorking;
            }
        }

        /// <summary>
        /// Signal thread or timer to stop.
        /// </summary>
        public void Stop()
        {
            lock (_keepWorkingLockObj)
                _keepWorking = false;
        }

        /// <summary>
        /// Reset stop flag.
        /// </summary>
        public void Reset()
        {
            lock (_keepWorkingLockObj)
                _keepWorking = true;
        }
    }
}
