using System;
using System.Text;
using Microsoft.SPOT;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Converts a sequence ofWindows 1250 encoded bytes into a set of characters. 
    /// </summary>
    public class Windows1250Decoder : Decoder
    {
        private static char[] _decodingTable = {(char)0,
(char)1,(char)2,(char)3,(char)4,(char)5,(char)6,(char)7,(char)8,(char)9,(char)10,
(char)11,(char)12,(char)13,(char)14,(char)15,(char)16,(char)17,(char)18,(char)19,(char)20,
(char)21,(char)22,(char)23,(char)24,(char)25,(char)26,(char)27,(char)28,(char)29,(char)30,
(char)31,(char)32,(char)33,(char)34,(char)35,(char)36,(char)37,(char)38,(char)39,(char)40,
(char)41,(char)42,(char)43,(char)44,(char)45,(char)46,(char)47,(char)48,(char)49,(char)50,
(char)51,(char)52,(char)53,(char)54,(char)55,(char)56,(char)57,(char)58,(char)59,(char)60,
(char)61,(char)62,(char)63,(char)64,(char)65,(char)66,(char)67,(char)68,(char)69,(char)70,
(char)71,(char)72,(char)73,(char)74,(char)75,(char)76,(char)77,(char)78,(char)79,(char)80,
(char)81,(char)82,(char)83,(char)84,(char)85,(char)86,(char)87,(char)88,(char)89,(char)90,
(char)91,(char)92,(char)93,(char)94,(char)95,(char)96,(char)97,(char)98,(char)99,(char)100,
(char)101,(char)102,(char)103,(char)104,(char)105,(char)106,(char)107,(char)108,(char)109,(char)110,
(char)111,(char)112,(char)113,(char)114,(char)115,(char)116,(char)117,(char)118,(char)119,(char)120,
(char)121,(char)122,(char)123,(char)124,(char)125,(char)126,(char)127,(char)8364,(char)129,(char)8218,
(char)131,(char)8222,(char)8230,(char)8224,(char)8225,(char)136,(char)8240,(char)352,(char)8249,(char)346,
(char)356,(char)381,(char)377,(char)144,(char)8216,(char)8217,(char)8220,(char)8221,(char)8226,(char)8211,
(char)8212,(char)152,(char)8482,(char)353,(char)8250,(char)347,(char)357,(char)382,(char)378,(char)160,
(char)711,(char)728,(char)321,(char)164,(char)260,(char)166,(char)167,(char)168,(char)169,(char)350,
(char)171,(char)172,(char)173,(char)174,(char)379,(char)176,(char)177,(char)731,(char)322,(char)180,
(char)181,(char)182,(char)183,(char)184,(char)261,(char)351,(char)187,(char)317,(char)733,(char)318,
(char)380,(char)340,(char)193,(char)194,(char)258,(char)196,(char)313,(char)262,(char)199,(char)268,
(char)201,(char)280,(char)203,(char)282,(char)205,(char)206,(char)270,(char)272,(char)323,(char)327,
(char)211,(char)212,(char)336,(char)214,(char)215,(char)344,(char)366,(char)218,(char)368,(char)220,
(char)221,(char)354,(char)223,(char)341,(char)225,(char)226,(char)259,(char)228,(char)314,(char)263,
(char)231,(char)269,(char)233,(char)281,(char)235,(char)283,(char)237,(char)238,(char)271,(char)273,
(char)324,(char)328,(char)243,(char)244,(char)337,(char)246,(char)247,(char)345,(char)367,(char)250,
(char)369,(char)252,(char)253,(char)355,(char)729};


        /// <summary>
        /// Converts an array of Windows 1250 encoded bytes to an array of chars. 
        /// </summary>
        /// <param name="bytes">A byte array to convert.</param>
        /// <param name="byteIndex">The index of the first byte in bytes to convert.</param>
        /// <param name="byteCount">The number of bytes to convert in bytes.</param>
        /// <param name="chars">An array in which to store the converted characters.</param>
        /// <param name="charIndex">The index of the first char in chars in which to store a converted character.</param>
        /// <param name="charCount">The maximum number of chars to write into chars.</param>
        /// <param name="flush">Set to <b>true</b> to indicate that no further data is to be converted; otherwise, set to <b>false</b>.</param>
        /// <param name="bytesUsed">On exit, contains the number of bytes from bytes that were converted.</param>
        /// <param name="charsUsed">On exit, contains the number of chars that were written to in chars.</param>
        /// <param name="completed">On exit, contains <b>true</b> if all the characters specified by byteCount were converted, or <b>false</b> if not.</param>
        public override void Convert(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex, int charCount, bool flush, out int bytesUsed, out int charsUsed, out bool completed)
        {
            //In Windows 1250 encoding one byte equals one character
            //Wrong parameters will cause exception in table access anyway

            for (int i = 0, j = 0; (i < byteCount) && (j < charCount); i++, j++)
            {
                chars[j+charIndex]=_decodingTable[bytes[i+byteIndex]];
            }

            bytesUsed = byteCount;
            charsUsed = charCount;
            completed = true;
        }
    }
}
