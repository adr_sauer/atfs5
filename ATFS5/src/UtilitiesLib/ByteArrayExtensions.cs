using System;
using Microsoft.SPOT;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Class declares extension methods for byte array.
    /// </summary>
    public static class ByteArrayExtensions
    {
        /// <summary>
        /// Reverses the sequence of bytes in byte array.
        /// </summary>
        /// <param name="byteArray">Byte array to reverse.</param>
        public static void ReverseBytes(this byte[] byteArray)
        {
            if (byteArray == null)
                throw new ArgumentNullException();
            for (int i = 0, j = byteArray.Length - 1; i < j; i++, j--)
            {
                byte temp = byteArray[j];
                byteArray[j] = byteArray[i];
                byteArray[i] = temp;
            }
        }

        /// <summary>
        /// Compares byte arrays.
        /// </summary>
        /// <param name="byteArray1">Byte array to compare to.</param>
        /// <param name="byteArray2">Byte array to compare.</param>
        /// <returns><b>True</b> if parameter array contains the same bytes in the same order as array compared to. <b>False</b> otherwise.</returns>
        public static bool CompareByteArray(this byte[] byteArray1, byte[] byteArray2)
        {
            if (byteArray1 == null)
                throw new ArgumentNullException();
            if (byteArray2 == null)
                throw new ArgumentNullException("byteArray2");

            bool areEqual = true;
            if (byteArray1.Length != byteArray2.Length)
                areEqual = false;
            else
            {
                for (int i = 0; i < byteArray1.Length; i++)
                {
                    if (byteArray1[i] != byteArray2[i])
                        areEqual = false;
                }
            }

            return areEqual;
        }
    }
}
