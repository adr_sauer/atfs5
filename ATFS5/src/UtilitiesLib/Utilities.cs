using System;
using Microsoft.SPOT;
using ATFS5.src.UtilitiesLib.SettingsLib;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Class provides access to system wide utilities.
    /// Correct order of initialization is:
    /// 1. LedSignaller
    /// 2. SDCard
    /// 3. Logger
    /// 4. UnforeseenExceptionHandler
    /// 5. Settings
    /// </summary>
    public static class Utilities
    {
        private static LedSignaller _ledSignaller = null;
        private static object _sdCard = null;
        private static Logger _logger=null;
        private static UnforeseenExceptionHandlerDelegate _unforeseenExceptionHandler = null;
        private static Settings _settings = null;
        

        /// <summary>
        /// Gets program wide LED signaller.
        /// </summary>
        public static LedSignaller ProgramLedSignaller
        {
            get
            {
                if (_ledSignaller != null)
                    return _ledSignaller;
                else
                    throw new UtilityUninitializedException("LedSignaller not initialized.");
            }
        }

        /// <summary>
        /// Gets program wide logger.
        /// </summary>
        public static Logger ProgramLogger
        {
            get
            {
                if (_logger != null)
                    return _logger;
                else
                    throw new UtilityUninitializedException("Logger not initialized.");
            }
        }

        /// <summary>
        /// Gets program wide settings.
        /// </summary>
        public static Settings ProgramSettings
        {
            get
            {
                if (_settings != null)
                    return _settings;
                else
                    throw new UtilityUninitializedException("Settings not initialized.");
            }
        }

        /// <summary>
        /// Initializes ProgramLedSignaller property. Can be initialized only once during progam execution.
        /// </summary>
        /// <param name="ledSignaller"></param>
        public static void InitializeLedSignaller(LedSignaller ledSignaller)
        {
            if (_ledSignaller == null)
                _ledSignaller = ledSignaller;
            else
                throw new UtilityAlreadyinitializedException("LedSignaller already initialized.");

        }

        /// <summary>
        /// Initializes SD card and mounts it to filesystem.
        /// </summary>
        public static void InitializeSDCard()
        {
            if (_sdCard == null)
            {
                SDCard.OpenCard();
                _sdCard = new object();
            }
            else
                throw new UtilityAlreadyinitializedException("SDCard already initialized.");
            
        }

        /// <summary>
        /// Initializes ProgramLogger property. Can be initialized only once during progam execution.
        /// </summary>
        /// <param name="logger"></param>
        public static void InitializeLogger(Logger logger)
        {
            if (_logger == null)
                _logger = logger;
            else
                throw new UtilityAlreadyinitializedException("Logger already initialized.");
           
        }

        /// <summary>
        /// Initializes UnforeseenExceptionHandler delegate called by HandleUnforeseenException method. Can be initialized only once during progam execution.
        /// </summary>
        /// <param name="unforeseenExceptionHandler"></param>
        public static void InitializeUnforeseenExceptionHandler(UnforeseenExceptionHandlerDelegate unforeseenExceptionHandler)
        {
            if (_unforeseenExceptionHandler == null)
                _unforeseenExceptionHandler = unforeseenExceptionHandler;
            else
                throw new UtilityAlreadyinitializedException("UnforeseenExceptionHandler already initialized.");
        }

        /// <summary>
        /// Initializes ProgramSettings property. Can be initialized only once during progam execution.
        /// </summary>
        /// <param name="settings"></param>
        public static void InitializeSettings(Settings settings)
        {
            
            if (_settings == null)
                _settings = settings;
            else
                throw new UtilityAlreadyinitializedException("Settings already initialized.");
        }

        /// <summary>
        /// Handles unforeseen exceptions.
        /// </summary>
        /// <param name="e">Caught exception.</param>
        public static void HandleUnforeseenException(Exception e)
        {
            _unforeseenExceptionHandler(e);
        }
    }

    /// <summary>
    /// The exception that is thrown when a utility that has been accessed has not been initialized yet.
    /// </summary>
    public class UtilityUninitializedException : SystemException
    {

        /// <summary>
        /// Initializes a new instance of the UtilityUninitializedException class with a specified error message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public UtilityUninitializedException(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    /// The exception that is thrown when an attemt to reinitialize an utility occurs.
    /// </summary>
    public class UtilityAlreadyinitializedException : SystemException
    {
        /// <summary>
        /// Initializes a new instance of the UtilityAlreadyinitializedException class with a specified error message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public UtilityAlreadyinitializedException(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    /// Represents a delegate, which can refer to a method which handles unforeseen exceptions.
    /// </summary>
    /// <param name="e">Caught exception.</param>
    public delegate void UnforeseenExceptionHandlerDelegate (Exception e);
}
