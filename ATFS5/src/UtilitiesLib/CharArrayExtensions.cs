using System;
using Microsoft.SPOT;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Class declares extension methods for char array.
    /// </summary>
    public static class CharArrayExtensions
    {
        /// <summary>
        /// Searches for the specified character and returns the index of its first occurrence in a one-dimensional array.
        /// </summary>
        /// <param name="charArray">Character array to search.</param>
        /// <param name="character">The character to locate in array.</param>
        /// <returns>The index of the first occurrence of value in array, if found; otherwise -1.</returns>
        public static int IndexOf(this char[] charArray, char character)
        {
            int i = 0;
            while ((charArray[i] != character) && (i < charArray.Length))
            {
                i++;
            }
            if (i == charArray.Length)
                return -1;
            else
                return i;
        }
    }
}
