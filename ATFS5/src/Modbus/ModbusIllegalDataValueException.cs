using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;

namespace ATFS5.src.Modbus
{
    class ModbusIllegalDataValueException : ModbusException
    {
        public override ModbusExceptionPdu GetAppropriateExceptionPdu(byte requestFunctionCode)
        {
            return new Exception03IllegalDataValuePdu(requestFunctionCode);
        }
    }
}
