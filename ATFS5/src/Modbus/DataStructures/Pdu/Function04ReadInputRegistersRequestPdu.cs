using System;
using Microsoft.SPOT;
using ATFS5.src.UtilitiesLib;
using NetMf.CommonExtensions;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Function04ReadInputRegistersRequestPdu : ModbusFunctionPdu
    {
        public const byte FunctionCodeOfPduType = 4;
        private const int _function04RequestByteLength = 5;
        private const int _minRegistersQuantity = 1;
        private const int _maxRegistersQuantity = 125;

        private ushort _startAddress = 1;
        private ushort _registersQuantity = 0;

        public override byte FunctionCode
        {
            get { return FunctionCodeOfPduType; }
        }

        public ushort StartAddress
        {
            get { return _startAddress; }
        }

        public ushort RegistersQuantity
        {
            get { return _registersQuantity; }
        }

        public override int ByteDataLength
        {
            get { return _function04RequestByteLength; }
        }

        public Function04ReadInputRegistersRequestPdu(byte[] data,int index=0)
        {
            GetFromBytes(data, index);
        }

        public Function04ReadInputRegistersRequestPdu(ushort startAddress, ushort registersQuantity)
        {
            _startAddress = startAddress;
            _registersQuantity = registersQuantity;
            if (_registersQuantity > _maxRegistersQuantity)
                throw new ModbusIllegalDataValueException();
            if (_registersQuantity < _minRegistersQuantity)
                throw new ModbusIllegalDataValueException();
        }

        public override string ToString()
        {
            return StringUtility.Format("Modbus read input registers request\nFunction code: {0}\nStart address: {1}\nRegisters quantity: {2}",
                FunctionCode, _startAddress, _registersQuantity);
        }

        public override void WriteIntoByteTable(byte[] buffer, int index)
        {
            if (index > buffer.Length - ByteDataLength)
                throw new ArgumentException("Buffer too short");
            buffer[index] = FunctionCode;

            byte[] startAddressBytes = BitConverter.GetBytes(_startAddress);
            byte[] registersQuantityBytes = BitConverter.GetBytes(_registersQuantity);

            if (BitConverter.IsLittleEndian)
            {
                //Modbus protocol requires BigEndian
                startAddressBytes.ReverseBytes();
                registersQuantityBytes.ReverseBytes();
            }
            startAddressBytes.CopyTo(buffer, index+1);
            registersQuantityBytes.CopyTo(buffer, index+3);
        }

        protected override void GetFromBytes(byte[] data, int index)
        {
            if (index > data.Length - ByteDataLength)
                throw new ArgumentException("Byte data too short");

            if (data[index] != FunctionCode)
                throw new ArgumentException("Wrong function data.");


            byte[] startAddressBytes = new byte[sizeof(ushort)];
            byte[] registersQuantityBytes = new byte[sizeof(ushort)];

            Array.Copy(data, index + 1, startAddressBytes, 0, sizeof(ushort));
            Array.Copy(data, index + 3, registersQuantityBytes, 0, sizeof(ushort));

            if (BitConverter.IsLittleEndian)
            {
                startAddressBytes.ReverseBytes();
                registersQuantityBytes.ReverseBytes();
            }
            _startAddress = BitConverter.ToUInt16(startAddressBytes, 0);
            _registersQuantity = BitConverter.ToUInt16(registersQuantityBytes, 0);

            if (_registersQuantity > _maxRegistersQuantity)
                throw new ModbusIllegalDataValueException();
            if (_registersQuantity < _minRegistersQuantity)
                throw new ModbusIllegalDataValueException();
        }
    }
}
