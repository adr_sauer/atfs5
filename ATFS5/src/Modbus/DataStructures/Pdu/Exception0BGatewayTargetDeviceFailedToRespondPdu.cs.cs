using System;
using Microsoft.SPOT;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Exception0BGatewayTargetDeviceFailedToRespondPdu : ModbusExceptionPdu
    {
        public const byte ExceptionCodeOfPduType = 0x0B;

        public override byte ExceptionCode
        {
            get { return ExceptionCodeOfPduType; }
        }

        public Exception0BGatewayTargetDeviceFailedToRespondPdu(byte originalFunctionCode)
            : base(originalFunctionCode)
        {
        }

        public Exception0BGatewayTargetDeviceFailedToRespondPdu(byte[] data, int index = 0)
            : base(data, index)
        {
        }
    }
}
