using System;
using Microsoft.SPOT;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    abstract class ModbusExceptionPdu : ModbusPdu
    {
        //public const byte ModbusExceptionCode = 1;
        protected byte _functionCode;
        private const int _exceptionByteLength = 2;

        public abstract byte ExceptionCode { get; }

        public byte OriginalFunctionCode
        {
            get { return _functionCode; }
        }

        public override int ByteDataLength
        {
            get { return _exceptionByteLength; }
        }

        public ModbusExceptionPdu(byte originalFunctionCode)
        {
            _functionCode = originalFunctionCode;
        }

        public ModbusExceptionPdu(byte[] data, int index)
        {            
            GetFromBytes(data, index);
        }

        
        public override void WriteIntoByteTable(byte[] buffer, int index)
        {
            if (index > buffer.Length - ByteDataLength)
                throw new ArgumentException("Buffer too short");
            buffer[index + 0] = (byte)(_functionCode + MaximumFunctionCodeNotIndicatingException);
            buffer[index + 1] = ExceptionCode;
        }

        protected override void GetFromBytes(byte[] data, int index)
        {
            if (index > data.Length - ByteDataLength)
                throw new ArgumentException("Byte data too short");

            if (data[index] <= MaximumFunctionCodeNotIndicatingException)
                throw new ArgumentException("Function code does not indicate modbus exception.");

            if (data[index + 1] != ExceptionCode)
                throw new ArgumentException("Exception code does not fit exception class.");

            _functionCode = (byte)(data[index]-MaximumFunctionCodeNotIndicatingException);
        }

    }
}
