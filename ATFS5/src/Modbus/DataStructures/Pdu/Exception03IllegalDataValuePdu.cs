using System;
using Microsoft.SPOT;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Exception03IllegalDataValuePdu : ModbusExceptionPdu
    {
        public const byte ExceptionCodeOfPduType = 3;

        public override byte ExceptionCode
        {
            get { return ExceptionCodeOfPduType; }
        }

        public Exception03IllegalDataValuePdu(byte originalFunctionCode)
            : base(originalFunctionCode)
        {
        }

        public Exception03IllegalDataValuePdu(byte[] data, int index = 0)
            : base(data, index)
        {
        }
    }
}
