using System;
using Microsoft.SPOT;
using ATFS5.src.UtilitiesLib;
using NetMf.CommonExtensions;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Function03ReadHoldingRegistersResponsePdu : ModbusFunctionPdu
    {
        public const byte FunctionCodeOfPduType = 3;
        private const int _function03ConstantPartByteLength = 2;
        private const int _minRegistersQuantity = 1;
        private const int _maxRegistersQuantity = 125;

        private ushort[] _registersValues;

        public ushort this[int index]
        {
            get
            {
                return _registersValues[index];
            }
        }

        public ushort RegistersQuantity
        {
            get
            {
                return (ushort)_registersValues.Length;
            }
        }

        public override byte FunctionCode
        {
            get { return FunctionCodeOfPduType; }
        }

        public override int ByteDataLength
        {
            get { return _function03ConstantPartByteLength + (sizeof(ushort) * _registersValues.Length); }
        }

        public Function03ReadHoldingRegistersResponsePdu(byte[] data, int index = 0)
        {
            GetFromBytes(data, index);
        }

        public Function03ReadHoldingRegistersResponsePdu(ushort[] registersValues)
        {
            _registersValues = new ushort[registersValues.Length];
            registersValues.CopyTo(_registersValues, 0);
            if (_registersValues.Length > _maxRegistersQuantity)
                throw new ModbusIllegalDataValueException();
            if (_registersValues.Length < _minRegistersQuantity)
                throw new ModbusIllegalDataValueException();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder("Modbus read holding registers response\nRegisters values:\n");

            foreach (ushort value in _registersValues)
                builder.Append(value.ToString());
            return builder.ToString();
        }



        public override void WriteIntoByteTable(byte[] buffer, int index)
        {
            if (index > buffer.Length - ByteDataLength)
                throw new ArgumentException("Buffer too short");
            buffer[index] = FunctionCode;
            buffer[index + 1] = (byte)(sizeof(ushort) * _registersValues.Length);

            for (int i = 0, j = 2; i < _registersValues.Length; i++, j += 2)
            {
                byte[] register = BitConverter.GetBytes(_registersValues[i]);
                if (BitConverter.IsLittleEndian)
                {
                    //Modbus protocol requires BigEndian
                    register.ReverseBytes();
                }
                register.CopyTo(buffer, index + j);
            }
        }

        protected override void GetFromBytes(byte[] data, int index)
        {
            if (index > data.Length - _function03ConstantPartByteLength)
                throw new ArgumentException("Byte data too short");

            if (data[index] != FunctionCode)
                throw new ArgumentException("Wrong function data.");

            byte byteCount = data[index + 1];

            if ((byteCount % 2) != 0)
                throw new ArgumentException("Byte count not divisible by 2.");

            int registersQuantity = byteCount / 2;

            if (registersQuantity > _maxRegistersQuantity)
                throw new ModbusIllegalDataValueException();
            if (registersQuantity < _minRegistersQuantity)
                throw new ModbusIllegalDataValueException();

            _registersValues = new ushort[registersQuantity];

            if (index > data.Length - ByteDataLength)
                throw new ArgumentException("Byte data too short");

            for (int i = 0, j = 2; i < _registersValues.Length; i++, j += 2)
            {
                byte[] register = new byte[sizeof(ushort)];
                Array.Copy(data, index + j, register, 0, sizeof(ushort));
                if (BitConverter.IsLittleEndian)
                {
                    //Modbus protocol requires BigEndian
                    register.ReverseBytes();
                }
                _registersValues[i] = BitConverter.ToUInt16(register, 0);
            }

        }


    }
}
