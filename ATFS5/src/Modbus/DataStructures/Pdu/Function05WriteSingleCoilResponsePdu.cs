using System;
using Microsoft.SPOT;
using ATFS5.src.UtilitiesLib;
using NetMf.CommonExtensions;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Function05WriteSingleCoilResponsePdu : Function05WriteSingleCoilRequestPdu
    {
        public Function05WriteSingleCoilResponsePdu(byte[] data, int index = 0)
            : base(data, index)
        {

        }

        public Function05WriteSingleCoilResponsePdu(ushort coilAddress, bool coilStateToSet)
            : base(coilAddress, coilStateToSet)
        {

        }

        public override string ToString()
        {
            return StringUtility.Format("Modbus write single coil response\nFunction code: {0}\nCoil address: {1}\nCoil state to set: {2}",
                FunctionCode, CoilAddress, CoilStateToSet);
        }
    }
}
