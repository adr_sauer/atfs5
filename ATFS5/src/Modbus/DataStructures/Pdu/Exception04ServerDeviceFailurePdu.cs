using System;
using Microsoft.SPOT;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Exception04ServerDeviceFailurePdu : ModbusExceptionPdu
    {
        public const byte ExceptionCodeOfPduType = 3;

        public override byte ExceptionCode
        {
            get { return ExceptionCodeOfPduType; }
        }

        public Exception04ServerDeviceFailurePdu(byte originalFunctionCode)
            : base(originalFunctionCode)
        {
        }

        public Exception04ServerDeviceFailurePdu(byte[] data, int index = 0)
            : base(data, index)
        {
        }
    }
}
