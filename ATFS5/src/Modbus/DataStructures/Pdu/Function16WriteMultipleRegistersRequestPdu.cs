using System;
using Microsoft.SPOT;
using ATFS5.src.UtilitiesLib;
using NetMf.CommonExtensions;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Function16WriteMultipleRegistersRequestPdu : ModbusFunctionPdu
    {
        public const byte FunctionCodeOfPduType = 16;
        private const int _function16ConstantPartByteLength = 6;
        private const int _minRegistersQuantity = 1;
        private const int _maxRegistersQuantity = 123;

        private ushort _startAddress = 1;
        private ushort[] _registersValues;

        public override byte FunctionCode
        {
            get { return FunctionCodeOfPduType; }
        }

        public override int ByteDataLength
        {
            get { return _function16ConstantPartByteLength + (sizeof(ushort) * _registersValues.Length); }
        }

        public ushort StartAddress
        {
            get { return _startAddress; }
        }

        public ushort this[int index]
        {
            get
            {
                return _registersValues[index];
            }
        }

        public ushort RegistersQuantity
        {
            get
            {
                return (ushort)_registersValues.Length;
            }
        }

        public Function16WriteMultipleRegistersRequestPdu(byte[] data, int index = 0)
        {
            GetFromBytes(data, index);
        }

        public Function16WriteMultipleRegistersRequestPdu(ushort startAddress, ushort[] registersValues)
        {
            _startAddress = startAddress;
            _registersValues = new ushort[registersValues.Length];
            registersValues.CopyTo(_registersValues, 0);
            if (_registersValues.Length > _maxRegistersQuantity)
                throw new ModbusIllegalDataValueException();
            if (_registersValues.Length < _minRegistersQuantity)
                throw new ModbusIllegalDataValueException();
        }

        public override void WriteIntoByteTable(byte[] buffer, int index)
        {
            if (index > buffer.Length - ByteDataLength)
                throw new ArgumentException("Buffer too short");
            buffer[index] = FunctionCode;

            byte[] startAddressBytes = BitConverter.GetBytes(_startAddress);
            byte[] registersQuantityBytes = BitConverter.GetBytes((ushort)_registersValues.Length);

            if (BitConverter.IsLittleEndian)
            {
                //Modbus protocol requires BigEndian
                startAddressBytes.ReverseBytes();
                registersQuantityBytes.ReverseBytes();
            }
            startAddressBytes.CopyTo(buffer, index + 1);
            registersQuantityBytes.CopyTo(buffer, index + 3);
            buffer[index + 5] = (byte)(_registersValues.Length * 2);

            for (int i = 0, j = 6; i < _registersValues.Length; i++, j += 2)
            {
                byte[] register = BitConverter.GetBytes(_registersValues[i]);
                if (BitConverter.IsLittleEndian)
                {
                    //Modbus protocol requires BigEndian
                    register.ReverseBytes();
                }
                register.CopyTo(buffer, index + j);
            }
        }

        protected override void GetFromBytes(byte[] data, int index)
        {
            if (index > data.Length - _function16ConstantPartByteLength)
                throw new ArgumentException("Byte data too short");

            if (data[index] != FunctionCode)
                throw new ArgumentException("Wrong function data.");

            byte[] startAddressBytes = new byte[sizeof(ushort)];
            byte[] registersQuantityBytes = new byte[sizeof(ushort)];
            

            Array.Copy(data, index + 1, startAddressBytes, 0, sizeof(ushort));
            Array.Copy(data, index + 3, registersQuantityBytes, 0, sizeof(ushort));
            int byteCount = data[index + 5];

            if (BitConverter.IsLittleEndian)
            {
                startAddressBytes.ReverseBytes();
                registersQuantityBytes.ReverseBytes();
            }

            _startAddress = BitConverter.ToUInt16(startAddressBytes, 0);
            int registersQuantity = BitConverter.ToUInt16(registersQuantityBytes, 0);

            if (registersQuantity > _maxRegistersQuantity)
                throw new ModbusIllegalDataValueException();
            if (registersQuantity < _minRegistersQuantity)
                throw new ModbusIllegalDataValueException();
            if(byteCount != (registersQuantity*2))
                throw new ModbusIllegalDataValueException();

            _registersValues = new ushort[registersQuantity];

            if (index > data.Length - ByteDataLength)
                throw new ArgumentException("Byte data too short");

            for (int i = 0, j = 6; i < _registersValues.Length; i++, j += 2)
            {
                byte[] register = new byte[sizeof(ushort)];
                Array.Copy(data, index + j, register, 0, sizeof(ushort));
                if (BitConverter.IsLittleEndian)
                {
                    //Modbus protocol requires BigEndian
                    register.ReverseBytes();
                }
                _registersValues[i] = BitConverter.ToUInt16(register, 0);
            }

        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("Modbus write multiple registers request\nFunction code: {0}\nStart address: {1}\nRegisters values:\n",
                FunctionCodeOfPduType, _startAddress);
            
            foreach (ushort value in _registersValues)
                builder.Append(value.ToString());
            return builder.ToString();
        }
    }
}
