using System;
using Microsoft.SPOT;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Exception02IllegalDataAddressPdu : ModbusExceptionPdu
    {
        public const byte ExceptionCodeOfPduType = 2;

        public override byte ExceptionCode
        {
            get { return ExceptionCodeOfPduType; }
        }

        public Exception02IllegalDataAddressPdu(byte originalFunctionCode)
            : base(originalFunctionCode)
        {
        }

        public Exception02IllegalDataAddressPdu(byte[] data, int index = 0) : base (data,index)
        {
        }
    }
}
