using System;
using Microsoft.SPOT;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Exception0AGatewayPathUnavailablePdu : ModbusExceptionPdu
    {
        public const byte ExceptionCodeOfPduType = 0x0A;

        public override byte ExceptionCode
        {
            get { return ExceptionCodeOfPduType; }
        }

        public Exception0AGatewayPathUnavailablePdu(byte originalFunctionCode)
            : base(originalFunctionCode)
        {
        }

        public Exception0AGatewayPathUnavailablePdu(byte[] data, int index = 0)
            : base(data, index)
        {
        }
    }
}
