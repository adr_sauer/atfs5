using System;
using Microsoft.SPOT;
using ATFS5.src.UtilitiesLib;
using NetMf.CommonExtensions;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Function05WriteSingleCoilRequestPdu : ModbusFunctionPdu
    {
        public const byte FunctionCodeOfPduType = 5;
        private const ushort _coilOnValue = 0xFF00;
        private const ushort _coilOffValue = 0x0000;
        private const int _function05RequestByteLength = 5;
        private ushort _coilAddress = 1;
        private bool _coilStateToSet = false;

        public override byte FunctionCode
        {
            get { return FunctionCodeOfPduType; }
        }

        public override int ByteDataLength
        {
            get { return _function05RequestByteLength; }
        }

        public ushort CoilAddress
        {
            get { return _coilAddress; }
        }

        public bool CoilStateToSet
        {
            get { return _coilStateToSet; }
        }

        public Function05WriteSingleCoilRequestPdu(byte[] data, int index = 0)
        {            
            GetFromBytes(data, index);
        }

        public Function05WriteSingleCoilRequestPdu(ushort coilAddress, bool coilStateToSet)
        {
            _coilAddress = coilAddress;
            _coilStateToSet = coilStateToSet;
        }

        public override void WriteIntoByteTable(byte[] buffer, int index)
        {
            if (index > buffer.Length - ByteDataLength)
                throw new ArgumentException("Buffer too short");
            buffer[index] = FunctionCode;
            byte[] coilAddressBytes = BitConverter.GetBytes(_coilAddress);
            byte[] coilStateToSetBytes = BitConverter.GetBytes(_coilStateToSet ? _coilOnValue : _coilOffValue);

            if (BitConverter.IsLittleEndian)
            {
                //Modbus protocol requires BigEndian
                coilAddressBytes.ReverseBytes();
                coilStateToSetBytes.ReverseBytes();
            }
            coilAddressBytes.CopyTo(buffer, index+1);
            coilStateToSetBytes.CopyTo(buffer, index+3);
        }

        public override string ToString()
        {
            return StringUtility.Format("Modbus write single coil request\nFunction code: {0}\nCoil address: {1}\nCoil state to set: {2}",
                FunctionCode, _coilAddress, _coilStateToSet);
        }

        protected override void GetFromBytes(byte[] data, int index)
        {
            if (index > data.Length - ByteDataLength)
                throw new ArgumentException("Byte data too short");

            if (data[index] != FunctionCode)
                throw new ArgumentException("Wrong function data.");

            byte[] coilAddressBytes = new byte[sizeof(ushort)];
            byte[] coilStateToSetBytes = new byte[sizeof(ushort)];

            Array.Copy(data, index + 1, coilAddressBytes, 0, sizeof(ushort));
            Array.Copy(data, index + 3, coilStateToSetBytes, 0, sizeof(ushort));

            if (BitConverter.IsLittleEndian)
            {
                coilAddressBytes.ReverseBytes();
                coilStateToSetBytes.ReverseBytes();
            }

            _coilAddress = BitConverter.ToUInt16(coilAddressBytes, 0);

            switch (BitConverter.ToUInt16(coilStateToSetBytes, 0))
            {
                case _coilOnValue:
                    _coilStateToSet = true;
                    break;
                case _coilOffValue:
                    _coilStateToSet = false;
                    break;
                default:
                    throw new ModbusIllegalDataValueException();
            }
        }
    }
}
