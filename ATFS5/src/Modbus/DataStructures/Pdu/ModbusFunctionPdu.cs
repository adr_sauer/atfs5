using System;
using Microsoft.SPOT;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    abstract class ModbusFunctionPdu : ModbusPdu
    {
        public abstract byte FunctionCode { get; }

        public static ModbusFunctionPdu GetRequestPduFromBytes(byte[] data, int index)
        {
            int functionCode = data[index];

            ModbusFunctionPdu pdu = null;


            if (functionCode == Function03ReadHoldingRegistersRequestPdu.FunctionCodeOfPduType)
                pdu = new Function03ReadHoldingRegistersRequestPdu(data, index);
            else if (functionCode == Function04ReadInputRegistersRequestPdu.FunctionCodeOfPduType)
                pdu = new Function04ReadInputRegistersRequestPdu(data, index);
            else if (functionCode == Function05WriteSingleCoilRequestPdu.FunctionCodeOfPduType)
                pdu = new Function05WriteSingleCoilRequestPdu(data, index);
            else if (functionCode == Function16WriteMultipleRegistersRequestPdu.FunctionCodeOfPduType)
                pdu = new Function16WriteMultipleRegistersRequestPdu(data, index);
            else
                throw new ModbusIllegalFunctionException();

            return pdu;
        }
    }
}
