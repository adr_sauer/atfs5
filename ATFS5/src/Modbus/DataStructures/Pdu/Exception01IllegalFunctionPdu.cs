using System;
using Microsoft.SPOT;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    class Exception01IllegalFunctionPdu : ModbusExceptionPdu
    {
        public const byte ExceptionCodeOfPduType = 1;

        public override byte ExceptionCode
        {
            get { return ExceptionCodeOfPduType; }
        }

        public Exception01IllegalFunctionPdu(byte originalFunctionCode)
            : base(originalFunctionCode)
        {
        }

        public Exception01IllegalFunctionPdu(byte[] data, int index = 0)
            : base(data, index)
        {
        }
    }
}
