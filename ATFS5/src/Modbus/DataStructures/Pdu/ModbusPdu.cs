using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus;

namespace ATFS5.src.Modbus.DataStructures.Pdu
{
    abstract class ModbusPdu
    {
        private const int _maximumFunctionCodeNotIndicatingException = 128;

        public static int MaximumFunctionCodeNotIndicatingException
        {
            get { return _maximumFunctionCodeNotIndicatingException; }
        }

        public abstract int ByteDataLength { get; }

        public byte[] GetBytes()
        {
            byte[] bytes = new byte[ByteDataLength];
            WriteIntoByteTable(bytes, 0);
            return bytes;
        }

        public abstract void WriteIntoByteTable(byte[] buffer, int index);

        protected abstract void GetFromBytes(byte[] data, int index);

        public static ModbusPdu GetResponseFromBytes(byte[] data, int index)
        {
            int functionCode = data[index];

            ModbusPdu pdu = null;

            if (functionCode > _maximumFunctionCodeNotIndicatingException)
            {
                int exceptionCode = data[index + 1];
                if (exceptionCode == Exception01IllegalFunctionPdu.ExceptionCodeOfPduType)
                    pdu = new Exception01IllegalFunctionPdu(data, index);
                else if (exceptionCode == Exception02IllegalDataAddressPdu.ExceptionCodeOfPduType)
                    pdu = new Exception02IllegalDataAddressPdu(data, index);
                else if (exceptionCode == Exception03IllegalDataValuePdu.ExceptionCodeOfPduType)
                    pdu = new Exception03IllegalDataValuePdu(data, index);
                else if (exceptionCode == Exception04ServerDeviceFailurePdu.ExceptionCodeOfPduType)
                    pdu = new Exception04ServerDeviceFailurePdu(data, index);
                else if (exceptionCode == Exception0AGatewayPathUnavailablePdu.ExceptionCodeOfPduType)
                    pdu = new Exception0AGatewayPathUnavailablePdu(data, index);
                else if (exceptionCode == Exception0BGatewayTargetDeviceFailedToRespondPdu.ExceptionCodeOfPduType)
                    pdu = new Exception0BGatewayTargetDeviceFailedToRespondPdu(data, index);
                else
                    throw new ModbusIllegalFunctionException();
            }
            else
            {
                if (functionCode == Function03ReadHoldingRegistersResponsePdu.FunctionCodeOfPduType)
                    pdu = new Function03ReadHoldingRegistersResponsePdu(data, index);
                else if (functionCode == Function04ReadInputRegistersResponsePdu.FunctionCodeOfPduType)
                    pdu = new Function04ReadInputRegistersResponsePdu(data, index);
                else if (functionCode == Function05WriteSingleCoilResponsePdu.FunctionCodeOfPduType)
                    pdu = new Function05WriteSingleCoilResponsePdu(data, index);
                else if (functionCode == Function16WriteMultipleRegistersResponsePdu.FunctionCodeOfPduType)
                    pdu = new Function16WriteMultipleRegistersResponsePdu(data, index);
                else
                    throw new ModbusIllegalFunctionException();
            }

            return pdu;
        }



    }
}
