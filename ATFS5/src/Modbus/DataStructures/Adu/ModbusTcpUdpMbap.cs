using System;
using Microsoft.SPOT;
using ATFS5.src.UtilitiesLib;
using NetMf.CommonExtensions;

namespace ATFS5.src.Modbus.DataStructures.Adu
{
    class ModbusTcpUdpMbap
    {
        private const int _mbapBytesLength = 7;
        private const int _modbusProtocolId = 0;

        private ushort _transactionId;
        private ushort _protocolId;
        private ushort _pduBytesLength;
        private byte _unitId;

        public ushort TransactionId
        {
            get { return _transactionId; }
            protected set { _transactionId = value; }
        }       

        public ushort ProtocolId
        {
            get { return _protocolId; }
            protected set { _protocolId = value; }
        }        

        public ushort PduBytesLength
        {
            get { return _pduBytesLength; }
            protected set { _pduBytesLength = value; }
        }        

        public byte UnitId
        {
            get { return _unitId; }
            protected set { _unitId = value; }
        }

        public static int ModbusProtocolId
        {
            get { return _modbusProtocolId; }
        } 


        public static int MbapBytesLength
        {
            get { return _mbapBytesLength;}
        }

        

        public ModbusTcpUdpMbap(ushort transactionId, ushort protocolId, ushort pduBytesLength, byte unitId)
        {
            TransactionId = transactionId;
            ProtocolId = protocolId;
            PduBytesLength = pduBytesLength;
            UnitId = unitId;
        }

        public ModbusTcpUdpMbap(byte[] byteData, int index = 0)
        {
            GetMbapDataFromBytes(byteData, index);
        }

        public byte[] GetBytes()
        {
            byte[] ret = new byte[MbapBytesLength];
            WriteToByteTable(ret, 0);
            return ret;
        }

        public void WriteToByteTable(byte[] buffer, int index)
        {
            if (index > buffer.Length - MbapBytesLength)
                throw new ArgumentException("Buffer too short");
            else
            {                
                byte[] transactionIdBytes = BitConverter.GetBytes(TransactionId);
                byte[] protocolIdBytes = BitConverter.GetBytes(ProtocolId);
                byte[] pduBytesLenghtBytes = BitConverter.GetBytes(PduBytesLength);
                if (BitConverter.IsLittleEndian)
                {
                    //Modbus protocol requires BigEndian
                    transactionIdBytes.ReverseBytes();
                    protocolIdBytes.ReverseBytes();
                    pduBytesLenghtBytes.ReverseBytes();
                }

                buffer[index + 0] = transactionIdBytes[0];
                buffer[index + 1] = transactionIdBytes[1];
                buffer[index + 2] = protocolIdBytes[0];
                buffer[index + 3] = protocolIdBytes[1];
                buffer[index + 4] = pduBytesLenghtBytes[0];
                buffer[index + 5] = pduBytesLenghtBytes[1];
                buffer[index + 6] = UnitId;

            }
        }

        public override string ToString()
        {
            return StringUtility.Format("Modbus TCP/UDP MBAP\nTransaction ID: {0}\nProtocol ID: {1}\nPDU bytes following: {2}\nUnit ID: {3}",
                TransactionId, ProtocolId, PduBytesLength, UnitId);
        }

       
        private void GetMbapDataFromBytes(byte[] byteData, int index)
        {
            if (index > byteData.Length - MbapBytesLength)
                throw new ArgumentException("Byte data too short");
            else
            {
                //Done like that because there is an issue in BitConverter with startIndex.
                //https://netmf.codeplex.com/workitem/2288

                byte[] transactionIdBytes=new byte[sizeof(ushort)];
                byte[] protocolIdBytes=new byte[sizeof(ushort)];
                byte[] pduBytesLenghtBytes = new byte[sizeof(ushort)];

                transactionIdBytes[0] = byteData[index + 0];
                transactionIdBytes[1] = byteData[index + 1];
                protocolIdBytes[0] = byteData[index + 2];
                protocolIdBytes[1] = byteData[index + 3];
                pduBytesLenghtBytes[0] = byteData[index + 4];
                pduBytesLenghtBytes[1] = byteData[index + 5];

                if (BitConverter.IsLittleEndian)
                {
                    transactionIdBytes.ReverseBytes();
                    protocolIdBytes.ReverseBytes();
                    pduBytesLenghtBytes.ReverseBytes();
                }
                TransactionId = BitConverter.ToUInt16(transactionIdBytes, 0);
                ProtocolId = BitConverter.ToUInt16(protocolIdBytes, 0);
                PduBytesLength = BitConverter.ToUInt16(pduBytesLenghtBytes, 0);
                UnitId = byteData[index + 6];
            }
        }

    }
}
