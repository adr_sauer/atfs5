using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;

namespace ATFS5.src.Modbus
{
    class ModbusIllegalFunctionException : ModbusException
    {
        public override DataStructures.Pdu.ModbusExceptionPdu GetAppropriateExceptionPdu(byte requestFunctionCode)
        {
            return new Exception01IllegalFunctionPdu(requestFunctionCode);
        }
    }
}
