using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;

namespace ATFS5.src.Modbus
{
    class ModbusServerDeviceFailureException : ModbusException
    {
        public override DataStructures.Pdu.ModbusExceptionPdu GetAppropriateExceptionPdu(byte requestFunctionCode)
        {
            return new Exception04ServerDeviceFailurePdu(requestFunctionCode);
        }
    }
}
