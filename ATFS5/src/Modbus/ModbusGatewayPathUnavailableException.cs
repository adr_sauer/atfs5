using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;

namespace ATFS5.src.Modbus
{
    class ModbusGatewayPathUnavailableException : ModbusException
    {
        public override DataStructures.Pdu.ModbusExceptionPdu GetAppropriateExceptionPdu(byte requestFunctionCode)
        {
            return new Exception0AGatewayPathUnavailablePdu(requestFunctionCode);
        }
    }
}
