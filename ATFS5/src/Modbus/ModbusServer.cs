using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;

namespace ATFS5.src.Modbus
{
    abstract class ModbusServer
    {
        private ModbusData _modbusServerData;

        protected ModbusData Data
        {
            get
            {
                return _modbusServerData;
            }
        }

        public ModbusServer(ModbusData data)
        {
            _modbusServerData = data;
        }

        public abstract void StartModbusServer();


        protected ModbusPdu GetResponseToValidRequest(ModbusFunctionPdu requestPdu)
        {
            ModbusPdu responsePdu = null;
            if (requestPdu is Function03ReadHoldingRegistersRequestPdu)
                responsePdu = GetResponseToFunction03ReadHoldingRegisters(requestPdu as Function03ReadHoldingRegistersRequestPdu);
            else if (requestPdu is Function04ReadInputRegistersRequestPdu)
                responsePdu = GetResponseToFunction04ReadInputRegisters(requestPdu as Function04ReadInputRegistersRequestPdu);
            else if (requestPdu is Function05WriteSingleCoilRequestPdu)
                responsePdu = GetResponseToFunction05WriteSingleCoil(requestPdu as Function05WriteSingleCoilRequestPdu);
            else if (requestPdu is Function16WriteMultipleRegistersRequestPdu)
                responsePdu = GetResponseToFunction16WriteMultipleRegisters(requestPdu as Function16WriteMultipleRegistersRequestPdu);
            else
                responsePdu = new Exception01IllegalFunctionPdu(requestPdu.FunctionCode);
            return responsePdu;
        }

        private ModbusPdu GetResponseToFunction03ReadHoldingRegisters(Function03ReadHoldingRegistersRequestPdu requestPdu)
        {
            try
            {
                return new Function03ReadHoldingRegistersResponsePdu(
                    _modbusServerData.ReadHoldingRegisters(requestPdu.StartAddress, requestPdu.RegistersQuantity));
            }
            catch (ModbusException ex)
            {
                return ex.GetAppropriateExceptionPdu(requestPdu.FunctionCode);
            }
            catch (Exception)
            {
                return new Exception04ServerDeviceFailurePdu(requestPdu.FunctionCode);
            }
        }

        private ModbusPdu GetResponseToFunction04ReadInputRegisters(Function04ReadInputRegistersRequestPdu requestPdu)
        {
            try
            {
                return new Function04ReadInputRegistersResponsePdu(
                    _modbusServerData.ReadInputRegisters(requestPdu.StartAddress, requestPdu.RegistersQuantity));
            }
            catch (ModbusException ex)
            {
                return ex.GetAppropriateExceptionPdu(requestPdu.FunctionCode);
            }
            catch (Exception)
            {
                return new Exception04ServerDeviceFailurePdu(requestPdu.FunctionCode);
            }
        }

        private ModbusPdu GetResponseToFunction05WriteSingleCoil(Function05WriteSingleCoilRequestPdu requestPdu)
        {
            //HACK
            return new Function05WriteSingleCoilResponsePdu(requestPdu.CoilAddress, requestPdu.CoilStateToSet);
        }

        private ModbusPdu GetResponseToFunction16WriteMultipleRegisters(Function16WriteMultipleRegistersRequestPdu requestPdu)
        {
            //HACK
            return new Function16WriteMultipleRegistersResponsePdu(requestPdu.StartAddress, requestPdu.RegistersQuantity);
        }
    }
}
