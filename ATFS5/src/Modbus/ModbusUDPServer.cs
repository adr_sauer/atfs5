using System;
using Microsoft.SPOT;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using ATFS5.src.UtilitiesLib;
using ATFS5.src.Modbus.DataStructures.Adu;
using ATFS5.src.Modbus.DataStructures.Pdu;

namespace ATFS5.src.Modbus
{
    class ModbusUDPServer : ModbusServer
    {
        private Thread _modbusServerThread;
        private StopFlag _modbusServerThreadStopFlag;
        private Socket _serverSocket;
        private const int _modbusNetworkPort = 502;
        private string _authorizedClientAddress = "127.0.0.1";
        

        public ModbusUDPServer(ModbusData data)
            : base(data)
        {
            _modbusServerThread = new Thread(ServerThreadLoop);
            _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _modbusServerThreadStopFlag = new StopFlag();
        }

        public override void StartModbusServer()
        {
            EndPoint bindingEndPoint = new IPEndPoint(IPAddress.Any, _modbusNetworkPort);
            _serverSocket.Bind(bindingEndPoint);
            _modbusServerThread.Start();
        }


        public void StopModbusServer()
        {
            _modbusServerThreadStopFlag.Stop();
            _modbusServerThread.Join();
            _serverSocket.Close();
        }

        private void ServerThreadLoop()
        {
            try
            {
                while (_modbusServerThreadStopFlag.KeepWorking)
                {
                    if (_serverSocket.Poll(-1, SelectMode.SelectRead))
                    {
                        EndPoint remoteEndPoint=new IPEndPoint(IPAddress.Any,_modbusNetworkPort);
                        byte[] inBuffer = new byte[_serverSocket.Available];
                        int count = _serverSocket.ReceiveFrom(inBuffer, ref remoteEndPoint);
                        byte[] udpAduBytes = new byte[count];
                        Array.Copy(inBuffer, udpAduBytes, count);
                        if (GetIpAddressStringFromEndpoint(remoteEndPoint) == _authorizedClientAddress)
                        {
                            ProcessUdpRequestBytes(udpAduBytes, remoteEndPoint);
                        }
                        else
                        {
                            //Ignore message from unauthorized client.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.HandleUnforeseenException(ex);
            }
        }

        private void ProcessUdpRequestBytes(byte[] udpAduBytes,EndPoint replyToEndpoint)
        {
            try
            {
                ModbusTcpUdpMbap requestMbap = new ModbusTcpUdpMbap(udpAduBytes, 0);
                if (requestMbap.ProtocolId != ModbusTcpUdpMbap.ModbusProtocolId)
                    throw new InvalidOperationException("Wrong protocol ID in MBAP");

                if(udpAduBytes.Length != (ModbusTcpUdpMbap.MbapBytesLength+requestMbap.PduBytesLength))
                    throw new ArgumentException("Datagram data length error.");

                if(requestMbap.PduBytesLength == 0)
                    throw new ArgumentException("PDU lenght is 0.");

                ModbusFunctionPdu requestPdu=null;
                ModbusPdu responsePdu = null;
                try
                {
                    requestPdu = ModbusFunctionPdu.GetRequestPduFromBytes(udpAduBytes, ModbusTcpUdpMbap.MbapBytesLength);
                }
                catch (ModbusException ex)
                {
                    responsePdu=ex.GetAppropriateExceptionPdu(udpAduBytes[ModbusTcpUdpMbap.MbapBytesLength]);
                }
                catch 
                {
                    throw;
                }
                                
                
                if (responsePdu == null)
                {
                    responsePdu = GetResponseToValidRequest(requestPdu);
                }

                if (responsePdu != null)
                {
                    SendResponse(responsePdu,replyToEndpoint,requestMbap);
                }
            }
            catch (Exception ex)
            {
                Utilities.ProgramLogger.LogError("Error while processing UDP modbus request bytes");
                Utilities.ProgramLogger.LogException(ex);
            }
        }

        private void SendResponse(ModbusPdu responsePdu, EndPoint replyToEndpoint2,ModbusTcpUdpMbap requestMbap)
        {
            using (Socket responseSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                ModbusTcpUdpMbap responseMbap = new ModbusTcpUdpMbap(requestMbap.TransactionId, requestMbap.ProtocolId,
                    (ushort)responsePdu.ByteDataLength, requestMbap.UnitId);
                byte[] udpResponseBytes = new byte[ModbusTcpUdpMbap.MbapBytesLength + responsePdu.ByteDataLength];
                responseMbap.WriteToByteTable(udpResponseBytes, 0);
                responsePdu.WriteIntoByteTable(udpResponseBytes, ModbusTcpUdpMbap.MbapBytesLength);
                EndPoint replyToEndpoint = new IPEndPoint(IPAddress.Parse(_authorizedClientAddress), _modbusNetworkPort);
                responseSocket.SendTo(udpResponseBytes, replyToEndpoint);
            }
        }

        

        private static string GetIpAddressStringFromEndpoint(EndPoint endpoint)
        {
            return endpoint.ToString().Split(':')[0];
        }

    }
}
