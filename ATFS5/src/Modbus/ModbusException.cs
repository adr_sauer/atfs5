using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;

namespace ATFS5.src.Modbus
{
    abstract class ModbusException : SystemException
    {
        public abstract ModbusExceptionPdu GetAppropriateExceptionPdu(byte requestFunctionCode);
    }
}
