using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;

namespace ATFS5.src.Modbus
{
    class ModbusGatewayTargetDeviceFailedToRespondException : ModbusException
    {
        public override ModbusExceptionPdu GetAppropriateExceptionPdu(byte requestFunctionCode)
        {
            return new Exception0BGatewayTargetDeviceFailedToRespondPdu(requestFunctionCode);
        }
    }
}
