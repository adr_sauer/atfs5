using System;
using Microsoft.SPOT;

namespace ATFS5.src.Modbus
{
    abstract class ModbusData
    {
        
        public abstract ushort ReadInputRegister(ushort address);
        public abstract ushort[] ReadInputRegisters(ushort startAddress, ushort quantity);
        public abstract ushort ReadHoldingRegister(ushort address);
        public abstract ushort[] ReadHoldingRegisters(ushort startAddress, ushort quantity);
        public abstract void WriteToHoldingRegister(ushort address, ushort value);
        public abstract void WriteToHoldingRegisters(ushort startAddress, ushort[] values);
        public abstract bool WriteToCoil(ushort address, bool value);
        public abstract void WriteToCoils(ushort startAddress, bool[] values);

    }
}
