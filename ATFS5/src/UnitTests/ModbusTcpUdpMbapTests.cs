using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Adu;
using MFUnit;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.UnitTests
{
#if UNIT_TESTING
    class ModbusTcpUdpMbapTests
    {
        /*
        public void TestMbapFromBytes1()
        {
            //Transaction ID: 2
            //Protocol Id=0
            //PDU lenght=6
            //Unit ID=1
            //Byte table lenght equal to MBAP lenght
            byte[] bytes = { 0x00, 0x02, 0x00, 0x00, 0x00, 0x06, 0x01 };
            ModbusTcpUdpMbap mbap = new ModbusTcpUdpMbap(bytes);
            Assert.IsTrue((mbap.TransactionId == 2) && (mbap.ProtocolId == 0) && (mbap.PduBytesLength == 6) && (mbap.UnitId == 1));
        }

        public void TestMbapFromBytes2()
        {
            //Transaction ID: 2
            //Protocol Id=0
            //PDU lenght=6
            //Unit ID=1
            //Byte table lenght longer than MBAP lenght
            byte[] bytes = { 0x00, 0x02, 0x00, 0x00, 0x00, 0x06, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            ModbusTcpUdpMbap mbap = new ModbusTcpUdpMbap(bytes);
            Assert.IsTrue((mbap.TransactionId == 2) && (mbap.ProtocolId == 0) && (mbap.PduBytesLength == 6) && (mbap.UnitId == 1));
        }

        public void TestMbapByteDataTooShort()
        {
            //6 bytes
            byte[] bytes = { 0x00, 0x02, 0x00, 0x00, 0x00, 0x06 };
            ModbusTcpUdpMbap mbap;

            Assert.Throws(() => { mbap = new ModbusTcpUdpMbap(bytes); }, typeof(ArgumentException));
        }

        public void TestMbapByteWriteBufferTooShort()
        {
            //Transaction ID: 2
            //Protocol Id=0
            //PDU lenght=6
            //Unit ID=1
            ModbusTcpUdpMbap mbap = new ModbusTcpUdpMbap(2,0,6,1);
            byte[] bytes = new byte[6];

            Assert.Throws(() => { mbap.WriteToByteTable(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestMbapWriteToBytes()
        {
            //Transaction ID: 2
            //Protocol Id=0
            //PDU lenght=6
            //Unit ID=1
            ModbusTcpUdpMbap mbap = new ModbusTcpUdpMbap(2, 0, 6, 1);
            byte[] bytes = new byte[7];
            byte[] expectedBytes = { 0x00, 0x02, 0x00, 0x00, 0x00, 0x06, 0x01 };
            mbap.WriteToByteTable(bytes, 0);
            Assert.IsTrue(bytes.CompareByteArray(expectedBytes));
        }
        */
    }
#endif
}
