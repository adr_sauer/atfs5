using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;
using ATFS5.src.Modbus;
using MFUnit;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.UnitTests
{
#if UNIT_TESTING
    class ModbusFunction16RequestPduTests
    {
        /*
        public void TestValidPduBytesGeneration1()
        {
            const byte functionCode = 16;
            const ushort startAddress = 1; //0x0001
            ushort[] registersValues = { 10, 258 }; // 0x000A, 0x0102
            ushort registersQuantity = (ushort)registersValues.Length; // 2 = 0x0002
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] expectedBytes = { functionCode, 0x00, 0x01, 0x00, 0x02, byteCount, 0x00, 0x0A, 0x01, 0x02 };

            var pdu = new Function16WriteMultipleRegistersRequestPdu(startAddress, registersValues);
            byte[] resultBytes = pdu.GetBytes();

            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestValidPduBytesGeneration2()
        {
            const byte functionCode = 16;
            const ushort startAddress = 1; //0x0001
            ushort[] registersValues = { 10, 258, 421, 0 }; // 0x000A, 0x0102, 0x01A5, 0x0000
            ushort registersQuantity = (ushort)registersValues.Length; // 4 = 0x0004
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] expectedBytes = { functionCode, 0x00, 0x01, 0x00, 0x04, byteCount, 0x00, 0x0A, 0x01, 0x02, 0x01, 0xA5, 0x00, 0x00 };

            var pdu = new Function16WriteMultipleRegistersRequestPdu(startAddress, registersValues);
            byte[] resultBytes = pdu.GetBytes();

            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestWritingPduBytesIntoBufferWithOffset()
        {
            const byte functionCode = 16;
            const ushort startAddress = 1; //0x0001
            ushort[] registersValues = { 10, 258 }; // 0x000A, 0x0102
            ushort registersQuantity = (ushort)registersValues.Length; // 2 = 0x0002
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] expectedBytes = { 0x00,functionCode, 0x00, 0x01, 0x00, 0x02, byteCount, 0x00, 0x0A, 0x01, 0x02 };
            byte[] resultBytes = new byte[expectedBytes.Length];

            var pdu = new Function16WriteMultipleRegistersRequestPdu(startAddress, registersValues);
            pdu.WriteIntoByteTable(resultBytes, 1);

            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestPduGenerationFromValidBytes1()
        {
            const byte functionCode = 16;
            const ushort startAddress = 1; //0x0001
            ushort[] registersValues = { 10, 258 }; // 0x000A, 0x0102
            ushort registersQuantity = (ushort)registersValues.Length; // 2 = 0x0002
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] bytes = { functionCode, 0x00, 0x01, 0x00, 0x02, byteCount, 0x00, 0x0A, 0x01, 0x02 };

            var pdu = new Function16WriteMultipleRegistersRequestPdu(bytes);

            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.RegistersQuantity == registersQuantity) &&
                (pdu.StartAddress == startAddress) &&
                (pdu[0] == registersValues[0]) && (pdu[1] == registersValues[1]));
        }

        public void TestPduGenerationFromValidBytes2()
        {
            const byte functionCode = 16;
            const ushort startAddress = 1; //0x0001
            ushort[] registersValues = { 10, 258, 421, 0 }; // 0x000A, 0x0102, 0x01A5, 0x0000
            ushort registersQuantity = (ushort)registersValues.Length; // 4 = 0x0004
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] bytes = { functionCode, 0x00, 0x01, 0x00, 0x04, byteCount, 0x00, 0x0A, 0x01, 0x02, 0x01, 0xA5, 0x00, 0x00 };

            var pdu = new Function16WriteMultipleRegistersRequestPdu(bytes);

            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.RegistersQuantity == registersQuantity) &&
                (pdu.StartAddress == startAddress) &&
                (pdu[0] == registersValues[0]) && (pdu[1] == registersValues[1]) && (pdu[2] == registersValues[2]) && (pdu[3] == registersValues[3]));
        }

        public void TestPduGenerationFromValidBytesWithOffset()
        {
            const byte functionCode = 16;
            const ushort startAddress = 1; //0x0001
            ushort[] registersValues = { 10, 258 }; // 0x000A, 0x0102
            ushort registersQuantity = (ushort)registersValues.Length; // 2 = 0x0002
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] bytes = {0x00, functionCode, 0x00, 0x01, 0x00, 0x02, byteCount, 0x00, 0x0A, 0x01, 0x02 };

            var pdu = new Function16WriteMultipleRegistersRequestPdu(bytes,1);

            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.RegistersQuantity == registersQuantity) &&
                (pdu.StartAddress == startAddress) &&
                (pdu[0] == registersValues[0]) && (pdu[1] == registersValues[1]));
        }

        public void TestPduGenerationFromTooShortByteData()
        {
            const byte functionCode = 16;
            byte[] bytes = { functionCode, 0x00, 0x01, 0x00, 0x02, };

            Assert.Throws(() => { new Function16WriteMultipleRegistersRequestPdu(bytes, 0); }, typeof(ArgumentException));
        }

        

        public void TestPduConstructionWithTooHighRegisterCount()
        {
            const ushort startAddress = 1; //0x0001
            ushort[] registerValues = new ushort[124];

            Assert.Throws(() => { new Function16WriteMultipleRegistersRequestPdu(startAddress,registerValues); }, typeof(ModbusIllegalDataValueException));
        }

        public void TestPduConstructionWithTooLowRegisterCount()
        {
            const ushort startAddress = 1; //0x0001
            ushort[] registerValues = new ushort[0];

            Assert.Throws(() => { new Function16WriteMultipleRegistersRequestPdu(startAddress, registerValues); }, typeof(ModbusIllegalDataValueException));
        }

        public void WriteBytesIntoTooShortBuffer()
        {
            const ushort startAddress = 1; //0x0001
            ushort[] registersValues = { 10, 258 }; // 0x000A, 0x0102



            var pdu = new Function16WriteMultipleRegistersRequestPdu(startAddress, registersValues);
            byte[] buffer = new byte[pdu.ByteDataLength - 1];

            Assert.Throws(() => { pdu.WriteIntoByteTable(buffer, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromTooShortByteData1()
        {
            const byte functionCode = 16;            
            byte[] bytes = { functionCode, 0x00, 0x01, 0x00, 0x02 };

            Assert.Throws(() => { new Function16WriteMultipleRegistersRequestPdu(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromTooShortByteData2()
        {
            const byte functionCode = 16;
            ushort[] registersValues = { 10, 258 }; // 0x000A, 0x0102
            ushort registersQuantity = (ushort)registersValues.Length; // 2 = 0x0002
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] bytes = { functionCode, 0x00, 0x01, 0x00, 0x02, byteCount, 0x00, 0x0A, 0x01};

            Assert.Throws(() => { new Function16WriteMultipleRegistersRequestPdu(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithWrongFunctionCode()
        {
            const byte wrongFunctionCode = 15;
            //const ushort startAddress = 1; //0x0001
            ushort[] registersValues = { 10, 258 }; // 0x000A, 0x0102
            ushort registersQuantity = (ushort)registersValues.Length; // 2 = 0x0002
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] bytes = { wrongFunctionCode, 0x00, 0x01, 0x00, 0x02, byteCount, 0x00, 0x0A, 0x01, 0x02 };

            Assert.Throws(() => { new Function16WriteMultipleRegistersRequestPdu(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithTooLowRegisterCount()
        {
            const byte functionCode = 16;
            //const ushort startAddress = 1; //0x0001
            ushort[] registersValues = {  }; // 0x000A, 0x0102
            ushort registersQuantity = (ushort)registersValues.Length; // 2 = 0x0002
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] bytes = { functionCode, 0x00, 0x01, 0x00, 0x02, byteCount};

            Assert.Throws(() => { new Function16WriteMultipleRegistersRequestPdu(bytes, 0); }, typeof(ModbusIllegalDataValueException));
        }

        public void TestPduGenerationFromByteDataWithTooHighRegisterCount()
        {
            const byte functionCode = 16;
            //const ushort startAddress = 1; //0x0001
            ushort[] registersValues = new ushort[124]; // 0x000A, 0x0102
            ushort registersQuantity = (ushort)registersValues.Length; // 2 = 0x0002
            byte byteCount = (byte)(registersQuantity * 2);
            byte[] bytes = new byte[6 + byteCount];
            bytes[0] = functionCode;
            bytes[1] = 0x00;
            bytes[2] = 0x01;
            bytes[3] = 0x00;
            bytes[4] = 0x7C;
            bytes[5] = byteCount;

            Assert.Throws(() => { new Function16WriteMultipleRegistersRequestPdu(bytes, 0); }, typeof(ModbusIllegalDataValueException));
        }

        public void TestPduGenerationFromByteDataWithByteCountNotEqualToQuantityTimes2()
        {
            const byte functionCode = 16;
            const ushort startAddress = 1; //0x0001
            ushort[] registersValues = { 10, 258 }; // 0x000A, 0x0102
            ushort registersQuantity = (ushort)registersValues.Length; // 2 = 0x0002
            byte byteCount = (byte)((registersQuantity * 2)+1);
            byte[] bytes = { functionCode, 0x00, 0x01, 0x00, 0x02, byteCount, 0x00, 0x0A, 0x01, 0x02,0x00 };


            Assert.Throws(() => { new Function16WriteMultipleRegistersRequestPdu(bytes, 0); }, typeof(ModbusIllegalDataValueException));
        }
        */
    }
#endif
}
