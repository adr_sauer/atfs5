using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;
using ATFS5.src.Modbus;
using MFUnit;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.UnitTests
{
    #if UNIT_TESTING
    class ModbusFunction05RequestPduTests
    {
        /*
        public void TestValidPduBytesGeneration1()
        {
            const byte functionCode = 5;
            const ushort coilAddress = 172;
            const bool coilStateToSet = false;
            //Coil address= 172=0x00AC
            //Coil state to set= false=0x0000

            var pdu = new Function05WriteSingleCoilRequestPdu(coilAddress, coilStateToSet);
            byte[] expectedBytes = { functionCode, 0x00, 0xAC, 0x00, 0x00 };
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestValidPduBytesGeneration2()
        {
            const byte functionCode = 5;
            const ushort coilAddress = 172;
            const bool coilStateToSet = true;
            //Coil address= 172=0x00AC
            //Coil state to set= true=0xFF00

            var pdu = new Function05WriteSingleCoilRequestPdu(coilAddress, coilStateToSet);
            byte[] expectedBytes = { functionCode, 0x00, 0xAC, 0xFF, 0x00 };
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestValidPduBytesGeneration3()
        {
            const byte functionCode = 5;
            const ushort coilAddress = 300;
            const bool coilStateToSet = true;
            //Coil address= 300=0x012C
            //Coil state to set= true=0xFF00

            var pdu = new Function05WriteSingleCoilRequestPdu(coilAddress, coilStateToSet);
            byte[] expectedBytes = { functionCode, 0x01, 0x2C, 0xFF, 0x00 };
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestWritingPduBytesIntoBufferWithOffset()
        {
            const byte functionCode = 5;
            const ushort coilAddress = 300;
            const bool coilStateToSet = true;
            //Coil address= 300=0x012C
            //Coil state to set= true=0xFF00

            var pdu = new Function05WriteSingleCoilRequestPdu(coilAddress, coilStateToSet);
            byte[] expectedBytes = {0x00,0x00, functionCode, 0x01, 0x2C, 0xFF, 0x00 };
            byte[] resultBytes = new byte[7];
            pdu.WriteIntoByteTable(resultBytes, 2);
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestPduGenerationFromValidBytes1()
        {
            const byte functionCode = 5;
            const ushort coilAddress = 172;
            const bool coilStateToSet = true;
            //Coil address= 172=0x00AC
            //Coil state to set= true=0xFF00

            byte[] bytes = { functionCode, 0x00, 0xAC, 0xFF, 0x00 };
            var pdu = new Function05WriteSingleCoilRequestPdu(bytes);
            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.CoilAddress == coilAddress) && (pdu.CoilStateToSet == coilStateToSet));
        }

        public void TestPduGenerationFromValidBytes2()
        {
            const byte functionCode = 5;
            const ushort coilAddress = 172;
            const bool coilStateToSet = false;
            //Coil address= 172=0x00AC
            //Coil state to set= false=0x0000

            byte[] bytes = { functionCode, 0x00, 0xAC, 0x00, 0x00 };
            var pdu = new Function05WriteSingleCoilRequestPdu(bytes);
            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.CoilAddress == coilAddress) && (pdu.CoilStateToSet == coilStateToSet));
        }

        public void TestPduGenerationFromValidBytesWithOffset()
        {
            const byte functionCode = 5;
            const ushort coilAddress = 172;
            const bool coilStateToSet = true;
            //Coil address= 172=0x00AC
            //Coil state to set= true=0xFF00

            byte[] bytes = {0x00, functionCode, 0x00, 0xAC, 0xFF, 0x00 };
            var pdu = new Function05WriteSingleCoilRequestPdu(bytes,1);
            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.CoilAddress == coilAddress) && (pdu.CoilStateToSet == coilStateToSet));
        }

        public void TestPduGenerationFromTooShortByteData()
        {
            const byte functionCode = 5;
            //Coil address= 172=0x00AC
            //Coil state to set= true=0xFF00

            byte[] bytes = {functionCode, 0x00, 0xAC, 0xFF, 0x00 };
            
            Assert.Throws(() => { new Function05WriteSingleCoilRequestPdu(bytes, 1); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithWrongFunctionCode()
        {
            const byte wrongfunctionCode = 3;
            //Coil address= 172=0x00AC
            //Coil state to set= true=0xFF00

            byte[] bytes = { wrongfunctionCode, 0x00, 0xAC, 0xFF, 0x00 };

            Assert.Throws(() => { new Function05WriteSingleCoilRequestPdu(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithWrongCoilStateValue()
        {
            const byte functionCode = 5;
            //Coil address= 172=0x00AC
            //Wrong coil state to set= 0x5311

            byte[] bytes = { functionCode, 0x00, 0xAC, 0x53, 0x11 };

            Assert.Throws(() => { new Function05WriteSingleCoilRequestPdu(bytes, 0); }, typeof(ModbusIllegalDataValueException));
        }
        */
    }
    #endif
}
