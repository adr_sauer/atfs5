using System;
using Microsoft.SPOT;
using ATFS5.src.UtilitiesLib;
using MFUnit;

namespace ATFS5.src.UnitTests
{
#if UNIT_TESTING
    class ByteArrayExtensionsTests
    {
        /*
        public void TestCompareNullArray()
        {
            byte[] bytes1 = null;
            byte[] bytes2 = { };
            Assert.Throws(() => { bytes1.CompareByteArray(bytes2); }, typeof(ArgumentNullException));
        }

        public void TestCompareToNullArray()
        {
            byte[] bytes1 = { };
            byte[] bytes2 = null;
            Assert.Throws(() => { bytes1.CompareByteArray(bytes2); }, typeof(ArgumentNullException));
        }

        public void TestCompareDifferentLenghtsArrays()
        {
            byte[] bytes1 = { };
            byte[] bytes2 = { 1 };
            Assert.IsFalse(bytes1.CompareByteArray(bytes2));
        }

        public void TestCompareSameLenghtsDifferentArrays()
        {
            byte[] bytes1 = { 1, 2 };
            byte[] bytes2 = { 2, 3 };
            Assert.IsFalse(bytes1.CompareByteArray(bytes2));
        }

        public void TestCompareSameArrays()
        {
            byte[] bytes1 = { 1, 2, 3 };
            byte[] bytes2 = { 1, 2, 3 };
            Assert.IsTrue(bytes1.CompareByteArray(bytes2));
        }


        public void TestReverseNullArray()
        {
            byte[] bytes = null;
            Assert.Throws(() => { bytes.ReverseBytes(); }, typeof(ArgumentNullException));

        }

        public void TestReverseZeroLenghtArray()
        {
            byte[] testBytes = { };
            byte[] expectedResultBytes = { };
            testBytes.ReverseBytes();
            Assert.IsTrue(testBytes.CompareByteArray(expectedResultBytes));
        }

        public void TestReverseOneElementArray()
        {
            byte[] testBytes = { 1 };
            byte[] expectedResultBytes = { 1 };
            testBytes.ReverseBytes();
            Assert.IsTrue(testBytes.CompareByteArray(expectedResultBytes));
        }

        public void TestReverseTwoElementsArray()
        {
            byte[] testBytes = { 1, 2 };
            byte[] expectedResultBytes = { 2, 1 };
            testBytes.ReverseBytes();
            Assert.IsTrue(testBytes.CompareByteArray(expectedResultBytes));
        }

        public void TestReverseThreeElementsArray()
        {
            byte[] testBytes = { 1, 2, 3 };
            byte[] expectedResultBytes = { 3, 2, 1 };
            testBytes.ReverseBytes();
            Assert.IsTrue(testBytes.CompareByteArray(expectedResultBytes));
        }

        public void TestReverseFourElementsArray()
        {
            byte[] testBytes = { 1, 2, 3, 4 };
            byte[] expectedResultBytes = { 4, 3, 2, 1 };
            testBytes.ReverseBytes();
            Assert.IsTrue(testBytes.CompareByteArray(expectedResultBytes));
        }

        public void TestReverseFiveElementsArray()
        {
            byte[] testBytes = { 1, 2, 3, 4, 5 };
            byte[] expectedResultBytes = { 5, 4, 3, 2, 1 };
            testBytes.ReverseBytes();
            Assert.IsTrue(testBytes.CompareByteArray(expectedResultBytes));
        }
        */

    }
#endif
}
