using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;
using ATFS5.src.Modbus;
using MFUnit;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.UnitTests
{
#if UNIT_TESTING
    class ModbusFunction04RequestPduTests
    {
        /*
        public void TestValidPduBytesGeneration1()
        {
            const byte functionCode = 4;
            const ushort startAddress = 107;
            const ushort registersQuantity = 3;
            //Start address= 107=0x006B
            //Register quantity=3=0x0003

            Function04ReadInputRegistersRequestPdu pdu = new Function04ReadInputRegistersRequestPdu(startAddress, registersQuantity);
            byte[] expectedBytes = { functionCode, 0x00, 0x6B, 0x00, 0x03 };
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestValidPduBytesGeneration2()
        {
            const byte functionCode = 4;
            const ushort startAddress = 300;
            const ushort registersQuantity = 3;
            //Start address= 300=0x012C
            //Register quantity=3=0x0003

            Function04ReadInputRegistersRequestPdu pdu = new Function04ReadInputRegistersRequestPdu(startAddress, registersQuantity);
            byte[] expectedBytes = { functionCode, 0x01, 0x2C, 0x00, 0x03 };
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestPduGenerationFromValidBytes1()
        {
            const byte functionCode = 4;
            const ushort startAddress = 107;
            const ushort registersQuantity = 3;
            //Start address= 107=0x006B
            //Register quantity=3=0x0003

            byte[] bytes = { functionCode, 0x00, 0x6B, 0x00, 0x03 };
            Function04ReadInputRegistersRequestPdu pdu = new Function04ReadInputRegistersRequestPdu(bytes);
            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.StartAddress == startAddress) && (pdu.RegistersQuantity == registersQuantity));
        }

        public void TestPduGenerationFromValidBytes2()
        {
            const byte functionCode = 4;
            const ushort startAddress = 300;
            const ushort registersQuantity = 3;
            //Start address= 300=0x012C
            //Register quantity=3=0x0003

            byte[] bytes = {functionCode, 0x01, 0x2C, 0x00, 0x03 };
            Function04ReadInputRegistersRequestPdu pdu = new Function04ReadInputRegistersRequestPdu(bytes);
            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.StartAddress == startAddress) && (pdu.RegistersQuantity == registersQuantity));
        }

        public void TestPduGenerationFromTooShortByteData()
        {
            const byte functionCode = 4;
            //Start address= 300=0x012C
            //Register quantity=3=0x0003

            byte[] bytes = { functionCode, 0x01, 0x2C, 0x00, 0x03 };
            Function04ReadInputRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersRequestPdu(bytes, 1); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithWrongFunctionCode()
        {
            const byte wrongfunctionCode = 3;
            //Start address= 300=0x012C
            //Register quantity=3=0x0003

            byte[] bytes = { wrongfunctionCode, 0x01, 0x2C, 0x00, 0x03 };
            Function04ReadInputRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersRequestPdu(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithTooHighRegisterCount()
        {
            //0x7E=126
            byte[] bytes = { 0x04, 0x01, 0x2C, 0x00, 0x7E };
            Function04ReadInputRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersRequestPdu(bytes, 0); }, typeof(ModbusIllegalDataValueException));
        }

        public void TestPduGenerationFromByteDataWithTooLowRegisterCount()
        {

            byte[] bytes = { 0x04, 0x01, 0x2C, 0x00, 0x00 };
            Function04ReadInputRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersRequestPdu(bytes, 0); }, typeof(ModbusIllegalDataValueException));
        }



        public void TestPduConstructionWithTooHighRegisterCount()
        {
            const ushort registerQuantity = 126;
            const ushort startAddress = 0;
            Function04ReadInputRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersRequestPdu(startAddress, registerQuantity); }, typeof(ModbusIllegalDataValueException));
        }

        public void TestPduConstructionWithTooLowRegisterCount()
        {
            const ushort registerQuantity = 0;
            const ushort startAddress = 0;
            Type expectedException = typeof(ModbusIllegalDataValueException);
            Function04ReadInputRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersRequestPdu(startAddress, registerQuantity); }, expectedException);
        }
        */
    }
#endif
}
