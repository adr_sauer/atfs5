using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;
using MFUnit;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.UnitTests
{
#if UNIT_TESTING
    class ModbusExceptionsPdusTests
    {
        /*
        public void TestConversionFromByteData1()
        {
            //Original function code = 131-128=3
            //Exception code=1;
            byte[] bytes = { 131, 1 };
            ModbusExceptionPdu pdu = new Exception01IllegalFunctionPdu(bytes);
            Assert.IsTrue((pdu.ExceptionCode == 1) && (pdu.OriginalFunctionCode == 3));

        }

        public void TestConversionFromByteData2()
        {
            //Original function code = 131-128=3
            //Exception code=1;
            //7 preceeding bytes to simulate MBAP
            byte[] bytes = {0,0,0,0,0,0,0, 131, 1 };
            ModbusExceptionPdu pdu = new Exception01IllegalFunctionPdu(bytes,7);
            Assert.IsTrue((pdu.ExceptionCode == 1) && (pdu.OriginalFunctionCode == 3));

        }

        public void TestConversionFromByteData3()
        {
            //Byte data too short.
            byte[] bytes = { 0, 0, 0, 0, 0, 0, 0, 131};
            ModbusExceptionPdu pdu=null;
            Assert.Throws(() => { pdu = new Exception01IllegalFunctionPdu(bytes, 7); }, typeof(ArgumentException));
        }

        public void TestConversionFromByteData4()
        {
            //Function code does not indicate modbus exception.
            byte[] bytes = { 0, 0, 0, 0, 0, 0, 0, 1,1 };
            ModbusExceptionPdu pdu = null;
            Assert.Throws(() => { pdu = new Exception01IllegalFunctionPdu(bytes, 7); }, typeof(ArgumentException));
        }
        public void TestConversionFromByteData5()
        {
            //Exception code does not fit exception class.
            byte[] bytes = { 0, 0, 0, 0, 0, 0, 0, 131 ,2 };
            ModbusExceptionPdu pdu = null;
            Assert.Throws(() => { pdu = new Exception01IllegalFunctionPdu(bytes, 7); }, typeof(ArgumentException));
        }

        public void TestConversionFromByteData6()
        {
            //Original function code = 1
            //Exception code=2;
            byte[] bytes = { 0x81, 0x02 };
            ModbusExceptionPdu pdu = new Exception02IllegalDataAddressPdu(bytes);
            Assert.IsTrue((pdu.ExceptionCode == 2) && (pdu.OriginalFunctionCode == 1));

        }
        

        public void TestConversionToByteData1()
        {
            //Original function code = 131-128=3
            //Exception code=1;
            ModbusExceptionPdu pdu = new Exception01IllegalFunctionPdu(3);  
            byte[] expectedBytes = { 131, 1 };
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));

        }

        public void TestConversionToByteData2()
        {
            //Buffer too short
            byte[] buffer = new byte[8];
            ModbusExceptionPdu pdu = new Exception01IllegalFunctionPdu(3);
            Assert.Throws(() => { pdu.WriteIntoByteTable(buffer, 7); }, typeof(ArgumentException));
        }

        public void TestConversionToByteData3()
        {
            //Original function code = 1
            //Exception code=2;
            ModbusExceptionPdu pdu = new Exception02IllegalDataAddressPdu(1);
            byte[] expectedBytes = { 0x81, 0x02 };
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));

        }
        */
    }
#endif
}
