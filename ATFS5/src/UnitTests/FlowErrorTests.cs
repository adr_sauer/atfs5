using System;
using Microsoft.SPOT;
using MFUnit;

namespace ATFS5.src.UnitTests
{
#if UNIT_TESTING
    class FlowErrorTests
    {
        /*
        public void TestFriendlyStringNoError()
        {
            FlowError error = FlowError.NoError;
            string expectedString = "No error";

            string resultString=error.ToFriendlyString();

            Assert.IsTrue(resultString == expectedString);
        }

        public void TestFriendlyStringAllErrors()
        {
            FlowError error = FlowError.NoError;
            error |= FlowError.FlowmeterLowCurrent;
            error |= FlowError.FlowmeterHighCurrent;
            error |= FlowError.FlowmeterStatus;
            error |= FlowError.FlowSensor;
            error |= FlowError.NoChangeInMassIntegral;
            string expectedString = "Flowmeter Low Current, Flowmeter High Current, Flowmeter Status, Flow Sensor, No Change In Mass Integral";

            string resultString = error.ToFriendlyString();

            Assert.IsTrue(resultString == expectedString);
        }

        public void TestFriendlyStringFlowSensor()
        {
            FlowError error = FlowError.NoError;
            error |= FlowError.FlowSensor;
            string expectedString = "Flow Sensor";

            string resultString = error.ToFriendlyString();

            Assert.IsTrue(resultString == expectedString);
        }

        public void TestFriendlyStringFlowSensorAndHighCurrent()
        {
            FlowError error = FlowError.NoError;
            error |= FlowError.FlowSensor;
            error |= FlowError.FlowmeterHighCurrent;
            string expectedString = "Flowmeter High Current, Flow Sensor";

            string resultString = error.ToFriendlyString();

            Assert.IsTrue(resultString == expectedString);
        }
        */
    }
#endif
}
