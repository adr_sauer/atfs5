using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;
using ATFS5.src.Modbus;
using MFUnit;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.UnitTests
{
#if UNIT_TESTING
    class ModbusFunction03RequestPduTests
    {
        /*
        public void TestValidPduBytesGeneration1()
        {
            //Function code=3
            //Start address= 107=0x6B
            //Register quantity=3
            Function03ReadHoldingRegistersRequestPdu pdu = new Function03ReadHoldingRegistersRequestPdu(107, 3);
            byte[] expectedBytes = { 0x03, 0x00, 0x6B, 0x00, 0x03 };
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestValidPduBytesGeneration2()
        {
            //Function code=3
            //Start address= 300=0x12C
            //Register quantity=3
            Function03ReadHoldingRegistersRequestPdu pdu = new Function03ReadHoldingRegistersRequestPdu(300, 3);
            byte[] expectedBytes = { 0x03, 0x01, 0x2C, 0x00, 0x03 };
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestPduGenerationFromValidBytes1()
        {
            //Function code=3
            //Start address= 107=0x6B
            //Register quantity=3
            byte[] bytes = { 0x03, 0x00, 0x6B, 0x00, 0x03 };
            Function03ReadHoldingRegistersRequestPdu pdu = new Function03ReadHoldingRegistersRequestPdu(bytes);
            Assert.IsTrue((pdu.FunctionCode == 3) && (pdu.StartAddress == 107) && (pdu.RegistersQuantity == 3));
        }

        public void TestPduGenerationFromValidBytes2()
        {
            //Function code=3
            //Start address= 300=0x12C
            //Register quantity=3
            byte[] bytes = { 0x03, 0x01, 0x2C, 0x00, 0x03 };
            Function03ReadHoldingRegistersRequestPdu pdu = new Function03ReadHoldingRegistersRequestPdu(bytes);
            Assert.IsTrue((pdu.FunctionCode == 3) && (pdu.StartAddress == 300) && (pdu.RegistersQuantity == 3));
        }

        public void TestPduGenerationFromTooShortByteData()
        {
            byte[] bytes = { 0x03, 0x01, 0x2C, 0x00, 0x03 };
            Function03ReadHoldingRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function03ReadHoldingRegistersRequestPdu(bytes, 1); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithWrongFunctionCode()
        {
            byte[] bytes = { 0x04, 0x01, 0x2C, 0x00, 0x03 };
            Function03ReadHoldingRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function03ReadHoldingRegistersRequestPdu(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithTooHighRegisterCount()
        {
            //0x7E=126
            byte[] bytes = { 0x03, 0x01, 0x2C, 0x00, 0x7E };
            Function03ReadHoldingRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function03ReadHoldingRegistersRequestPdu(bytes, 0); }, typeof(ModbusIllegalDataValueException));
        }

        public void TestPduGenerationFromByteDataWithTooLowRegisterCount()
        {
           
            byte[] bytes = { 0x03, 0x01, 0x2C, 0x00, 0x00 };
            Function03ReadHoldingRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function03ReadHoldingRegistersRequestPdu(bytes, 0); }, typeof(ModbusIllegalDataValueException));
        }

        

        public void TestPduConstructionWithTooHighRegisterCount()
        {
            const ushort registerQuantity = 126;
            const ushort startAddress = 0;
            Function03ReadHoldingRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function03ReadHoldingRegistersRequestPdu(startAddress,registerQuantity); }, typeof(ModbusIllegalDataValueException));
        }

        public void TestPduConstructionWithTooLowRegisterCount()
        {
            const ushort registerQuantity = 0;
            const ushort startAddress = 0;
            Type expectedException = typeof(ModbusIllegalDataValueException);
            Function03ReadHoldingRegistersRequestPdu pdu = null;
            Assert.Throws(() => { pdu = new Function03ReadHoldingRegistersRequestPdu(startAddress, registerQuantity); }, expectedException);
        }
        */
    }
#endif
}
