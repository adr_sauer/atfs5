using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus.DataStructures.Pdu;
using ATFS5.src.Modbus;
using MFUnit;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.UnitTests
{
#if UNIT_TESTING
    class ModbusFunction04ResponsePduTests
    {
        /*
        public void TestValidPduBytesGeneration1()
        {
            // 0x022B=555
            // 0x0000=0
            // 0x0064=100
            const byte functionCode = 4;
            const byte registersQuantity = 3;            
            const byte byteCount = registersQuantity * 2;
            ushort[] registersValues = { 555, 0, 100 };
            byte[] expectedBytes = { functionCode, byteCount, 0x02, 0x2B, 0x00, 0x00, 0x00, 0x64 };
            Function04ReadInputRegistersResponsePdu pdu = new Function04ReadInputRegistersResponsePdu(registersValues);
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));
        }

        public void TestValidPduBytesGeneration2()
        {
            // 0x07D0=2000
            // 0x1C58=7256
            const byte functionCode = 4;
            const byte registersQuantity = 2;            
            const byte byteCount = registersQuantity * 2;
            ushort[] registersValues = { 2000, 7256 };
            byte[] expectedBytes = { functionCode, byteCount, 0x07, 0xD0, 0x1C, 0x58 };
            Function04ReadInputRegistersResponsePdu pdu = new Function04ReadInputRegistersResponsePdu(registersValues);
            byte[] resultBytes = pdu.GetBytes();
            Assert.IsTrue(expectedBytes.CompareByteArray(resultBytes));

        }

        public void TestPduGenerationFromValidBytes1()
        {
            const byte functionCode = 4;
            const byte registersQuantity = 3;
            const byte byteCount = registersQuantity * 2;
            ushort[] registersValues = { 0x022B, 0x0000,0x0064 };
            // 0x022B=555
            // 0x0000=0
            // 0x0064=100

            byte[] bytes = { functionCode, byteCount, 0x02, 0x2B, 0x00, 0x00, 0x00, 0x64 };
            Function04ReadInputRegistersResponsePdu pdu = new Function04ReadInputRegistersResponsePdu(bytes);
            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.RegistersQuantity == registersQuantity) && (pdu[0] == registersValues[0]) && (pdu[1] == registersValues[1]) && (pdu[2] == registersValues[2]));
        }

        public void TestPduGenerationFromValidBytes2()
        {
            const byte functionCode = 4;
            const byte registersQuantity = 2;
            const byte byteCount = registersQuantity * 2;
            ushort[] registersValues = { 0x07D0, 0x1C58};
            
            
            byte[] bytes = { functionCode, byteCount, 0x07, 0xD0, 0x1C, 0x58 };
            Function04ReadInputRegistersResponsePdu pdu = new Function04ReadInputRegistersResponsePdu(bytes);
            Assert.IsTrue((pdu.FunctionCode == functionCode) && (pdu.RegistersQuantity == registersQuantity) &&
                (pdu[0] == registersValues[0]) && (pdu[1] == registersValues[1]));
        }

        public void TestPduGenerationFromTooShortByteData1()
        {
            const byte functionCode = 4;
            const byte registersQuantity = 2;
            const byte byteCount = registersQuantity * 2;
            byte[] bytes = { functionCode, byteCount, 0x07, 0xD0, 0x1C };
            Function04ReadInputRegistersResponsePdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersResponsePdu(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromTooShortByteData2()
        {
            const byte functionCode = 4;
            byte[] bytes = { functionCode };
            Function04ReadInputRegistersResponsePdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersResponsePdu(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithWrongFunctionCode()
        {
            const byte wrongfunctionCode = 3;
            const byte registersQuantity = 2;
            const byte byteCount = registersQuantity * 2;
            //const ushort register1Value = 2000;
            //const ushort register2Value = 7256;
            byte[] bytes = { wrongfunctionCode, byteCount, 0x07, 0xD0, 0x1C, 0x58 };
            Function04ReadInputRegistersResponsePdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersResponsePdu(bytes, 0); }, typeof(ArgumentException));
        }

        public void TestPduGenerationFromByteDataWithTooHighRegisterCount()
        {
            const byte functionCode = 4;
            const byte registersQuantity = 126;
            const byte byteCount = registersQuantity * 2;
            byte[] bytes = new byte[254];
            bytes[0] = functionCode;
            bytes[1] = byteCount;
            Type expectedException = typeof(ModbusIllegalDataValueException);
            Function04ReadInputRegistersResponsePdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersResponsePdu(bytes, 0); }, expectedException);
        }

        public void TestPduGenerationFromByteDataWithTooLowRegisterCount()
        {

            const byte functionCode = 4;
            const byte registersQuantity = 0;
            const byte byteCount = registersQuantity * 2;
            byte[] bytes = { functionCode, byteCount };
            Type expectedException = typeof(ModbusIllegalDataValueException);
            Function04ReadInputRegistersResponsePdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersResponsePdu(bytes, 0); }, expectedException);
        }

        public void TestPduGenerationFromByteDataWithByteCountIndivisibleBy2()
        {
            const byte functionCode = 4;
            //const byte registersQuantity = 0;
            const byte byteCount = 1;
            byte[] bytes = { functionCode, byteCount, 0x00 };
            Type expectedException = typeof(ArgumentException);
            Function04ReadInputRegistersResponsePdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersResponsePdu(bytes, 0); }, expectedException);
        }

        public void TestPduConstructionWithTooHighRegisterCount()
        {
            ushort[] registerValues = new ushort[126];
            Type expectedException = typeof(ModbusIllegalDataValueException);
            Function04ReadInputRegistersResponsePdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersResponsePdu(registerValues); }, expectedException);
        }

        public void TestPduConstructionWithTooLowRegisterCount()
        {
            ushort[] registerValues = new ushort[0];
            Type expectedException = typeof(ModbusIllegalDataValueException);
            Function04ReadInputRegistersResponsePdu pdu = null;
            Assert.Throws(() => { pdu = new Function04ReadInputRegistersResponsePdu(registerValues); }, expectedException);
        }
        */
    }
#endif
}
