using System;
using Microsoft.SPOT;
using ATFS5.src.Screens20x4;

namespace ATFS5.src
{
    /// <summary>
    /// Enumeration of possible operation positions in operations series. A series of operations leads to the creation of a product.
    /// </summary>
    public enum OperationOrderInSeries 
    { 
        /// <summary>
        /// Operation begins a series.
        /// </summary>
        Begin = 0, 
        
        /// <summary>
        /// Operation is in the middle of series.
        /// </summary>
        Continue = 1, 
       
        /// <summary>
        /// Operation ends a series.
        /// </summary>
        End = 2, 
        
        /// <summary>
        /// Operation is appended to the last series.
        /// </summary>
        Append = 3 
    };

    /// <summary>
    /// Class declares extension methods for OperationOrderInSeries enum.
    /// </summary>
    public static class OperationOrderInSeriesExtensions
    {
        

        /// <summary>
        /// Retrieves an array of the values of the constants in OperationOrderInSeries enumeration.
        /// </summary>
        /// <param name="obj">Enumeration to get values of.</param>
        /// <returns>An array that contains the values of the constants in OperationOrderInSeries.</returns>
        public static OperationOrderInSeries[] GetValues(this OperationOrderInSeries obj)
        {
            OperationOrderInSeries[] values = new OperationOrderInSeries[4];
            values[0] = OperationOrderInSeries.Begin;
            values[1] = OperationOrderInSeries.Continue;
            values[2] = OperationOrderInSeries.End;
            values[3] = OperationOrderInSeries.Append;
            return values;
        }

        /// <summary>
        /// Retrieves an array of ITwoLineDisplayable objects representing values of the constants in OperationOrderInSeries enumeration.
        /// </summary>
        /// <param name="obj">Enumeration to get values of.</param>
        /// <returns>An array of ITwoLineDisplayable objects representing values of the constants in OperationOrderInSeries enumeration</returns>
        public static ITwoLineDisplayable[] GetTwoLineDisplayables(this OperationOrderInSeries obj)
        {
            OperationOrderInSeries[] values = obj.GetValues();
            ITwoLineDisplayable[] displayables = new ITwoLineDisplayable[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                displayables[i] = new OperationOrderInSeriesTwoLineDisplayable(values[i]);
            }
            return displayables;   
        }

        /// <summary>
        /// Gets a friendly string description of an OperationOrderInSeries enumeration.
        /// </summary>
        /// <param name="obj">Enumeration to get friendly string description of.</param>
        /// <returns>Friendly string description of an OperationOrderInSeries enumeration</returns>
        public static string DisplayFriendlyString(this OperationOrderInSeries obj)
        {
            switch (obj)
            {
                case OperationOrderInSeries.Begin:
                    return "PIERWSZA";
                case OperationOrderInSeries.Continue:
                    return "SRODKOWA";
                case OperationOrderInSeries.End:
                    return "OSTATNIA";
                case OperationOrderInSeries.Append:
                    return "DOLANIE PO KONCU";
                default:
                    throw new ArgumentException();
                    
            }
        }
    }

    /// <summary>
    /// Represents a facade for ITwoLineDisplayable interface implementation for OperationOrderInSeries enum.
    /// </summary>
    public class OperationOrderInSeriesTwoLineDisplayable : ITwoLineDisplayable
    {
        private OperationOrderInSeries _underlyingEnum;

        /// <summary>
        /// Gets the underlying OperationOrderInSeries enumeration.
        /// </summary>
        public OperationOrderInSeries UnderlyingEnum
        {
            get
            {
                return _underlyingEnum;
            }
        }

        /// <summary>
        /// Initializes a new instance of the OperationOrderInSeriesTwoLineDisplayable class using the specified OperationOrderInSeries enumeration.
        /// </summary>
        /// <param name="underlyingEnum">Underlying OperationOrderInSeries enumeration</param>
        public OperationOrderInSeriesTwoLineDisplayable(OperationOrderInSeries underlyingEnum)
        {
            _underlyingEnum = underlyingEnum;
        }

        /// <summary>
        /// Returns two strings which will represent operation order in series enum on display. For example in menu which displays one object at a time.
        /// </summary>
        /// <param name="line1">First line - operation order in series enum friendly string. Output parameter.</param>
        /// <param name="line2">Second line - empty string. Output parameter.</param>
        public void GetTwoDisplayableLines(out string line1, out string line2)
        {
            line1 = _underlyingEnum.DisplayFriendlyString();
            line2 = "";
        }
    }
    
}
