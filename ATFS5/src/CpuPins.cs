using Microsoft.SPOT.Hardware;

namespace ATFS5.src.UtilitiesLib
{
    /// <summary>
    /// Provides pinout information. 
    /// </summary>
    public static class CpuPins
    {
#if DEVICE_DEPLOY

#elif EMUL_DEPLOY
        /// <summary>
        /// Pin connected to board LED.
        /// </summary>
        public const Cpu.Pin BoardLedPin = (Cpu.Pin)1;

        /// <summary>
        /// Pin connected to the first row from bottom edge.
        /// </summary>
        public const Cpu.Pin KeyboardRow1Pin = (Cpu.Pin)2;

        /// <summary>
        /// Pin connected to the second row from bottom edge.
        /// </summary>
        public const Cpu.Pin KeyboardRow2Pin = (Cpu.Pin)3;

        /// <summary>
        /// Pin connected to the third row from bottom edge.
        /// </summary>
        public const Cpu.Pin KeyboardRow3Pin = (Cpu.Pin)4;

        /// <summary>
        /// Pin connected to the fourth row from bottom edge.
        /// </summary>
        public const Cpu.Pin KeyboardRow4Pin = (Cpu.Pin)5;

        /// <summary>
        /// Pin connected to the first column from left edge.
        /// </summary>
        public const Cpu.Pin KeyboardColumn1Pin = (Cpu.Pin)6;

        /// <summary>
        /// Pin connected to the second column from left edge.
        /// </summary>
        public const Cpu.Pin KeyboardColumn2Pin = (Cpu.Pin)7;

        /// <summary>
        /// Pin connected to the third column from left edge.
        /// </summary>
        public const Cpu.Pin KeyboardColumn3Pin = (Cpu.Pin)8;

        /// <summary>
        /// Pin connected to RS pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayRsPin = (Cpu.Pin)9;

        /// <summary>
        /// Pin connected to RW pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayRwPin = (Cpu.Pin)10;

        /// <summary>
        /// Pin connected to E pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayEPin = (Cpu.Pin)11;

        /// <summary>
        /// Pin connected to DB0 pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayDb0Pin = (Cpu.Pin)12;

        /// <summary>
        /// Pin connected to DB1 pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayDb1Pin = (Cpu.Pin)13;

        /// <summary>
        /// Pin connected to DB2 pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayDb2Pin = (Cpu.Pin)14;

        /// <summary>
        /// Pin connected to DB3 pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayDb3Pin = (Cpu.Pin)15;

        /// <summary>
        /// Pin connected to DB4 pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayDb4Pin = (Cpu.Pin)16;

        /// <summary>
        /// Pin connected to DB5 pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayDb5Pin = (Cpu.Pin)17;

        /// <summary>
        /// Pin connected to DB6 pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayDb6Pin = (Cpu.Pin)18;

        /// <summary>
        /// Pin connected to DB7 pin of LCD display.
        /// </summary>
        public const Cpu.Pin LcdDisplayDb7Pin = (Cpu.Pin)19;

        /// <summary>
        /// Pin connected to status input of water flowmeter.
        /// </summary>
        public const Cpu.Pin WaterFlowmeterStatusInputPin = (Cpu.Pin)20;

        /// <summary>
        /// Pin connected to status input of medium flowmeter.
        /// </summary>
        public const Cpu.Pin MediumFlowmeterStatusInputPin = (Cpu.Pin)21;

        /// <summary>
        /// Pin connected to water flow sensor.
        /// </summary>
        public const Cpu.Pin WaterFlowSensorPin = (Cpu.Pin)22;

        /// <summary>
        /// Pin connected to medium flow sensor.
        /// </summary>
        public const Cpu.Pin MediumFlowSensorPin = (Cpu.Pin)23;

        /// <summary>
        /// Pin connected to water valve relay.
        /// </summary>
        public const Cpu.Pin WaterValvePin = (Cpu.Pin)24;

        /// <summary>
        /// Pin connected to medium valve relay.
        /// </summary>
        public const Cpu.Pin MediumValvePin = (Cpu.Pin)25;

        /// <summary>
        /// Pin connected to water pump relay.
        /// </summary>
        public const Cpu.Pin WaterPumpPin = (Cpu.Pin)26;

        /// <summary>
        /// Pin connected to medium pump relay.
        /// </summary>
        public const Cpu.Pin MediumPumpPin = (Cpu.Pin)27;

        /// <summary>
        /// Pin connected to mechanical mixer relay.
        /// </summary>
        public const Cpu.Pin MechanicalMixerPin = (Cpu.Pin)28;

        /// <summary>
        /// Pin connected to siren.
        /// </summary>
        public const Cpu.Pin SirenPin = (Cpu.Pin)29;

        /// <summary>
        /// Pin connected to a relay which enables 24V outputs.
        /// </summary>
        public const Cpu.Pin Enable24VPin = (Cpu.Pin)30;

        /// <summary>
        /// Pin allowing for LCD backlight control.
        /// </summary>
        public const Cpu.Pin LcdDisplayBacklightPin = (Cpu.Pin)31;


#endif
    }
}
