using System;
using Microsoft.SPOT;

namespace ATFS5.src
{
    /// <summary>
    /// Flowmeter possible error flags.
    /// </summary>
    [Flags]
    public enum FlowmeterErrorStatus
    {
        /// <summary>
        /// No error indicated.
        /// </summary>
        None=0,
        /// <summary>
        /// Current input signalling error by allowing too much current.
        /// </summary>
        CurrentHighError=2,
        /// <summary>
        /// Current input signalling error by allowing too less current.
        /// </summary>
        CurrentLowError=4,
        /// <summary>
        /// Flowmeter status input signalling error.
        /// </summary>
        StatusInputError=8
    };
}
