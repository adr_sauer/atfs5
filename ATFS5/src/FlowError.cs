using System;
using Microsoft.SPOT;
using NetMf.CommonExtensions;

namespace ATFS5.src
{
    /// <summary>
    /// Possible flow error flags.
    /// </summary>
    [Flags]
    public enum FlowError
    {
        /// <summary>
        /// There is no flow error.
        /// </summary>
        NoError = 0,
        /// <summary>
        /// Flowmeter current input signalling error by allowing too less current.
        /// </summary>
        FlowmeterLowCurrent = 1,
        /// <summary>
        /// Flowmeter current input signalling error by allowing too much current.
        /// </summary>
        FlowmeterHighCurrent = 2,
        /// <summary>
        /// Flowmeter status input signalling error.
        /// </summary>
        FlowmeterStatus = 4,
        /// <summary>
        /// Flow sensor indicating no flow.
        /// </summary>
        FlowSensor = 8,
        /// <summary>
        /// There was no change in poured mass sum.
        /// </summary>
        NoChangeInMassIntegral = 16
    }

    /// <summary>
    /// Declares extension methods for ATFS.src.FlowError enum.
    /// </summary>
    public static class FlowErrorExtensions
    {
        /// <summary>
        /// Merges errors indicated by FlowError enum with errors indicated by FlowmeterErrorStatus enum.
        /// </summary>
        /// <param name="error">FlowError enum</param>
        /// <param name="flowmeterErrorStatus">FlowmeterErrorStatus enum</param>
        /// <returns>Merged FlowError enum</returns>
        public static FlowError Union(this FlowError error, FlowmeterErrorStatus flowmeterErrorStatus)
        {
            
            if ((flowmeterErrorStatus & FlowmeterErrorStatus.CurrentHighError) == FlowmeterErrorStatus.CurrentHighError)
                error |= FlowError.FlowmeterHighCurrent;
            if ((flowmeterErrorStatus & FlowmeterErrorStatus.CurrentLowError) == FlowmeterErrorStatus.CurrentLowError)
                error |= FlowError.FlowmeterLowCurrent;
            if ((flowmeterErrorStatus & FlowmeterErrorStatus.StatusInputError) == FlowmeterErrorStatus.StatusInputError)
                error |= FlowError.FlowmeterStatus;

            return error;
        }

        /// <summary>
        /// Gets a string representing FlowError enum flags as a set of characters.
        /// </summary>
        /// <param name="error">FlowError enum.</param>
        /// <returns>String representing FlowError enum flags as a set of characters.
        /// H - means current input is signalling error by allowing too much current.
        /// L - means current input is signalling error by allowing too less current.
        /// F - means flowmeter status input is signalling error.
        /// S - means flow sensor indicates no flow.
        /// C - means no change in flow mass integral has been observed.
        /// </returns>
        /// <example>
        /// LSC - means current input is signalling error by allowing too less current, flow sensor indicates no flow and there was no change in flow mass integral.
        /// Propably there is no flow of liquid.
        /// </example>
        public static string ToSymbolString(this FlowError error)
        {
            StringBuilder builder = new StringBuilder();
            if ((error & FlowError.FlowmeterLowCurrent) == FlowError.FlowmeterLowCurrent)
                builder.Append("L");
            if ((error & FlowError.FlowmeterHighCurrent) == FlowError.FlowmeterHighCurrent)
                builder.Append("H");            
            if ((error & FlowError.FlowmeterStatus) == FlowError.FlowmeterStatus)
                builder.Append("F");
            if ((error & FlowError.FlowSensor) == FlowError.FlowSensor)
                builder.Append("S");
            if ((error & FlowError.NoChangeInMassIntegral) == FlowError.NoChangeInMassIntegral)
                builder.Append("C");

            return builder.ToString();
        }

        /// <summary>
        /// Gets a user friendly string representing FlowError enum flags.
        /// </summary>
        /// <param name="error">FlowError enum.</param>
        /// <returns>User friendly string representing FlowError enum flags</returns>
        public static string ToFriendlyString(this FlowError error)
        {
            if (error == FlowError.NoError)
            {
                return "No error";
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                if ((error & FlowError.FlowmeterLowCurrent) == FlowError.FlowmeterLowCurrent)
                    builder.Append("Flowmeter Low Current, ");
                if ((error & FlowError.FlowmeterHighCurrent) == FlowError.FlowmeterHighCurrent)
                    builder.Append("Flowmeter High Current, ");                
                if ((error & FlowError.FlowmeterStatus) == FlowError.FlowmeterStatus)
                    builder.Append("Flowmeter Status, ");
                if ((error & FlowError.FlowSensor) == FlowError.FlowSensor)
                    builder.Append("Flow Sensor, ");
                if ((error & FlowError.NoChangeInMassIntegral) == FlowError.NoChangeInMassIntegral)
                    builder.Append("No Change In Mass Integral, ");

                string friendlyStringWithCommaAtTheEnd = builder.ToString();

                return friendlyStringWithCommaAtTheEnd.Substring(0, friendlyStringWithCommaAtTheEnd.Length - 2);
            }
        }
    }
}
