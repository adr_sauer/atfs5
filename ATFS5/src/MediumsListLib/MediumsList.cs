using System;
using Microsoft.SPOT;
using atetech.pl.atfs;
using ATFS5.src.UtilitiesLib;
using Ws.Services.Binding;
using ServiceMedium = schemas.datacontract.org.TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib.Medium;
using ServiceMediumArray = schemas.datacontract.org.TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib.ArrayOfMedium;
using System.Threading;

namespace ATFS5.src.MediumsListLib
{   
    /// <summary>
    /// Represents a list of mediums which can be poured into the tank. 
    /// Mediums list data is got from web service on initialization and on a given interval.
    /// Data received from web service is saved to a local file.
    /// If there is a problem with the web service, data is read from local file.
    /// Mediums list is changed only when system is idle.
    /// </summary>
    public class MediumsList
    {

        
        private Medium[] _mediums=null;
        private MediumsListFile _mediumsListFile;
        private IAtfsMediumsListServiceClientProxy _mediumsServiceClient;
        private Timer _webServiceCheckTimer;
        private object _mediumsLockObject;

        /// <summary>
        /// Gets Medium at given index.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <returns>Medium object which was a given index.</returns>
        public Medium this[int index]
        {
            get
            {
                lock (_mediumsLockObject)
                    return _mediums[index];
            }
        }

        /// <summary>
        /// Copies the elements of the MediumsList to a new array.
        /// </summary>
        /// <returns>An array containing copies of the elements of the MediumsList.</returns>
        public Medium[] ToArray()
        {
            lock (_mediumsLockObject)
            {
                Medium[] arrayCopy = new Medium[_mediums.Length];
                for (int i = 0; i < _mediums.Length; i++)
                    arrayCopy[i] = (Medium)_mediums[i].Clone();

                return arrayCopy;
            }
        }

        /// <summary>
        /// Initializes a new instance of the MediumsList class using the specified mediums data file pathname, service endpoint address and checking interval.
        /// </summary>
        /// <param name="filePathname">Pathname to file containing backup mediums data.</param>
        /// <param name="serviceEndpointAddress">Endpoint address of web service providing mediums data.</param>
        /// <param name="webServiceCheckIntervalSeconds">Interval at which web service is called to check for new mediums data. Given in seconds.</param>
        public MediumsList(string filePathname, string serviceEndpointAddress, int webServiceCheckIntervalSeconds)
        {
            _mediumsLockObject = new object();
            _mediumsListFile = new MediumsListNETMFBinarySerializedFile(filePathname);

            InitializeWebServiceClient(serviceEndpointAddress);

            //HACK Turned off mediums service getter in constructor
            //_mediums = GetMediumsFromWebService();

            if (_mediums == null)
            {
                Utilities.ProgramLogger.LogError("Cannot get mediums list from web service. Reading from file.");
                _mediums = _mediumsListFile.ReadFromFile();
            }
            else
            {
                _mediumsListFile.SaveToFile(_mediums);
            }
            
            _webServiceCheckTimer = new Timer(OnCheckWebServiceTimer, null, webServiceCheckIntervalSeconds * 1000, webServiceCheckIntervalSeconds * 1000);            
        }

        private void InitializeWebServiceClient(string serviceEndpointAddress)
        {
            var binding = new WS2007HttpBinding(new HttpTransportBindingConfig(new Uri(serviceEndpointAddress)));
            _mediumsServiceClient = new IAtfsMediumsListServiceClientProxy(binding, new Ws.Services.ProtocolVersion11());
        }

        private Medium[] GetMediumsFromWebService()
        {
            //Default value returned if an error occurs.
            Medium[] mediums = null;
            try
            {
                var request = new GetAtfsMediums();
                GetAtfsMediumsResponse response = _mediumsServiceClient.GetAtfsMediums(request);
                ServiceMedium[] responseArray = response.GetAtfsMediumsResult.Medium;
                if (responseArray.Length > 0)
                {
                    mediums = new Medium[responseArray.Length];
                    for (int i = 0; i < responseArray.Length; i++)
                    {
                        mediums[i] = new Medium(responseArray[i].Id, responseArray[i].Name);
                    }
                }

            }
            catch (Exception exception)
            {
                //If an exception occurs during communication with the web service null is returned.
                Utilities.ProgramLogger.LogException(exception);
            }
            return mediums;
        }

        private bool CheckIfSystemIsIdle()
        {
            return Program.ProgramControllersContext.IsSystemIdle;
        }

        private void OnCheckWebServiceTimer(object state)
        {
            //Get mediums data from service only if system is idle and they can be switched with current data.
            if (CheckIfSystemIsIdle())
            {
                //HACK Disabled mediums list service queries in timer.
                //Medium[] dataFromService = GetMediumsFromWebService();
                Medium[] dataFromService = null; 
    
                //Switch mediums data from service only if they are not null and system is idle.
                if ((dataFromService != null) && CheckIfSystemIsIdle())
                {
                    lock (_mediumsLockObject)
                    {
                        _mediums = dataFromService;
                        _mediumsListFile.SaveToFile(_mediums);
                    }
                }
            }
        }

    }
}
