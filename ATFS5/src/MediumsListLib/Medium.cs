using System;
using Microsoft.SPOT;
using ATFS5.src.Screens20x4;

namespace ATFS5.src.MediumsListLib
{
    /// <summary>
    /// Represents medium data.
    /// </summary>
    [Serializable]
    public class Medium : ICloneable, ITwoLineDisplayable
    {
        private const int _maxNameLength = 20;
        private string _name = String.Empty;
        private int _id = 0;

        /// <summary>
        /// Maximum length of medium name equal to 20.
        /// </summary>
        static public int MaxNameLength
        {

            get { return _maxNameLength; }
        }

        /// <summary>
        /// Initializes a new instance of the Medium class.
        /// </summary>
        public Medium()
        {
        }

        /// <summary>
        ///  Initializes a new instance of the Medium class using the specified id and name.
        /// </summary>
        /// <param name="id">Medium ID. Must be a positive integer.</param>
        /// <param name="name">Medium name. Must not be null and up to 20 characters long.</param>
        /// <exception cref="ArgumentException">ID not a positive integer, medium name null or longer than 20 characters.</exception>
        public Medium(int id, string name)
        {
            if (id < 0)
                throw new ArgumentException("Medium ID must be a positive integer.");
            if (name == null)
                throw new ArgumentException("Medium name must not be null.");
            if (name.Length > _maxNameLength)
                throw new ArgumentException("Maximum medium name length is 20");            
            _id = id;
            _name = name;
        }

        /// <summary>
        /// Gets or sets medium name. Must not be null and up to 20 characters long.
        /// </summary>
        /// <exception cref="ArgumentException">Medium name null or longer than 20 characters.</exception>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value == null)
                    throw new ArgumentException("Medium name must not be null.");
                if (value.Length > _maxNameLength)
                    throw new ArgumentException("Maximum medium name length is 20");                
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets medium ID. Must be a positive integer.
        /// </summary>
        /// <exception cref="ArgumentException">ID not a positive integer.</exception>        
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("Medium ID must be a positive integer.");
                _id = value;
            }
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            return new Medium(_id, _name);
        }

        /// <summary>
        /// Returns two strings which will represent medium on display. For example in menu which displays one object at a time.
        /// </summary>
        /// <param name="line1">First line - medium name. Output parameter.</param>
        /// <param name="line2">Second line - empty string. Output parameter.</param>
        public void GetTwoDisplayableLines(out string line1, out string line2)
        {
            line1 = _name;
            line2 = String.Empty;
        }
    }
}
