using System;
using Microsoft.SPOT;
using System.IO;

namespace ATFS5.src.MediumsListLib
{
    /// <summary>
    /// Represents a NET Micro Framework binary serialization file containing backup mediums data.
    /// </summary>
    public class MediumsListNETMFBinarySerializedFile : MediumsListFile
    {
        /// <summary>
        /// Initializes a new instance of the MediumsListNETMFBinarySerializedFile class using the specified mediums data file pathname.
        /// </summary>
        /// <param name="filePathname"></param>
        public MediumsListNETMFBinarySerializedFile(string filePathname)
            : base(filePathname)
        {
        }

        /// <summary>
        /// Reads mediums data from NET Micro Framework binary serialization file.
        /// </summary>
        /// <returns>An array of Medium objects.</returns>
        public override Medium[] ReadFromFile()
        {
            byte[] mediumsByteData = null;
            using (var mediumsFileStream = File.Open(_filePathname, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                if (mediumsFileStream.Length > Int32.MaxValue)
                    throw new IOException("Mediums list file too long.");
                mediumsByteData=new byte[(int)mediumsFileStream.Length];
                mediumsFileStream.Read(mediumsByteData, 0, mediumsByteData.Length);
            }
            return Reflection.Deserialize(mediumsByteData, typeof(Medium[])) as Medium[];
        }
        
        /// <summary>
        /// Saves mediums data to NET Micro Framework binary serialization file.
        /// </summary>
        /// <param name="mediums">An array of Medium objects to save.</param>
        public override void SaveToFile(Medium[] mediums)
        {
            byte[] mediumsByteData = Reflection.Serialize(mediums, typeof(Medium[]));

            using (var mediumsFileStream = File.Open(_filePathname, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                mediumsFileStream.Write(mediumsByteData, 0, mediumsByteData.Length);
                mediumsFileStream.Flush();
            }
        }
    }
}
