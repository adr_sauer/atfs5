using System;
using Microsoft.SPOT;

namespace ATFS5.src.MediumsListLib
{
    /// <summary>
    /// Represents an abstract base class forclasses representing various file formats containing backup mediums data.
    /// </summary>
    public abstract class MediumsListFile
    {
        /// <summary>
        /// Pathname to file containing backup mediums data.
        /// </summary>
        protected string _filePathname;

        /// <summary>
        /// Initializes a new instance of the MediumsListFile class using the specified mediums data file pathname.
        /// </summary>
        /// <param name="filePathname">Pathname to file containing backup mediums data.</param>
        public MediumsListFile(string filePathname)
        {
            _filePathname = filePathname;
        }
        
        /// <summary>
        /// Reads mediums data from file.
        /// </summary>
        /// <returns>An array of Medium objects.</returns>
        public abstract Medium[] ReadFromFile();

        /// <summary>
        /// Saves mediums data to file.
        /// </summary>
        /// <param name="mediums">An array of Medium objects to save.</param>
        public abstract void SaveToFile(Medium[] mediums);

    }
}
