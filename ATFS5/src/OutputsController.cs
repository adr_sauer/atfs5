using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.Reflection;

namespace ATFS5.src
{
    /// <summary>
    /// Represents a controller of ATFS system binary outputs.
    /// Class is thread-safe.
    /// </summary>
    public sealed class OutputsController : IDisposable
    {
        private OutputPort _waterValve;
        private OutputPort _mediumValve;
        private OutputPort _waterPump;
        private OutputPort _mediumPump;
        private OutputPort _mechanicalMixer;
        private OutputPort _siren;
        private OutputPort _enable24V;
        private OutputPort _lcdDisplayBacklight;
        private bool _disposed = false;
        private object _lockObject;

        /// <summary>
        /// Initializes a new instance of the OutputsController class using the specified output ports pins.
        /// </summary>
        /// <param name="waterValvePin">Pin connected to water valve relay.</param>
        /// <param name="mediumValvePin">Pin connected to medium valve relay.</param>
        /// <param name="waterPumpPin">Pin connected to water pump relay.</param>
        /// <param name="mediumPumpPin">Pin connected to medium pump relay.</param>
        /// <param name="mechanicalMixerPin">Pin connected to mechanical mixer relay.</param>
        /// <param name="sirenPin">Pin connected to siren.</param>
        /// <param name="enable24VPin">Pin connected to a relay which enables 24V outputs.</param>
        /// <param name="lcdDisplayBacklightPin">Pin allowing for LCD backlight control.</param>
        public OutputsController(Cpu.Pin waterValvePin, Cpu.Pin mediumValvePin, Cpu.Pin waterPumpPin, Cpu.Pin mediumPumpPin,
            Cpu.Pin mechanicalMixerPin, Cpu.Pin sirenPin, Cpu.Pin enable24VPin, Cpu.Pin lcdDisplayBacklightPin)
        {
            _lockObject = new object();
            _disposed = false;

            _waterValve = new OutputPort(waterValvePin, false);
            _mediumValve = new OutputPort(mediumValvePin, false);
            _waterPump = new OutputPort(waterPumpPin, false);
            _mediumPump = new OutputPort(mediumPumpPin, false);
            _mechanicalMixer = new OutputPort(mechanicalMixerPin, false);
            _siren = new OutputPort(sirenPin, false);
            _enable24V = new OutputPort(enable24VPin, false);
            _lcdDisplayBacklight = new OutputPort(lcdDisplayBacklightPin, false);
        }

        /// <summary>
        /// Open medium valve and turn medium pump on.
        /// </summary>
        public void TurnMediumFlowOn()
        {
            lock (_lockObject)
            {
                _mediumPump.Write(true);
                _mediumValve.Write(true);
            }
        }

        /// <summary>
        /// Close medium valve and turn medium pump off.
        /// </summary>
        public void TurnMediumFlowOff()
        {
            lock (_lockObject)
            {
                _mediumPump.Write(false);
                _mediumValve.Write(false);
            }
        }

        /// <summary>
        /// Open water valve and turn water pump on.
        /// </summary>
        public void TurnWaterFlowOn()
        {
            lock (_lockObject)
            {
                _waterPump.Write(true);
                _waterValve.Write(true);
            }
        }

        /// <summary>
        /// Close water valve and turn water pump off.
        /// </summary>
        public void TurnWaterFlowOff()
        {
            lock (_lockObject)
            {
                _waterPump.Write(false);
                _waterValve.Write(false);
            }
        }

        /// <summary>
        /// Turn mixer on.
        /// </summary>
        public void TurnMixerOn()
        {
            lock (_lockObject)
                _mechanicalMixer.Write(true);
        }

        /// <summary>
        /// Turn mixer off.
        /// </summary>
        public void TurnMixerOff()
        {
            lock (_lockObject)
                _mechanicalMixer.Write(false);
        }

        /// <summary>
        /// Turn siren on.
        /// </summary>
        public void TurnSirenOn()
        {
            lock (_lockObject)
                _siren.Write(true);
        }

        /// <summary>
        /// Turn siren off.
        /// </summary>
        public void TurnSirenOff()
        {
            lock (_lockObject)
                _siren.Write(false);
        }

        /// <summary>
        /// Turn on 24V power.
        /// </summary>
        public void Enable24V()
        {
            lock (_lockObject)
                _enable24V.Write(true);
        }

        /// <summary>
        /// Turns on or off LCD display backlight.
        /// </summary>
        /// <param name="state">If <b>true</b> than turn backlight on. If <b>false</b> than turn backlight off.</param>
        public void SetLcdDisplayBacklightState(bool state)
        {
            lock (_lockObject)
                _lcdDisplayBacklight.Write(state);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes of output ports.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        private void Dispose(bool disposing)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    return;
                _enable24V.Write(false);
                if (disposing)
                {
                    //Dispose of every OutputPort object declared as field in OutputsController.                    
                    foreach (FieldInfo field in this.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
                    {
                        if (field.FieldType.FullName == typeof(OutputPort).FullName)
                        {
                            OutputPort port = field.GetValue(this) as OutputPort;
                            if (port != null)
                                port.Dispose();
                        }
                    }
                }

                _disposed = true;
            }
        }
    }
}
