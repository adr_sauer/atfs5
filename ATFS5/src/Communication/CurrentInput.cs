using System;
using Microsoft.SPOT;

namespace ATFS5.src.Communication
{
    /// <summary>
    /// Provides interface for a current loop input based on an analog to digital converter.
    /// </summary>
    public sealed class CurrentInput
    {
        
        private readonly int _adcValueForMinInput;
        private readonly int _adcValueForMaxInput;
        private readonly int _adcValueForLowCurrentError;
        private readonly int _adcValueForHighCurrentError;
        private readonly int _adcValueThreshholdForLowCurrentError;
        private readonly int _adcValueThreshholdForHighCurrentError;
        private readonly int _adcValidInputRange;
        
        
        private AnalogToDigitalConverter _analogToDigitalConverter;

        

        /// <summary>
        /// Current input value and error status.
        /// </summary>
        public CurrentInputValue Value
        {
            get
            {
                int adcValue = _analogToDigitalConverter.Value;
                double valueOn0To1Scale;
                CurrentInputErrorStatus errorStatus = CurrentInputErrorStatus.NoError;
                if (adcValue < _adcValueForMinInput)
                {
                    valueOn0To1Scale = 0;
                    if (adcValue < _adcValueThreshholdForLowCurrentError)
                        errorStatus = CurrentInputErrorStatus.LowCurrentError;
                }
                else if (adcValue > _adcValueForMaxInput)
                {
                    valueOn0To1Scale = 0;
                    if (adcValue > _adcValueThreshholdForHighCurrentError)
                        errorStatus = CurrentInputErrorStatus.HighCurrentError;
                }
                else
                {
                    valueOn0To1Scale = (double)(adcValue- _adcValueForMinInput) / _adcValidInputRange;
                    errorStatus = CurrentInputErrorStatus.NoError;
                }

                return new CurrentInputValue(valueOn0To1Scale,errorStatus);
            }
        }

        /// <summary>
        /// Reads value from ADC.
        /// </summary>
        public int ADCValue
        {
            get
            {
                return _analogToDigitalConverter.Value;
            }
        }

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="analogToDigitalConverter">Analog to digital converter responsible for reading input value.</param>
        /// <param name="adcValueForMinInput">Analog to digital converter value corresponding to maximum valid current. Usually to 20 mA</param>
        /// <param name="adcValueForMaxInput">Analog to digital converter value corresponding to minimum valid current. Usually to 4 mA</param>
        /// <param name="adcValueForLowCurrentError">Analog to digital converter value corresponding to current level below scale signalling error.</param>
        /// <param name="adcValueForHighCurrentError">Analog to digital converter value corresponding to current level above scale signalling error.</param>
        public CurrentInput(AnalogToDigitalConverter analogToDigitalConverter,
            int adcValueForMinInput, int adcValueForMaxInput, int adcValueForLowCurrentError, int adcValueForHighCurrentError)
        {
            _analogToDigitalConverter = analogToDigitalConverter;
            _adcValueForMinInput = adcValueForMinInput;
            _adcValueForMaxInput = adcValueForMaxInput;
            _adcValueForLowCurrentError = adcValueForLowCurrentError;
            _adcValueForHighCurrentError = adcValueForHighCurrentError;

            _adcValueThreshholdForLowCurrentError = _adcValueForLowCurrentError +
                ((_adcValueForMinInput - _adcValueForLowCurrentError) / 2);

            _adcValueThreshholdForHighCurrentError = _adcValueForHighCurrentError -
                ((_adcValueForHighCurrentError - _adcValueForMaxInput) / 2);

            _adcValidInputRange = _adcValueForMaxInput - _adcValueForMinInput;
        }

    }
}
