using System;
using Microsoft.SPOT;

namespace ATFS5.src.Communication
{
    /// <summary>
    /// Class provides general interface for analog to digital converters.
    /// </summary>
    public abstract class AnalogToDigitalConverter : IDisposable
    {
        /// <summary>
        /// Maximum possible value to get from ADC.
        /// </summary>
        public abstract int MaxValue { get; }

        /// <summary>
        /// Value corresponding to 0V voltage on ADC.
        /// </summary>
        public abstract int ZeroValue { get; }

        /// <summary>
        /// Minimum possible value to get from ADC.
        /// </summary>
        public abstract int MinValue { get; }

        /// <summary>
        /// Reads value from ADC.
        /// </summary>
        public abstract int Value { get; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        protected abstract void Dispose(bool disposing);

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
