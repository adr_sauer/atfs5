using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace ATFS5.src.Communication
{
    /// <summary>
    /// Class provides comunication interface with MCP3425 analog to digital converter. 
    /// Implementation is simplyfied. Only 16 bit and continous conversion mode are implemented.
    /// Lock on I2CDevice object representing I2C bus is acquired on every I2C bus operation.
    /// Class is thread-safe.
    /// </summary>
    public sealed partial class MCP3425 : AnalogToDigitalConverter
    {
        

        private const int _timeout = 50;
        private const int _zeroValue=0;
        private const int _minValue16BitMode = -32768;
        private const int _maxValue16BitMode = 32767;

        /// <summary>
        /// I2C Device configuration object. (address and speed)
        /// </summary>
        private I2CDevice.Configuration _i2cDeviceConfiguration;
        private I2CDevice _i2cBus;
        private Gain _adcGain;
        private SampleRate _adcSampleRate;
        private ConversionMode _conversionMode;
        private byte _lastConfigurationRegistryState = 0; //without RDY bit
        private byte[] _writeDataBuffer, _readDataBuffer;
        private I2CDevice.I2CTransaction[] _writeActionBuffer, _readActionBuffer;
        private object _lockObject;
        private bool _disposed = false;

        /// <summary>
        /// Maximum possible value to get from ADC.
        /// </summary>
        public override int MaxValue
        {
            get { return _maxValue16BitMode; }
        }

        /// <summary>
        /// Value corresponding to 0V voltage on ADC.
        /// </summary>
        public override int ZeroValue
        {
            get { return _zeroValue; }
        }

        /// <summary>
        /// Minimum possible value to get from ADC.
        /// </summary>
        public override int MinValue
        {
            get { return _minValue16BitMode; }
        }

        /// <summary>
        /// Reads value from ADC.
        /// </summary>
        public override int Value
        {
            get 
            { 
                lock(_lockObject)
                    return ReadValue(); 
            }
        }

        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="i2cBus">I2C bus object.</param>
        /// <param name="address">I2C bus chip address.</param>
        /// <param name="busSpeedInKhz">I2C bus speed in Kiloherzes.</param>
        /// <param name="gain">ADC gain.</param>
        /// <param name="sampleRate">ADC sample rate.</param>
        /// <param name="conversionMode">ADC conversion mode.</param>
        public MCP3425(ref I2CDevice i2cBus, DeviceAddress address, int busSpeedInKhz, Gain gain, SampleRate sampleRate,
            ConversionMode conversionMode)
        {
            _lockObject = new object();
            _disposed = false;

            _i2cDeviceConfiguration = new I2CDevice.Configuration((ushort)address, busSpeedInKhz);
            if (i2cBus == null)
                i2cBus = new I2CDevice(_i2cDeviceConfiguration);
            _i2cBus = i2cBus;

            if ((conversionMode != ConversionMode.Continuous) || (sampleRate != SampleRate.Rate15Sps16Bits))
                throw new NotImplementedException();
            
            _writeDataBuffer = new byte[1];
            _readDataBuffer = new byte[3];
            _writeActionBuffer = new I2CDevice.I2CTransaction[1];
            _readActionBuffer = new I2CDevice.I2CTransaction[1];

            ChangeConfigurationPrivate(gain, sampleRate, conversionMode);
        }

        /// <summary>
        /// Changes configuration of MCP3425 ADC. 
        /// </summary>
        /// <param name="gain">ADC gain.</param>
        /// <param name="sampleRate">ADC sample rate.</param>
        /// <param name="conversionMode">ADC conversion mode.</param>
        public void ChangeConfiguration(Gain gain, SampleRate sampleRate,
            ConversionMode conversionMode)
        {
            lock (_lockObject)
            {
                if ((conversionMode != ConversionMode.Continuous) || (sampleRate != SampleRate.Rate15Sps16Bits))
                    throw new NotImplementedException();
                ChangeConfigurationPrivate(gain, sampleRate, conversionMode);
            }
        }


        private short ReadValue()
        {

            ReadBytesIntoBuffer();


            if (!CheckConfigurationByteOK(_readDataBuffer[2]))
            {
                //If badly configured - reconfigure
                WriteConfigurationRegister(_lastConfigurationRegistryState, ReadyBit.UpdatedOrNoEffect);

                ReadBytesIntoBuffer();

                if (!CheckConfigurationByteOK(_readDataBuffer[2]))
                    throw new InvalidOperationException("Unable to reconfigure MCP3425.");
            }

            return ConvertBytesToShort(_readDataBuffer[0], _readDataBuffer[1]);
        }

        private void ReadBytesIntoBuffer()
        {
            _readActionBuffer[0] = I2CDevice.CreateReadTransaction(_readDataBuffer);

            BusOperation(_readActionBuffer);
        }

        private void ChangeConfigurationPrivate(Gain gain, SampleRate sampleRate,
            ConversionMode conversionMode)
        {
            _adcGain = gain;
            _adcSampleRate = sampleRate;
            _conversionMode = conversionMode;
            byte newConfigurationRegistryState = (byte)((int)gain | (int)sampleRate | (int)conversionMode | (int)Channel.C0);

            WriteConfigurationRegister(newConfigurationRegistryState, ReadyBit.UpdatedOrNoEffect);

            _lastConfigurationRegistryState = newConfigurationRegistryState;
        }

        private void WriteConfigurationRegister(byte registryState, ReadyBit readyBit)
        {
            _writeDataBuffer[0] = (byte)(registryState | (int)readyBit);
            _writeActionBuffer[0] = I2CDevice.CreateWriteTransaction(_writeDataBuffer);

            BusOperation(_writeActionBuffer);
        }

        private void BusOperation(I2CDevice.I2CTransaction[] transactionBuffer)
        {
            lock (_i2cBus)
            {
                _i2cBus.Config = _i2cDeviceConfiguration;
                if (_i2cBus.Execute(transactionBuffer, _timeout) == 0)
                {
                    throw new InvalidOperationException("Unable to communicate with MCP3425.");
                }
            }

        }


        private bool CheckConfigurationByteOK(byte configurationByte)
        {
            configurationByte &= 0x7F; //mask bits 0-6 (omits RDY bit)

            if (configurationByte == _lastConfigurationRegistryState)
                return true;
            else
                return false;
        }


        private short ConvertBytesToShort(byte upperByte, byte lowerByte)
        {            
            uint returnValue = upperByte;
            returnValue = returnValue << 8;
            returnValue = returnValue | lowerByte;
            return (short)returnValue;
        }


        /// <summary>
        /// Disposes of I2C bus.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        protected override void Dispose(bool disposing)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    return;
                if (disposing)
                {
                    _i2cBus.Dispose();
                }
               
                _disposed = true;
            }
        }
    }
}
