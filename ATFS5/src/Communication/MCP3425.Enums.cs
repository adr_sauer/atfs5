using System;
using Microsoft.SPOT;

namespace ATFS5.src.Communication
{
    public sealed partial class MCP3425
    {
        /// <summary>
        /// Device address. Bits 0-2 represent given addres. Bits 3-7 are device code: 1101
        /// </summary>
        public enum DeviceAddress
        {
            /// <summary>
            /// Chip factory programmed to A0 address=0x68 
            /// </summary>
            A0 = 0x68, //1101000 | 000 (0)= 1101000 = 0x68
            /// <summary>
            /// Chip factory programmed to A1 address=0x69
            /// </summary>
            A1 = 0x69, //1101000 | 001 (1)= 1101001 = 0x69
            /// <summary>
            /// Chip factory programmed to A2 address=0x6A
            /// </summary>
            A2 = 0x6A, //1101000 | 010 (2)= 1101010 = 0x6A
            /// <summary>
            /// Chip factory programmed to A3 address=0x6B
            /// </summary>
            A3 = 0x6B  //1101000 | 011 (3)= 1101011 = 0x6B
        };

        /// <summary>
        /// Bit 4 O/C: Conversion Mode Bit
        /// 1 = Continuous Conversion Mode. Once this bit is selected, the device performs data conversions
        ///  continuously.
        /// 0 = One-Shot Conversion Mode. The device performs a single conversion and enters a low power
        /// standby mode until it receives another write/read command.
        /// </summary>
        public enum ConversionMode
        {
            /// <summary>
            /// One-Shot Conversion Mode. The device performs a single conversion and enters a low power standby mode until it receives another write/read command.
            /// </summary>
            OneShot = 0,
            /// <summary>
            /// Continuous Conversion Mode. The device performs data conversions continuously.
            /// </summary>
            Continuous = 0x10 //00010000=0x10
        };

        /// <summary>
        /// Bits 0-1 G1-G0: PGA Gain Selector Bits
        /// </summary>
        public enum Gain
        {
            /// <summary>
            /// Multiplies voltage by 1. 
            /// </summary>
            G1 = 0,
            /// <summary>
            /// Multiplies voltage by 2. 
            /// </summary>
            G2 = 0x01, //00000001=0x01
            /// <summary>
            /// Multiplies voltage by 4. 
            /// </summary>
            G4 = 0x02, //00000010=0x02
            /// <summary>
            /// Multiplies voltage by 8. 
            /// </summary>
            G8 = 0x03  //00000011=0x03
        };

        /// <summary>
        /// Bits 2-3 S1-S0: Sample Rate Selection Bit
        /// </summary>
        public enum SampleRate
        {
            /// <summary>
            /// Sample rate 240 samples per second. Value precision 12 bits.
            /// </summary>
            Rate240Sps12Bits = 0,
            /// <summary>
            /// Sample rate 60 samples per second. Value precision 14 bits.
            /// </summary>
            Rate60Sps14Bits = 0x04, //00000100=0x04
            /// <summary>
            /// Sample rate 15 samples per second. Value precision 16 bits.
            /// </summary>
            Rate15Sps16Bits = 0x08 //00001000=0x08
        };

        /// <summary>
        /// Bits 6-5 C1-C0: Channel Selection Bits
        /// These are the Channel Selection bits, but not used in the MCP3425 device.
        /// </summary>
        public enum Channel
        {
            /// <summary>
            /// Only one channel available. 
            /// </summary>
            C0 = 0
        };

        /// <summary>
        /// Bit 7 RDY: Ready Bit
        /// This bit is the data ready flag. In read mode, this bit indicates if the output register has been updated
        /// with a new conversion. In One-Shot Conversion mode, writing this bit to �1� initiates a new conversion.
        /// 
        /// Reading RDY bit with the read command:
        /// 1 = Output register has not been updated.
        /// 0 = Output register has been updated with the latest conversion data.
        /// 
        /// Writing RDY bit with the write command:
        /// Continuous Conversion mode: No effect
        /// One-Shot Conversion mode:
        /// 1 = Initiate a new conversion.
        /// 0 = No effect.
        /// </summary>
        public enum ReadyBit
        {
            /// <summary>
            /// If read from device than output register has been updated with the latest conversion data. If written to devide it has no effect.
            /// </summary>
            UpdatedOrNoEffect = 0,
            /// <summary>
            /// If read from device than output register has not been updated. If written to device it initiates a new conversion in Continuous Conversion mode.
            /// </summary>
            NotUpdatedOrInitiateConversion = 0x80 //10000000=0x80
        };
    }
}
