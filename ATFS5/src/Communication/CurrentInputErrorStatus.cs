using System;
using Microsoft.SPOT;

namespace ATFS5.src.Communication
{
    /// <summary>
    /// Enumeration of possible error statuses of current loop input.
    /// </summary>
    public enum CurrentInputErrorStatus
    {
        /// <summary>
        /// Current value in value range. (usually 4-20mA)
        /// </summary>
        NoError,
        /// <summary>
        /// Current below low current error threshold.
        /// </summary>
        LowCurrentError,
        /// <summary>
        /// Current below above current error threshold.
        /// </summary>
        HighCurrentError
    };
}
