using System;
using Microsoft.SPOT;

namespace ATFS5.src.Communication
{
    /// <summary>
    /// Encapsulates current input value and errors.
    /// </summary>
    public struct CurrentInputValue
    {
        
       
        private double _valueOn0To1Scale;
        private CurrentInputErrorStatus _errorStatus;

        /// <summary>
        /// Current input value on a scale from 0 to 1. Valid only if there is no error.
        /// </summary>
        public double ValueOn0To1Scale
        {
            get { return _valueOn0To1Scale; }
        }

        /// <summary>
        /// Current input error status.
        /// </summary>
        public CurrentInputErrorStatus ErrorStatus
        {
            get { return _errorStatus; }
        }

        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="valueOn0To1Scale">Current input value on a scale from 0 to 1.</param>
        /// <param name="errorStatus">Current input error status.</param>
        public CurrentInputValue(double valueOn0To1Scale, CurrentInputErrorStatus errorStatus)
        {
            _valueOn0To1Scale = valueOn0To1Scale;
            _errorStatus = errorStatus;
        }

        /// <summary>
        /// Represents current input value as a string.
        /// </summary>
        /// <returns>String representation of a value betweeen 0 and 1 or error message.</returns>
        public override string ToString()
        {
            string returnValue="";
            if (_errorStatus == CurrentInputErrorStatus.NoError)
            {
                returnValue=_valueOn0To1Scale.ToString("F5");
            }
            else
            {
                if(_errorStatus == CurrentInputErrorStatus.LowCurrentError)
                    returnValue="Low current error";
                else if(_errorStatus == CurrentInputErrorStatus.HighCurrentError)
                    returnValue = "High current error";
            }
            return returnValue;
        }
    }
}
