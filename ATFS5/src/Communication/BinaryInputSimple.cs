using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace ATFS5.src.Communication
{
    /// <summary>
    /// Represents a simple binary input which raises no events.
    /// </summary>
    public class BinaryInputSimple : IDisposable
    {
        private bool _activeState;
        private InputPort _input;
        private bool _disposed = false;

        /// <summary>
        /// <b>True</b> if input is in active state. <b>False</b> otherwise.
        /// </summary>
        public bool IsActive
        {
            get
            {
                bool inputState;
                inputState = _input.Read();
                if (!_activeState)
                    inputState = !inputState;

                return inputState;
            }
        }

        /// <summary>
        /// Initializes a new instance of the BinaryInputSimple class using specified pin and pin state considered as input active state.
        /// </summary>
        /// <param name="button_pin">Input pin.</param>
        /// <param name="active_state">Pin state considered as input active state.</param>
        public BinaryInputSimple(Cpu.Pin button_pin, bool active_state)
        {
            _disposed = false;
            this._activeState = active_state;
            _input = new InputPort(button_pin, true, Port.ResistorMode.PullDown);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes of input port.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        protected void Dispose(bool disposing)
        {
            
                if (_disposed)
                    return;
                if (disposing)
                {
                    _input.Dispose();
                }

                _disposed = true;
            
        }
    }
}
