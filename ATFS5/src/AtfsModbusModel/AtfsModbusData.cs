using System;
using Microsoft.SPOT;
using ATFS5.src.Modbus;
using ATFS5.src.Controllers;

namespace ATFS5.src.AtfsModbusModel
{
    /// <summary>
    /// <para><b>Represents Automatic Tank Filling System Modbus data model.</b></para>
    /// 
    /// </summary>
    class AtfsModbusData : ModbusData
    {
        private ControllersContext _controllersContext;
        private object _lockObject;

        private const ushort _beginPouringOnWriteRegistry = 0;

        private DateTime _dateTimeToSetRemotely = DateTime.Now;

        private ushort _waterKgToPourInRemoteOperationRegistry = 0;
        private ushort _mediumKgToPourInRemoteOperationRegistry = 0;
        private ushort _secondsToMixInRemoteOperationRegistry = 0;
        private ushort _remoteOperationOrderInSeriesRegistry = 0;
        private ushort _idOfProductInRemoteOperationRegistry = 0;
        private ushort _idOfMediumToPourInRemoteOperationRegistry = 0;

        public AtfsModbusData(ControllersContext context)
        {
            _lockObject = new object();
            _controllersContext = context;
        }

        public override ushort ReadInputRegister(ushort address)
        {
            lock (_lockObject)
            {
                SystemStatusData statusData = null;
                try
                {
                    statusData = _controllersContext.RemoteCommandGetSystemStatusData();
                }
                catch (SystemInitializingException)
                {
                    throw new ModbusServerDeviceFailureException();
                }
                return ReadInputRegisterFromSystemStatusDataObject(statusData, address);
            }
        }

        public override ushort[] ReadInputRegisters(ushort startAddress, ushort quantity)
        {
            lock (_lockObject)
            {
                SystemStatusData statusData = null;
                try
                {
                    statusData = _controllersContext.RemoteCommandGetSystemStatusData();
                }
                catch (SystemInitializingException)
                {
                    throw new ModbusServerDeviceFailureException();
                }
                ushort[] returnArray = new ushort[quantity];
                for (int i = 0; i < quantity; i++, startAddress++)
                {
                    returnArray[i] = ReadInputRegisterFromSystemStatusDataObject(statusData, startAddress);
                }
                return returnArray;
            }
        }

        public override ushort ReadHoldingRegister(ushort address)
        {
            lock (_lockObject)
            {
                return ReadHoldingRegisterInternal(address);
            }
        }

        public override ushort[] ReadHoldingRegisters(ushort startAddress, ushort quantity)
        {
            lock (_lockObject)
            {
                ushort[] returnArray = new ushort[quantity];
                for (int i = 0; i < quantity; i++, startAddress++)
                {
                    returnArray[i] = ReadHoldingRegisterInternal(startAddress);
                }
                return returnArray;
            }
        }

        public override void WriteToHoldingRegister(ushort address, ushort value)
        {
            lock (_lockObject)
            {
                WriteHoldingRegisterInternal(address, value);
            }
        }

        public override void WriteToHoldingRegisters(ushort startAddress, ushort[] values)
        {
            lock (_lockObject)
            {

                for (int i = 0; i < values.Length; i++, startAddress++)
                {
                    WriteHoldingRegisterInternal(startAddress, values[i]);
                }

            }
        }

        public override bool WriteToCoil(ushort address, bool value)
        {
            throw new NotImplementedException();
        }

        public override void WriteToCoils(ushort startAddress, bool[] values)
        {
            throw new NotImplementedException();
        }

        private void WriteHoldingRegisterInternal(ushort address, ushort value)
        {
            switch (address)
            {
                case 0:
                    try
                    {
                        _dateTimeToSetRemotely = new DateTime(_dateTimeToSetRemotely.Year, _dateTimeToSetRemotely.Month,
                            value, _dateTimeToSetRemotely.Hour, _dateTimeToSetRemotely.Minute, _dateTimeToSetRemotely.Second);
                    }
                    catch
                    {
                        throw new ModbusServerDeviceFailureException();
                    }
                    break;
                case 1:
                    try
                    {
                        _dateTimeToSetRemotely = new DateTime(_dateTimeToSetRemotely.Year, value,
                            _dateTimeToSetRemotely.Day, _dateTimeToSetRemotely.Hour, _dateTimeToSetRemotely.Minute, _dateTimeToSetRemotely.Second);
                    }
                    catch
                    {
                        throw new ModbusServerDeviceFailureException();
                    }
                    break;
                case 2:
                    try
                    {
                        _dateTimeToSetRemotely = new DateTime(value, _dateTimeToSetRemotely.Month,
                            _dateTimeToSetRemotely.Day, _dateTimeToSetRemotely.Hour, _dateTimeToSetRemotely.Minute, _dateTimeToSetRemotely.Second);
                    }
                    catch
                    {
                        throw new ModbusServerDeviceFailureException();
                    }
                    break;
                case 3:
                    try
                    {
                        _dateTimeToSetRemotely = new DateTime(_dateTimeToSetRemotely.Year, _dateTimeToSetRemotely.Month,
                            _dateTimeToSetRemotely.Day, value, _dateTimeToSetRemotely.Minute, _dateTimeToSetRemotely.Second);
                    }
                    catch
                    {
                        throw new ModbusServerDeviceFailureException();
                    }
                    break;
                case 4:
                    try
                    {
                        _dateTimeToSetRemotely = new DateTime(_dateTimeToSetRemotely.Year, _dateTimeToSetRemotely.Month,
                            _dateTimeToSetRemotely.Day, _dateTimeToSetRemotely.Hour, value, _dateTimeToSetRemotely.Second);
                    }
                    catch
                    {
                        throw new ModbusServerDeviceFailureException();
                    }
                    break;
                case 5:
                    try
                    {
                        _dateTimeToSetRemotely = new DateTime(_dateTimeToSetRemotely.Year, _dateTimeToSetRemotely.Month,
                            _dateTimeToSetRemotely.Day, _dateTimeToSetRemotely.Hour, _dateTimeToSetRemotely.Minute, value);
                        _controllersContext.RemoteCommandSetSystemClock(_dateTimeToSetRemotely);
                    }
                    catch
                    {
                        throw new ModbusServerDeviceFailureException();
                    }
                    break;
                case 6:
                    _waterKgToPourInRemoteOperationRegistry = value;
                    break;
                case 7:
                    _mediumKgToPourInRemoteOperationRegistry = value;
                    break;
                case 8:
                    _secondsToMixInRemoteOperationRegistry = value;
                    break;
                case 9:
                    _remoteOperationOrderInSeriesRegistry = value;
                    break;
                case 10:
                    _idOfProductInRemoteOperationRegistry = value;
                    break;
                case 11:
                    _idOfMediumToPourInRemoteOperationRegistry = value;
                    break;
                case 12:
                    //TODO Begin pouring
                    int a = 0;
                    break;
                default:
                    throw new ModbusIllegalDataAddressException();

            }
        }

        private ushort ReadHoldingRegisterInternal(ushort address)
        {
            switch (address)
            {
                case 0:
                    return (ushort)_dateTimeToSetRemotely.Day;
                case 1:
                    return (ushort)_dateTimeToSetRemotely.Month;
                case 2:
                    return (ushort)_dateTimeToSetRemotely.Year;
                case 3:
                    return (ushort)_dateTimeToSetRemotely.Hour;
                case 4:
                    return (ushort)_dateTimeToSetRemotely.Minute;
                case 5:
                    return (ushort)_dateTimeToSetRemotely.Second;
                case 6:
                    return _waterKgToPourInRemoteOperationRegistry;
                case 7:
                    return _mediumKgToPourInRemoteOperationRegistry;
                case 8:
                    return _secondsToMixInRemoteOperationRegistry;
                case 9:
                    return _remoteOperationOrderInSeriesRegistry;
                case 10:
                    return _idOfProductInRemoteOperationRegistry;
                case 11:
                    return _idOfMediumToPourInRemoteOperationRegistry;
                case 12:
                    return _beginPouringOnWriteRegistry;
                default:
                    throw new ModbusIllegalDataAddressException();

            }
        }


        private static ushort ReadInputRegisterFromSystemStatusDataObject(SystemStatusData statusData, ushort address)
        {
            switch (address)
            {
                case 0:
                    return (ushort)statusData.StatusDate.Day;
                case 1:
                    return (ushort)statusData.StatusDate.Month;
                case 2:
                    return (ushort)statusData.StatusDate.Year;
                case 3:
                    return (ushort)statusData.StatusDate.Hour;
                case 4:
                    return (ushort)statusData.StatusDate.Minute;
                case 5:
                    return (ushort)statusData.StatusDate.Second;
                default:
                    throw new ModbusIllegalDataAddressException();

            }

        }
    }
}
