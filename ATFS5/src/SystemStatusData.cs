using System;
using Microsoft.SPOT;
using ATFS5.src.OperationProgressTrackerLib;
using NetMf.CommonExtensions;

namespace ATFS5.src
{
    public class SystemStatusData
    {
        public enum SystemState
        {
            Waiting, FillingMedium, FillingWater, FillingBoth, Mixing, LocalInterfaceActive
        };

        private DateTime _statusDate;
        private TimeSpan _machineTime;
        private SystemState _state;
        private double _waterFlowKgPerMin;
        private double _mediumFlowKgPerMin;
        private int _mediumId;
        private int _waterToPourKg;
        private int _mediumToPourKg;
        private int _waterPouredKg;
        private int _mediumPouredKg;
        private int _timeMixedSeconds;
        private int _timeToMixSeconds;
        private bool _unfinishedOperationExists;
        private OperationProgressTrack _unfinishedOperationData;
        private bool _safetyButtonActive;
        private FlowError _currentMediumFlowError;
        private FlowError _currentWaterFlowError;
        private FlowError _lastMediumFlowPauseCauseError;
        private FlowError _lastWaterFlowPauseCauseError;
        private bool emptyingPumpActive;
        private uint _operationId;
        private uint _seriesId;
        private int _productId;
        private OperationOrderInSeries _operationOrderInSeries;

        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { _statusDate = value; }
        }        

        public TimeSpan MachineTime
        {
            get { return _machineTime; }
            set { _machineTime = value; }
        }

        public SystemState State
        {
            get { return _state; }
            set { _state = value; }
        }

        public double WaterFlowKgPerMin
        {
            get { return _waterFlowKgPerMin; }
            set { _waterFlowKgPerMin = value; }
        }

        public double MediumFlowKgPerMin
        {
            get { return _mediumFlowKgPerMin; }
            set { _mediumFlowKgPerMin = value; }
        }        

        public int MediumId
        {
            get { return _mediumId; }
            set { _mediumId = value; }
        }        

        public int WaterToPourKg
        {
            get { return _waterToPourKg; }
            set { _waterToPourKg = value; }
        }        

        public int MediumToPourKg
        {
            get { return _mediumToPourKg; }
            set { _mediumToPourKg = value; }
        }

        public int WaterPouredKg
        {
            get { return _waterPouredKg; }
            set { _waterPouredKg = value; }
        }        

        public int MediumPouredKg
        {
            get { return _mediumPouredKg; }
            set { _mediumPouredKg = value; }
        }       

        public int TimeMixedSeconds
        {
            get { return _timeMixedSeconds; }
            set { _timeMixedSeconds = value; }
        }        

        public int TimeToMixSeconds
        {
            get { return _timeToMixSeconds; }
            set { _timeToMixSeconds = value; }
        }        

        public bool UnfinishedOperationExists
        {
            get { return _unfinishedOperationExists; }
            set { _unfinishedOperationExists = value; }
        }        

        public OperationProgressTrack UnfinishedOperationData
        {
            get { return _unfinishedOperationData; }
            set { _unfinishedOperationData = value; }
        }

        public bool SafetyButtonActive
        {
            get { return _safetyButtonActive; }
            set { _safetyButtonActive = value; }
        }

        public FlowError CurrentMediumFlowError
        {
            get { return _currentMediumFlowError; }
            set { _currentMediumFlowError = value; }
        }        

        public FlowError CurrentWaterFlowError
        {
            get { return _currentWaterFlowError; }
            set { _currentWaterFlowError = value; }
        }      

        public FlowError LastMediumFlowPauseCauseError
        {
            get { return _lastMediumFlowPauseCauseError; }
            set { _lastMediumFlowPauseCauseError = value; }
        }        

        public FlowError LastWaterFlowPauseCauseError
        {
            get { return _lastWaterFlowPauseCauseError; }
            set { _lastWaterFlowPauseCauseError = value; }
        }        

        public bool EmptyingPumpActive
        {
            get { return emptyingPumpActive; }
            set { emptyingPumpActive = value; }
        }        

        public uint OperationId
        {
            get { return _operationId; }
            set { _operationId = value; }
        }        

        public uint SeriesId
        {
            get { return _seriesId; }
            set { _seriesId = value; }
        }        

        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }

        public OperationOrderInSeries OperationOrderInSeries
        {
            get { return _operationOrderInSeries; }
            set { _operationOrderInSeries = value; }
        }

        public SystemStatusData()
        {
            Reset();
        }

        public void Reset()
        {
            StatusDate = DateTime.MinValue;
            MachineTime = TimeSpan.Zero;
            State = SystemStatusData.SystemState.Waiting;
            WaterFlowKgPerMin = 0;
            MediumFlowKgPerMin = 0;
            MediumId = 0;
            WaterToPourKg = 0;
            MediumToPourKg = 0;
            WaterPouredKg = 0;
            MediumPouredKg = 0;
            TimeToMixSeconds = 0;
            TimeMixedSeconds = 0;
            UnfinishedOperationExists = false;
            UnfinishedOperationData = null;
            SafetyButtonActive = false;
            CurrentWaterFlowError = FlowError.NoError;
            CurrentMediumFlowError = FlowError.NoError;
            LastWaterFlowPauseCauseError = FlowError.NoError;
            LastMediumFlowPauseCauseError = FlowError.NoError;
            EmptyingPumpActive = false;
            OperationId = 0;
            SeriesId = 0;
            ProductId = 0;
            OperationOrderInSeries = OperationOrderInSeries.Begin;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("System date and time: {0}\n", StatusDate);
            builder.AppendFormat("System up time: {0}\n", MachineTime);
            builder.AppendFormat("System state: {0}\n", State.ToString());
            builder.AppendFormat("Water flow kg/min: {0:F1}\n", WaterFlowKgPerMin);
            builder.AppendFormat("Medium flow kg/min: {0:F1}\n", MediumFlowKgPerMin);
            builder.AppendFormat("Medium ID: {0}\n",MediumId);
            builder.AppendFormat("Water to pour kg: {0}\n", WaterToPourKg);
            builder.AppendFormat("Medium to pour kg: {0}\n", MediumToPourKg);
            builder.AppendFormat("Water poured kg: {0}\n", WaterPouredKg);
            builder.AppendFormat("Medium poured kg: {0}\n", MediumPouredKg);
            builder.AppendFormat("Time to mix seconds: {0}\n", TimeToMixSeconds);
            builder.AppendFormat("Time mixed seconds: {0}\n", TimeMixedSeconds);
            if ((UnfinishedOperationExists) && (UnfinishedOperationData != null))
            {
                builder.AppendFormat("Unfinished operation medium ID: {0}\n", UnfinishedOperationData.MediumId);
                builder.AppendFormat("Unfinished operation water to pour kg: {0}\n", UnfinishedOperationData.WaterMassToPourKg);
                builder.AppendFormat("Unfinished operation medium to pour kg: {0}\n", UnfinishedOperationData.MediumMassToPourKg);
                builder.AppendFormat("Unfinished operation water poured kg: {0}\n", UnfinishedOperationData.WaterMassPouredKg);
                builder.AppendFormat("Unfinished operation medium poured kg: {0}\n", UnfinishedOperationData.MediumMassPouredKg);
                builder.AppendFormat("Unfinished operation time to mix seconds: {0}\n", UnfinishedOperationData.TimeToMixInSeconds);
                builder.AppendFormat("Unfinished operation time mixed seconds: {0}\n", UnfinishedOperationData.TimeMixedInSeconds);
                builder.AppendFormat("Unfinished operation operation ID: {0}\n", UnfinishedOperationData.OperationId);
                builder.AppendFormat("Unfinished operation operation series ID: {0}\n", UnfinishedOperationData.SeriesId);
                builder.AppendFormat("Unfinished operation product ID: {0}\n", UnfinishedOperationData.ProductId);
                builder.AppendFormat("Unfinished operation operation order in series: {0}\n", UnfinishedOperationData.OperationOrderInSeriesTrack.DisplayFriendlyString());
            }
            else
            {
                builder.Append("No unfinished operation\n");
            }
            builder.AppendFormat("Safety button active: {0}\n", SafetyButtonActive);
            builder.AppendFormat("Current water flow errors: {0}\n", CurrentWaterFlowError.ToFriendlyString());
            builder.AppendFormat("Current medium flow errors: {0}\n", CurrentMediumFlowError.ToFriendlyString());
            builder.AppendFormat("Water flow error that caused last alarm and pouring pause: {0}\n", LastWaterFlowPauseCauseError.ToFriendlyString());
            builder.AppendFormat("Medium flow error that caused last alarm and pouring pause: {0}\n", LastMediumFlowPauseCauseError.ToFriendlyString());
            builder.AppendFormat("Tank emptying pump active: {0}\n", EmptyingPumpActive);
            builder.AppendFormat("Operation ID: {0}\n", OperationId);
            builder.AppendFormat("Operation series ID: {0}\n", SeriesId);
            builder.AppendFormat("Product ID: {0}\n", ProductId);
            builder.AppendFormat("Operation order in series: {0}\n", OperationOrderInSeries.DisplayFriendlyString());

            return builder.ToString();
        }

    }
}
