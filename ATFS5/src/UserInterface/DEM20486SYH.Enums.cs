using System;
using Microsoft.SPOT;

namespace ATFS5.src.UserInterface
{
    public sealed partial class DEM20486SYH
    {
        //public enum ShiftWhat { Cursor, Display };
        //public enum ShiftDirection { Left, Right };

        /// <summary>
        /// Shift display memory or move memory address cursor.
        /// </summary>
        public enum Shift
        {
            /// <summary>
            /// Shift display memory.
            /// </summary>
            On,
            /// <summary>
            /// Move memory address cursor.
            /// </summary>
            Off
        };

        /// <summary>
        /// Cursor move direction.
        /// </summary>
        public enum CursorMoveDirection
        {
            /// <summary>
            /// Increase RAM address counter after writing to RAM.
            /// </summary>
            Increase,
            /// <summary>
            /// Decrease RAM address counter after writing to RAM.
            /// </summary>
            Decrease
        };

        /// <summary>
        /// HD44780 display lines connection.
        /// </summary>
        public enum Lines
        {
            /// <summary>
            /// Connect as to a one line display.
            /// </summary>
            One,
            /// <summary>
            /// Connect as to a two line display.
            /// </summary>
            Two
        };

        /// <summary>
        /// Display power status.
        /// </summary>
        public enum DisplayPower
        {
            /// <summary>
            /// Display power on.
            /// </summary>
            On,
            /// <summary>
            /// Display power off.
            /// </summary>
            Off
        };

        /// <summary>
        /// Display cursor status.
        /// </summary>
        public enum CursorDisplay 
        { 
            /// <summary>
            /// Cursor visible.
            /// </summary>
            On,
            /// <summary>
            /// Cursor invisible.
            /// </summary>
            Off 
        };

        /// <summary>
        /// Character that the cursor is pointing to blinking status.
        /// </summary>
        public enum Blinking
        {
            /// <summary>
            /// Character is blinking.
            /// </summary>
            On,
            /// <summary>
            /// Character is not blinking.
            /// </summary>
            Off
        };

        /// <summary>
        /// Data bus width.
        /// </summary>
        public enum DataBusWidth
        {
            /// <summary>
            /// 8 bus bus.
            /// </summary>
            W8bit,
            /// <summary>
            /// 4 bit bus.
            /// </summary>
            W4bit
        };

        /// <summary>
        /// Display character type.
        /// </summary>
        public enum FontType
        {
            /// <summary>
            /// 5x8 dots characters.
            /// </summary>
            Char5X8,
            /// <summary>
            /// 5x11 dots characters.
            /// </summary>
            Char5X11
        };      
    }
}
