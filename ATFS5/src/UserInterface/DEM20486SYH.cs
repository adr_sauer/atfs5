using System;
using Microsoft.SPOT;
using System.Threading;
using Microsoft.SPOT.Hardware;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.UserInterface
{
    /// <summary>
    /// Class provides interface for the DEM20486SYH alphanumeric monochrome LCD display based on HD44780 compatible controller. 
    /// Only 8 bit bus communication is implemented.
    /// </summary>
    public sealed partial class DEM20486SYH : AlphanumericLCDDisplay
    {
        private const int _lineLenght = 20;
        private const int _lineCount = 4;
        private const int _clearDisplayWaitInMiliseconds = 2; // in ms
        private const int _returnHomeWaitInMiliseconds = 2; // in ms
        private const int _initialWaitInMiliseconds = 30; // in ms
        private readonly int[] _lineBeginningsRamAddresses = { 0x00, 0x40, 0x14, 0x54 };
        private readonly char[] _lineSeparators = { '\n' };

        private int _ePortPulseWaitTicks;
        private int _commandExecutionWaitTicks;
        private OutputPort _rsPort;
        private OutputPort _rwPort;
        private OutputPort _ePort;
        private OutputPort[] _dbPorts;
        private bool _disposed = false;
        private object _lockObject;

        /// <summary>
        /// LCD line count.
        /// </summary>
        public override int LinesCount
        {
            get { return _lineCount; }
        }

        /// <summary>
        /// LCD line width (characters number).
        /// </summary>
        public override int LineWidth
        {
            get { return _lineLenght; }
        }

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="rsPin">RS pin.</param>
        /// <param name="rwPin">RW pin.</param>
        /// <param name="ePin">E pin.</param>
        /// <param name="busWidth">Data bus width.</param>
        /// <param name="dbPins">4 or 8 DB pins.</param>
        public DEM20486SYH(Cpu.Pin rsPin, Cpu.Pin rwPin, Cpu.Pin ePin, DataBusWidth busWidth, params Cpu.Pin[] dbPins)
        {
            _lockObject = new object();
            _disposed = false;

            if (busWidth == DataBusWidth.W4bit)
                throw new NotImplementedException("4 bit bus is not implemented.");

            if (busWidth == DataBusWidth.W8bit)
            {
                if (dbPins.Length != 8)
                    throw new ArgumentException("Wrong pin configuration");
                _dbPorts = new OutputPort[8];
            }

            _rsPort = new OutputPort(rsPin, false);
            _rwPort = new OutputPort(rwPin, false);
            _ePort = new OutputPort(ePin, false);

            for (int i = 0; i < _dbPorts.Length; i++)
                _dbPorts[i] = new OutputPort(dbPins[i], false);

            _ePortPulseWaitTicks = (int)(System.TimeSpan.TicksPerMillisecond / 2000);
            _commandExecutionWaitTicks = (int)(System.TimeSpan.TicksPerMillisecond / 20);

            InitializeLcd();

        }

        /// <summary>
        /// Write character to given position on display. 
        /// </summary>
        /// <param name="lineNumber">Zero based number of display line from up.</param>
        /// <param name="offset">Zero based horizontal character position.</param>
        /// <param name="character">Character to write</param>
        public override void WriteCharToPosition(int lineNumber, int offset, char character)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                if ((lineNumber < 0) || (lineNumber >= _lineCount))
                    throw new ArgumentOutOfRangeException("linenr", "Wrong line number.");
                if ((offset < 0) || (offset >= _lineLenght))
                    throw new ArgumentOutOfRangeException("offset", "Wrong char offset.");

                SetDDRAMAdress(_lineBeginningsRamAddresses[lineNumber] + offset);

                WriteSingleDataByte((byte)character);
            }
        }

        /// <summary>
        /// Writes string to whole display.
        /// </summary>
        /// <param name="lcdContent">Newline split string. Number of lines in string must be equal to display line count. Lines separated only by LF.</param>
        public override void Write(string lcdContent)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                string[] lines = lcdContent.Split(_lineSeparators);
                if (lines.Length != _lineCount)
                    throw new ArgumentException("Wrong number of lines in argument string.");

                PrivateWriteToLine(lines[0], 0);
                PrivateWriteToLine(lines[1], 1);
                PrivateWriteToLine(lines[2], 2);
                PrivateWriteToLine(lines[3], 3);
            }
        }

        /// <summary>
        /// Displays string as a given line on display.
        /// </summary>
        /// <param name="lineContent">Display line string.</param>
        /// <param name="lineNumber">Zero based number of display line from up.</param>
        public override void WriteToLine(string lineContent, int lineNumber)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                PrivateWriteToLine(lineContent, lineNumber);
            }
        }

        /// <summary>
        /// Clears display.
        /// </summary>
        public override void ClearDisplay()
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                byte set = 1;
                WriteSingleInstructionByte(set);
                Thread.Sleep(_clearDisplayWaitInMiliseconds);
            }
        }

        /// <summary>
        /// Sends change EntryMode command.
        /// </summary>
        /// <param name="dir">Cursor direction.</param>
        /// <param name="shift">Shift memory or move cursor.</param>
        public void EntryMode(CursorMoveDirection dir, Shift shift)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                byte set = 4;
                if (dir == CursorMoveDirection.Increase)
                    set += 2;
                if (shift == Shift.On)
                    set += 1;
                WriteSingleInstructionByte(set);
            }
        }

        /// <summary>
        /// Sends DisplayControl command.
        /// </summary>
        /// <param name="power">Power display up or down.</param>
        /// <param name="cursor">Display cursor or not.</param>
        /// <param name="blinking">Enable blinking of the character that the cursor is pointing to.</param>
        public void DisplayControl(DisplayPower power, CursorDisplay cursor, Blinking blinking)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                byte set = 8;
                if (power == DisplayPower.On)
                    set += 4;
                if (cursor == CursorDisplay.On)
                    set += 2;
                if (blinking == Blinking.On)
                    set += 1;
                WriteSingleInstructionByte(set);
            }
        }

        /// <summary>
        /// Sends Function Set command.
        /// </summary>
        /// <param name="busWidth">Data bus width.</param>
        /// <param name="lines">Display lines.</param>
        /// <param name="fontType">Character font type.</param>
        public void FunctionSet(DataBusWidth busWidth, Lines lines, FontType fontType)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    throw new ObjectDisposedException();
                byte set = 32;
                if (busWidth == DataBusWidth.W8bit)
                    set += 16;
                if (lines == Lines.Two)
                    set += 8;
                if (fontType == FontType.Char5X11)
                    set += 4;
                WriteSingleInstructionByte(set);
            }
        }

        /// <summary>
        /// Disposes of input and output ports.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        protected override void Dispose(bool disposing)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    return;
                if (disposing)
                {
                    if (_rwPort != null)
                        _rwPort.Dispose();
                    if (_rsPort != null)
                        _rsPort.Dispose();
                    if (_ePort != null)
                        _ePort.Dispose();

                    foreach (IDisposable port in _dbPorts)
                    {
                        if (port != null)
                            port.Dispose();
                    }
                }
                _disposed = true;
            }
        }

        private void PrivateWriteToLine(string lineContent, int lineNumber)
        {
            if ((lineNumber < 0) || (lineNumber >= _lineCount))
                throw new ArgumentOutOfRangeException("linenr", "Wrong line number.");

            SetDDRAMAdress(_lineBeginningsRamAddresses[lineNumber]);

            WriteDataBytes(StringToTrimmedASCIIBytes(lineContent, _lineLenght));
        }

        private void SetDDRAMAdress(int address)
        {
            byte set = 128;
            if (address < 0)
                throw new ArgumentOutOfRangeException("address", "Address too low.");
            if (address > 127)
                throw new ArgumentOutOfRangeException("address", "Address too high.");
            set += (byte)address;
            WriteSingleInstructionByte(set);
        }

        private void WriteSingleInstructionByte(byte data)
        {
            _rsPort.Write(false);
            _rwPort.Write(false);
            SetByteOnOutput(data);
            PulseEPortToConfirmCommand();
            WaitForCommandExecution();

        }

        private void WriteSingleDataByte(byte data)
        {
            _rsPort.Write(true);
            _rwPort.Write(false);
            SetByteOnOutput(data);
            PulseEPortToConfirmCommand();
            WaitForCommandExecution();
        }

        private void WriteDataBytes(byte[] data)
        {
            _rsPort.Write(true);
            _rwPort.Write(false);
            foreach (byte b in data)
            {
                SetByteOnOutput(b);
                PulseEPortToConfirmCommand();
                WaitForCommandExecution();
            }
        }

        private string TrimOrBlankPadStringToDisplayWidth(string line, int lenght)
        {
            if (line.Length > lenght)
                line = line.Substring(0, lenght);

            if (line.Length < lenght)
            {
                int len = lenght - line.Length;
                line += new String(' ', len);
            }

            return line;
        }

        private byte[] StringToTrimmedASCIIBytes(string line, int lenght)
        {
            byte[] bytes;
            line = TrimOrBlankPadStringToDisplayWidth(line, lenght);
            bytes = Windows1250Encoding.Windows1250.GetBytes(line);
            return bytes;
        }

        private void SetByteOnOutput(byte bytetoset)
        {
            for (int i = 0, flag = 1; i < 8; i++, flag *= 2)
            {
                _dbPorts[i].Write((flag & (int)bytetoset) != 0);
            }
        }

        private void InitializeLcd()
        {
            Thread.Sleep(_initialWaitInMiliseconds);
            FunctionSet(DataBusWidth.W8bit, Lines.Two, FontType.Char5X8);
            ClearDisplay();
            EntryMode(CursorMoveDirection.Increase, Shift.Off);
            DisplayControl(DisplayPower.On, CursorDisplay.Off, Blinking.Off);
        }

        private void PulseEPortToConfirmCommand()
        {
            _ePort.Write(true);
            long ticks = Utility.GetMachineTime().Ticks;
            while (Utility.GetMachineTime().Ticks - ticks < _ePortPulseWaitTicks) ;
            _ePort.Write(false);
        }

        private void WaitForCommandExecution()
        {
            long ticks = Utility.GetMachineTime().Ticks;
            while (Utility.GetMachineTime().Ticks - ticks < _commandExecutionWaitTicks) ;
        }


    }
}
