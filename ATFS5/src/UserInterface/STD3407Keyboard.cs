using System;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.UserInterface
{
    /// <summary>
    /// Class provides interface for STD-34-07 matrix keyboard.
    /// </summary>
    public sealed class STD3407Keyboard : IATFSKeyboard
    {
        private const int _rowCount = 4;
        private const int _columnCount = 3;
        private const int _shiftKeyRow = 0, _shiftKeyColumn = 0;
        private const int _zeroKeyRow = 0, _zeroKeyColumn = 1;
        private const int _enterKeyRow = 0, _enterKeyColumn = 2;
        private const int _f1KeyRow = 1, _f1KeyColumn = 0;
        private const int _downKeyRow = 1, _downKeyColumn = 1;
        private const int _f2KeyRow = 1, _f2KeyColumn = 2;
        private const int _leftKeyRow = 2, _leftKeyColumn = 0;
        private const int _f3KeyRow = 2, _f3KeyColumn = 1;
        private const int _rightKeyRow = 2, _rightKeyColumn = 2;
        private const int _f4KeyRow = 3, _f4KeyColumn = 0;
        private const int _upKeyRow = 3, _upKeyColumn = 1;
        private const int _f5KeyRow = 3, _f5KeyColumn = 2;
        private const int _lineIntervalMiliseconds = 10;

        /// <summary>
        /// Occurs when up button is pressed.
        /// </summary>
        public event EventHandler UpPressed;

        /// <summary>
        /// Occurs when down button is pressed.
        /// </summary>
        public event EventHandler DownPressed;

        /// <summary>
        /// Occurs when left button is pressed.
        /// </summary>
        public event EventHandler LeftPressed;

        /// <summary>
        /// Occurs when right button is pressed.
        /// </summary>
        public event EventHandler RightPressed;

        /// <summary>
        /// Occurs when enter button is pressed.
        /// </summary>
        public event EventHandler EnterPressed;

        /// <summary>
        /// Occurs when shift button is pressed.
        /// </summary>
        public event EventHandler ShiftPressed;

        /// <summary>
        /// Occurs when F1 button is pressed.
        /// </summary>
        public event EventHandler F1Pressed;

        /// <summary>
        /// Occurs when F2 button is pressed.
        /// </summary>
        public event EventHandler F2Pressed;

        /// <summary>
        /// Occurs when F3 button is pressed.
        /// </summary>
        public event EventHandler F3Pressed;

        /// <summary>
        /// Occurs when F4 button is pressed.
        /// </summary>
        public event EventHandler F4Pressed;

        /// <summary>
        /// Occurs when F5 button is pressed.
        /// </summary>
        public event EventHandler F5Pressed;

        /// <summary>
        /// Occurs when zero button is pressed.
        /// </summary>
        public event EventHandler ZeroPressed;

        private InputPort[] _columnInputPorts;
        private OutputPort[] _rowOutputPorts;
        private bool[][] _buttonsStatus;

        private Thread _keyboardStateCheckingThread;
        private StopFlag _keyboardStateCheckingThreadStopFlag;
        private bool _disposed=false;

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="column1Pin">Pin connected to the first column from left edge.</param>
        /// <param name="column2Pin">Pin connected to the second column from left edge.</param>
        /// <param name="column3Pin">Pin connected to the third column from left edge.</param>
        /// <param name="row1Pin">Pin connected to the first row from bottom edge.</param>
        /// <param name="row2Pin">Pin connected to the second row from bottom edge.</param>
        /// <param name="row3Pin">Pin connected to the third row from bottom edge.</param>
        /// <param name="row4Pin">Pin connected to the fourth row from bottom edge.</param>
        public STD3407Keyboard(Cpu.Pin column1Pin, Cpu.Pin column2Pin, Cpu.Pin column3Pin,
            Cpu.Pin row1Pin, Cpu.Pin row2Pin, Cpu.Pin row3Pin, Cpu.Pin row4Pin)
        {
            _disposed = false;
            _keyboardStateCheckingThreadStopFlag = new StopFlag();

            _buttonsStatus = new bool[_rowCount][];
            for (int i = 0; i < _rowCount; i++)
                _buttonsStatus[i] = new bool[_columnCount];
            _columnInputPorts = new InputPort[_columnCount];
            _rowOutputPorts = new OutputPort[_rowCount];

            _columnInputPorts[0] = new InputPort(column1Pin, false, Port.ResistorMode.PullDown);
            _columnInputPorts[1] = new InputPort(column2Pin, false, Port.ResistorMode.PullDown);
            _columnInputPorts[2] = new InputPort(column3Pin, false, Port.ResistorMode.PullDown);

            _rowOutputPorts[0] = new OutputPort(row1Pin, false);
            _rowOutputPorts[1] = new OutputPort(row2Pin, false);
            _rowOutputPorts[2] = new OutputPort(row3Pin, false);
            _rowOutputPorts[3] = new OutputPort(row4Pin, false);


            _keyboardStateCheckingThread = new Thread(CheckKeyboard);
            _keyboardStateCheckingThread.Start();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        void CheckKeyboard()
        {
            try
            {
                while (_keyboardStateCheckingThreadStopFlag.KeepWorking)
                {
                    bool but;
                    for (int row = 0; row < _rowCount; row++)
                    {
                        _rowOutputPorts[row].Write(true);
                        Thread.Sleep(_lineIntervalMiliseconds);
                        for (int column = 0; column < _columnCount; column++)
                        {
                            but = _columnInputPorts[column].Read();
                            if (but && !_buttonsStatus[row][column])
                            {
                                _buttonsStatus[row][column] = true;
                                ReactToPush(row, column);
                            }
                            if (!but && _buttonsStatus[row][column])
                            {
                                _buttonsStatus[row][column] = false;
                            }

                        }
                        _rowOutputPorts[row].Write(false);
                        Thread.Sleep(_lineIntervalMiliseconds);
                    }
                }
            }
            catch (Exception e)
            {
                Utilities.HandleUnforeseenException(e);
            }

        }

        void ReactToPush(int row, int column)
        {
            if ((row == _upKeyRow) && (column == _upKeyColumn))
                OnUpPressed(EventArgs.Empty);
            if ((row == _downKeyRow) && (column == _downKeyColumn))
                OnDownPressed(EventArgs.Empty);
            if ((row == _leftKeyRow) && (column == _leftKeyColumn))
                OnLeftPressed(EventArgs.Empty);
            if ((row == _rightKeyRow) && (column == _rightKeyColumn))
                OnRightPressed(EventArgs.Empty);
            if ((row == _shiftKeyRow) && (column == _shiftKeyColumn))
                OnShiftPressed(EventArgs.Empty);
            if ((row == _enterKeyRow) && (column == _enterKeyColumn))
                OnEnterPressed(EventArgs.Empty);
            if ((row == _f1KeyRow) && (column == _f1KeyColumn))
                OnF1Pressed(EventArgs.Empty);
            if ((row == _f2KeyRow) && (column == _f2KeyColumn))
                OnF2Pressed(EventArgs.Empty);
            if ((row == _f3KeyRow) && (column == _f3KeyColumn))
                OnF3Pressed(EventArgs.Empty);
            if ((row == _f4KeyRow) && (column == _f4KeyColumn))
                OnF4Pressed(EventArgs.Empty);
            if ((row == _f5KeyRow) && (column == _f5KeyColumn))
                OnF5Pressed(EventArgs.Empty);
            if ((row == _zeroKeyRow) && (column == _zeroKeyColumn))
                OnZeroPressed(EventArgs.Empty);

        }


        private void OnUpPressed(EventArgs e)
        {
            EventHandler handler = UpPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnDownPressed(EventArgs e)
        {
            EventHandler handler = DownPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnLeftPressed(EventArgs e)
        {
            EventHandler handler = LeftPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnRightPressed(EventArgs e)
        {
            EventHandler handler = RightPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnEnterPressed(EventArgs e)
        {
            EventHandler handler = EnterPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnShiftPressed(EventArgs e)
        {
            EventHandler handler = ShiftPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnF1Pressed(EventArgs e)
        {
            EventHandler handler = F1Pressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnF2Pressed(EventArgs e)
        {
            EventHandler handler = F2Pressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnF3Pressed(EventArgs e)
        {
            EventHandler handler = F3Pressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnF4Pressed(EventArgs e)
        {
            EventHandler handler = F4Pressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnF5Pressed(EventArgs e)
        {
            EventHandler handler = F5Pressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void OnZeroPressed(EventArgs e)
        {
            EventHandler handler = ZeroPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                _keyboardStateCheckingThreadStopFlag.Stop();
                _keyboardStateCheckingThread.Join();
                foreach (IDisposable disposable in _rowOutputPorts)
                {
                    if (disposable != null)
                        disposable.Dispose();
                }

                foreach (IDisposable disposable in _columnInputPorts)
                {
                    if (disposable != null)
                        disposable.Dispose();
                }

            }
            _disposed = true;

        }

    }
}
