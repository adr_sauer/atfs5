using System;
using Microsoft.SPOT;

namespace ATFS5.src.UserInterface
{
    /// <summary>
    /// Provides interface for ATFS system keyboard.
    /// </summary>
    public interface IATFSKeyboard : IDisposable
    {
        /// <summary>
        /// Occurs when up button is pressed.
        /// </summary>
        event EventHandler UpPressed;

        /// <summary>
        /// Occurs when down button is pressed.
        /// </summary>
        event EventHandler DownPressed;

        /// <summary>
        /// Occurs when left button is pressed.
        /// </summary>
        event EventHandler LeftPressed;

        /// <summary>
        /// Occurs when right button is pressed.
        /// </summary>
        event EventHandler RightPressed;

        /// <summary>
        /// Occurs when enter button is pressed.
        /// </summary>
        event EventHandler EnterPressed;

        /// <summary>
        /// Occurs when shift button is pressed.
        /// </summary>
        event EventHandler ShiftPressed;

        /// <summary>
        /// Occurs when F1 button is pressed.
        /// </summary>
        event EventHandler F1Pressed;

        /// <summary>
        /// Occurs when F2 button is pressed.
        /// </summary>
        event EventHandler F2Pressed;

        /// <summary>
        /// Occurs when F3 button is pressed.
        /// </summary>
        event EventHandler F3Pressed;

        /// <summary>
        /// Occurs when F4 button is pressed.
        /// </summary>
        event EventHandler F4Pressed;

        /// <summary>
        /// Occurs when F5 button is pressed.
        /// </summary>
        event EventHandler F5Pressed;

        /// <summary>
        /// Occurs when zero button is pressed.
        /// </summary>
        event EventHandler ZeroPressed;
    }
}
