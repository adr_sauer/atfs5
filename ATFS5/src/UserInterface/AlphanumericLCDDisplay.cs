using System;
using Microsoft.SPOT;

namespace ATFS5.src.UserInterface
{
    /// <summary>
    /// Class provides general interface for alphanumeric monochrome LCD displays.
    /// </summary>
    public abstract class AlphanumericLCDDisplay : IDisposable
    {
        /// <summary>
        /// LCD line count.
        /// </summary>
        public abstract int LinesCount
        {
            get;
        }

        /// <summary>
        /// LCD line width (characters number).
        /// </summary>
        public abstract int LineWidth
        {
            get;
        }

        /// <summary>
        /// Writes string to whole display.
        /// </summary>
        /// <param name="lcdContent">Newline split string. Number of lines in string must be equal to display line count.</param>
        public abstract void Write(string lcdContent);

        /// <summary>
        /// Displays string as a given line on display.
        /// </summary>
        /// <param name="lineContent">Display line string.</param>
        /// <param name="lineNumber">Zero based number of display line from up.</param>
        public abstract void WriteToLine(string lineContent, int lineNumber);

        /// <summary>
        /// Write character to given position on display. 
        /// </summary>
        /// <param name="lineNumber">Zero based number of display line from up.</param>
        /// <param name="offset">Zero based horizontal character position.</param>
        /// <param name="character">Character to write</param>
        public abstract void WriteCharToPosition(int lineNumber, int offset, char character);

        /// <summary>
        /// Clears display.
        /// </summary>
        public abstract void ClearDisplay();

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        protected abstract void Dispose(bool disposing);

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
