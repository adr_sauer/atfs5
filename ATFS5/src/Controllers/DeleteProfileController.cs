using System;
using Microsoft.SPOT;
using ATFS5.src.Screens20x4;
using ATFS5.src.PouringProfilesListLib;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents system controller which controls the system when user is deleting profile. Analogue to State in State design pattern.
    /// </summary>
    public class DeleteProfileController : LocalInterfaceActiveController
    {
        private const string _emptyListScreenContent = "LISTA PUSTA\n \n \n ";

        private TwoLinePerDisplayListChoicePromptScreen _screen;

        /// <summary>
        /// Initializes a new instance of the DeleteProfileController class using the specified controller context.
        /// </summary>
        /// <param name="context">Controller context.</param>
        public DeleteProfileController(ControllersContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Starts controller.
        /// </summary>
        public override void Start()
        {
            PouringProfile[] pouringProfiles = Context.ContextPouringProfilesList.ToArray();
            if (pouringProfiles.Length == 0)
            {
                Context.Hardware.Display.Write(_emptyListScreenContent);
                Context.Hardware.Keyboard.ShiftPressed += Cancel;
            }
            else
            {
                _screen = new TwoLinePerDisplayListChoicePromptScreen(pouringProfiles, "WYBIERZ PROFIL", "ENTER USUWA");
                RegisterKeyboardEventHandlersToScreen(_screen);
                _screen.PositionChanged += (o, e) => {
                    Context.Hardware.Display.Write(_screen.FullScreenString);
                };
                Context.Hardware.Display.Write(_screen.FullScreenString);
                Context.Hardware.Keyboard.EnterPressed += ChooseItem;
                Context.Hardware.Keyboard.ShiftPressed += Cancel;
            }
        }

        /// <summary>
        /// Stops controller
        /// </summary>
        public override void Stop()
        {
            UnRegisterKeyEventHandlers();
            Context.Hardware.Keyboard.EnterPressed -= ChooseItem;
            Context.Hardware.Keyboard.ShiftPressed -= Cancel;
        }

        private void ChooseItem(object o, EventArgs e)
        {
            UnRegisterKeyEventHandlers();
            try
            {
                Context.ContextPouringProfilesList.Remove(_screen.SelectedItem as PouringProfile);
            }
            catch (Exception ex)
            {
                Utilities.ProgramLogger.LogError("Exception occured when adding profile");
                Utilities.ProgramLogger.LogException(ex);
            }
            Context.SetController(new MainMenuController(Context));
        }

        private void Cancel(object o, EventArgs e)
        {
            Context.SetController(new MainMenuController(Context));
        }
    }
}
