using System;
using Microsoft.SPOT;
using ATFS5.src.Screens20x4;
using ATFS5.src.PouringProfilesListLib;
using ATFS5.src.UtilitiesLib;
using ATFS5.src.ProductsListLib;
using ATFS5.src.MediumsListLib;


namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents system controller which controls the system during pouring operation setup (profile, medium, product, series order). Analogue to State in State design pattern.
    /// </summary>
    public class OperationSetupController : LocalInterfaceActiveController
    {
        private PouringProfile _chosenPouringProfile;
        private Product _chosenProduct;
        private OperationOrderInSeries _chosenOperationOrderInSeries;
        private Medium _chosenMedium;

        private class DummyProfileForFillingWithoutProfile : ITwoLineDisplayable
        {

            public void GetTwoDisplayableLines(out string line1, out string line2)
            {
                line1 = "NALEWANIE";
                line2 = "BEZ PROFILU";
            }
        }


        private TwoLinePerDisplayListChoicePromptScreen _screen;

        /// <summary>
        /// Initializes a new instance of the OperationSetupController class using the specified controller context.
        /// </summary>
        /// <param name="context">Controller context.</param>
        public OperationSetupController(ControllersContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Starts controller.
        /// </summary>
        public override void Start()
        {
            ITwoLineDisplayable[] profiles = Context.ContextPouringProfilesList.ToArray();
            var menuItems = new ITwoLineDisplayable[profiles.Length + 1];
            menuItems[0] = new DummyProfileForFillingWithoutProfile();
            profiles.CopyTo(menuItems, 1);
            _screen = new TwoLinePerDisplayListChoicePromptScreen(menuItems, "WYBIERZ PROFIL", "ENTER POTWIERDZA");

            RegisterKeyboardEventHandlersToScreen(_screen);
            _screen.PositionChanged += (o, e) =>
            {
                Context.Hardware.Display.Write(_screen.FullScreenString);
            };
            Context.Hardware.Display.Write(_screen.FullScreenString);
            Context.Hardware.Keyboard.EnterPressed += ChooseProfile;
            Context.Hardware.Keyboard.ShiftPressed += Cancel;
        }

        /// <summary>
        /// Stops controller
        /// </summary>
        public override void Stop()
        {
            UnRegisterKeyEventHandlers();
            Context.Hardware.Keyboard.EnterPressed -= ChooseProfile;
            Context.Hardware.Keyboard.EnterPressed -= ChooseMedium;
            Context.Hardware.Keyboard.EnterPressed -= ChooseProduct;
            Context.Hardware.Keyboard.EnterPressed -= ChooseOperationOrderInSeries;
            Context.Hardware.Keyboard.ShiftPressed -= Cancel;
        }


        private void ChooseProfile(object o, EventArgs e)
        {
            Context.Hardware.Keyboard.EnterPressed -= ChooseProfile;
            UnRegisterKeyEventHandlers();
            ITwoLineDisplayable selectedItem= _screen.SelectedItem;
            if (selectedItem is DummyProfileForFillingWithoutProfile)
            {
                PourWithoutUsingSavedProfile();
            }
            else
            {
                _chosenPouringProfile = selectedItem as PouringProfile;
                ConfirmProfileChoiceAndDisplayMediumMenu();
            }
        }

        private void ChooseMedium(object o, EventArgs e)
        {
            Context.Hardware.Keyboard.EnterPressed -= ChooseMedium;
            UnRegisterKeyEventHandlers();
            _chosenMedium = _screen.SelectedItem as Medium;
            ConfirmMediumChoiceAndDisplayProductMenu();
        }

        private void ChooseProduct(object o, EventArgs e)
        {
            Context.Hardware.Keyboard.EnterPressed -= ChooseProduct;
            UnRegisterKeyEventHandlers();
            _chosenProduct = _screen.SelectedItem as Product;
            ConfirmProductChoiceAndDisplayOrderInSeriesMenu();
        }

        private void ChooseOperationOrderInSeries(object o, EventArgs e)
        {
            Context.Hardware.Keyboard.EnterPressed -= ChooseOperationOrderInSeries;
            UnRegisterKeyEventHandlers();
            _chosenOperationOrderInSeries = (_screen.SelectedItem as OperationOrderInSeriesTwoLineDisplayable).UnderlyingEnum;

            Controller p = new PouringController(Context, _chosenPouringProfile.WaterMass, _chosenPouringProfile.MediumMass, _chosenPouringProfile.MixingTimeInSeconds, _chosenMedium.Id, _chosenProduct.Id, _chosenOperationOrderInSeries, 0, 0, 0, 0);
            Context.SetController(p);
        }

        private void Cancel(object o, EventArgs e)
        {
            Context.SetController(new MainMenuController(Context));
        }

        private void ConfirmProfileChoiceAndDisplayMediumMenu()
        {
            ITwoLineDisplayable[] items = Context.ContextMediumsList.ToArray();
            _screen = new TwoLinePerDisplayListChoicePromptScreen(items, "WYBIERZ MEDIUM", "ENTER POTWIERDZA");
            RegisterKeyboardEventHandlersToScreen(_screen);
            _screen.PositionChanged += (o, e) =>
            {
                Context.Hardware.Display.Write(_screen.FullScreenString);
            };
            Context.Hardware.Display.Write(_screen.FullScreenString);
            Context.Hardware.Keyboard.EnterPressed += ChooseMedium;
        }

        private void ConfirmMediumChoiceAndDisplayProductMenu()
        {
            ITwoLineDisplayable[] items = Context.ContextProductsList.ToArray();
            _screen = new TwoLinePerDisplayListChoicePromptScreen(items, "WYBIERZ PRODUKT", "ENTER POTWIERDZA");
            RegisterKeyboardEventHandlersToScreen(_screen);
            _screen.PositionChanged += (o, e) =>
            {
                Context.Hardware.Display.Write(_screen.FullScreenString);
            };
            Context.Hardware.Display.Write(_screen.FullScreenString);
            Context.Hardware.Keyboard.EnterPressed += ChooseProduct;
        }

        private void ConfirmProductChoiceAndDisplayOrderInSeriesMenu()
        {
            ITwoLineDisplayable[] items = OperationOrderInSeries.Begin.GetTwoLineDisplayables();
            _screen = new TwoLinePerDisplayListChoicePromptScreen(items, "TYP OPERACJI W SERII", "ENTER POTWIERDZA");
            RegisterKeyboardEventHandlersToScreen(_screen);
            _screen.PositionChanged += (o, e) =>
            {
                Context.Hardware.Display.Write(_screen.FullScreenString);
            };
            Context.Hardware.Display.Write(_screen.FullScreenString);
            Context.Hardware.Keyboard.EnterPressed += ChooseOperationOrderInSeries;
        }

        private void PourWithoutUsingSavedProfile()
        {
            //TODO Pouring without using saved profile. Currently hack - go main menu
            Context.SetController(new MainMenuController(Context));
        }
    }
}
