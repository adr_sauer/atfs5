using System;
using Microsoft.SPOT;
using ATFS5.src.MediumsListLib;
using ATFS5.src.OperationProgressTrackerLib;
using ATFS5.src.PouringProfilesListLib;
using ATFS5.src.ProductsListLib;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents system controllers execution context. Analogue to Context in State design pattern.
    /// </summary>
    public class ControllersContext
    {
        private Controller _currentController;
        private ATFSHardware _hardware;
        private MediumsList _mediumsList = null;
        private OperationProgressTracker _operationProgressTracker = null;
        private PouringProfilesList _pouringProfilesList = null;
        private ProductsList _productsList = null;
        private object _lockObject;

        /// <summary>
        /// Gets system hardware access object.
        /// </summary>
        public ATFSHardware Hardware
        {
            get
            {
                return _hardware;
            }
        }

        /// <summary>
        /// Gets mediums list
        /// </summary>
        public MediumsList ContextMediumsList
        {
            get
            {
                return _mediumsList;
            }
        }

        /// <summary>
        /// Gets products list.
        /// </summary>
        public ProductsList ContextProductsList
        {
            get
            {
                return _productsList;
            }
        }

        /// <summary>
        /// Gets operation progress tracker.
        /// </summary>
        public OperationProgressTracker ContextOperationProgressTrack
        {
            get
            {
                return _operationProgressTracker;
            }
        }

        /// <summary>
        /// Gets pouring profiles list.
        /// </summary>
        public PouringProfilesList ContextPouringProfilesList
        {
            get
            {
                return _pouringProfilesList;
            }
        }

        /// <summary>
        /// <b>True</b> if system is in idle state (main menu). <b>False</b> otherwise.
        /// </summary>
        public bool IsSystemIdle
        {
            get
            {
                lock (_lockObject)
                    return _currentController is MainMenuController;
            }
        }

        /// <summary>
        /// Initializes a new instance of the ControllersContext class using the specified system hardware access object.
        /// </summary>
        /// <param name="hardware">System hardware access object.</param>
        public ControllersContext(ATFSHardware hardware)
        {
            _lockObject = new object();
            _hardware = hardware;
            _currentController = new InitializationController(this);
            _currentController.Start();
        }

        /// <summary>
        /// Initializes ContextMediumsList property.
        /// </summary>
        /// <param name="mediumsList">MediumsList object that will be accessed by ContextMediumsList property.</param>
        /// <exception cref="InvalidOperationException">Thrown when method is called again after successful initialization.</exception>
        /// <exception cref=" ArgumentNullException">Thrown when called with null parameter.</exception>
        public void InitializeMediumsList(MediumsList mediumsList)
        {

            if (mediumsList == null)
                throw new ArgumentNullException("mediumsList");
            lock (_lockObject)
            {
                if (_mediumsList != null)
                    throw new InvalidOperationException();
                _mediumsList = mediumsList;
            }
        }

        /// <summary>
        /// Initializes ContextOperationProgressTrack property.
        /// </summary>
        /// <param name="operationProgressTracker">OperationProgressTracker object that will be accessed by ContextOperationProgressTrack property.</param>
        /// <exception cref="InvalidOperationException">Thrown when method is called again after successful initialization.</exception>
        /// <exception cref=" ArgumentNullException">Thrown when called with null parameter.</exception>
        public void InitializeOperationProgressTracker(OperationProgressTracker operationProgressTracker)
        {
            if (operationProgressTracker == null)
                throw new ArgumentNullException("operationProgressTracker");
            lock (_lockObject)
            {
                if (_operationProgressTracker != null)
                    throw new InvalidOperationException();
                _operationProgressTracker = operationProgressTracker;
            }
        }

        /// <summary>
        /// Initializes ContextPouringProfilesList property.
        /// </summary>
        /// <param name="pouringProfilesList">PouringProfilesList object that will be accessed by ContextPouringProfilesList property.</param>
        /// <exception cref="InvalidOperationException">Thrown when method is called again after successful initialization.</exception>
        /// <exception cref=" ArgumentNullException">Thrown when called with null parameter.</exception>
        public void InitializePouringProfilesList(PouringProfilesList pouringProfilesList)
        {
            if (pouringProfilesList == null)
                throw new ArgumentNullException("pouringProfilesList");
            lock (_lockObject)
            {
                if (_pouringProfilesList != null)
                    throw new InvalidOperationException();
                _pouringProfilesList = pouringProfilesList;
            }
        }

        /// <summary>
        /// Initializes ContextProductsList property.
        /// </summary>
        /// <param name="productsList">ProductsList object that will be accessed by ContextProductsList property.</param>
        public void InitializeProductsList(ProductsList productsList)
        {
            if (productsList == null)
                throw new ArgumentNullException("productsList");
            lock (_lockObject)
            {
                if (_productsList != null)
                    throw new InvalidOperationException();
                _productsList = productsList;
            }
        }

        /// <summary>
        /// Passes remote request to get system status data to current controller.
        /// </summary>
        /// <returns>System status data.</returns>
        /// <exception cref="SystemInitializingException">Thrown if system is still initializing.</exception>
        public SystemStatusData RemoteCommandGetSystemStatusData()
        {
            lock (_lockObject)
                return _currentController.RemoteCommandGetSystemStatusData();
        }

        /// <summary>
        /// Passes remote command to set system clock to current controller.
        /// </summary>
        /// <param name="newClockSetting">New system clock setting.</param>
        /// <exception cref="SystemInitializingException">Thrown if system is still initializing.</exception>
        /// <exception cref="LocalInterfaceActiveException">Thrown if local interface is active.</exception>
        /// <exception cref="SystemBusyException">Thrown if system is executing pouring operation.</exception>
        public void RemoteCommandSetSystemClock(DateTime newClockSetting)
        {
            lock (_lockObject)
                _currentController.RemoteCommandSetSystemClock(newClockSetting);
        }

        /// <summary>
        /// Passes remote command to to begin pouring operation to current controller.
        /// </summary>
        /// <param name="waterToPourKg">Mass of water in kilograms to be poured.</param>
        /// <param name="mediumToPourKg">Mass of medium in kilograms to be poured.</param>
        /// <param name="timeToMixSeconds">Time to mix in seconds.</param>
        /// <param name="mediumId">ID of medium to be poured.</param>
        /// <param name="productId">ID of product to be produced.</param>
        /// <param name="operationOrderInSeries">Operation position in operations series.</param>
        /// <exception cref="SystemInitializingException">Thrown if system is still initializing.</exception>
        /// <exception cref="LocalInterfaceActiveException">Thrown if local interface is active.</exception>
        /// <exception cref="SystemBusyException">Thrown if system is executing pouring operation.</exception>
        public void RemoteCommandPour(int waterToPourKg, int mediumToPourKg, int timeToMixSeconds,
            int mediumId, int productId, OperationOrderInSeries operationOrderInSeries)
        {
            lock (_lockObject)
                _currentController.RemoteCommandPour(waterToPourKg,mediumToPourKg,
                    timeToMixSeconds,mediumId,productId,operationOrderInSeries);
        }

        /// <summary>
        /// Passes remote command to cancel pouring operation to current controller. If there is no operation to cancel, call is ignored.
        /// </summary>
        public void RemoteCommandCancelPouringOperation()
        {
            lock (_lockObject)
                _currentController.RemoteCommandCancelPouringOperation();
        }

        /// <summary>
        /// Passes remote command to resume unfinished operation to current controller.
        /// </summary>
        public void RemoteCommandResumeUnfinishedOperation()
        {
            lock (_lockObject)
                _currentController.RemoteCommandResumeUnfinishedOperation();
        }

        /// <summary>
        /// Changes system controller.
        /// </summary>
        /// <param name="newController">New system controller.</param>
        public void SetController(Controller newController)
        {
            lock (_lockObject)
            {
                _currentController.Stop();
                _currentController = newController;
                _currentController.Start();
            }
        }
    }
}
