using System;
using Microsoft.SPOT;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// The exception that is thrown when remote command to resume unfinished operation is received and no unfinished operation exists.
    /// </summary>
    public class NoUnfinishedOperationException : SystemException
    {
    }
}
