using System;
using Microsoft.SPOT;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// The exception that is thrown when remote command is received which requires local interface to be idle and it isn't.
    /// </summary>
    public class LocalInterfaceActiveException : SystemException
    {
    }
}
