using System;
using Microsoft.SPOT;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// The exception that is thrown when remote command is received which requires system to be idle and the the system is busy.
    /// </summary>
    public class SystemBusyException : SystemException
    {
    }
}
