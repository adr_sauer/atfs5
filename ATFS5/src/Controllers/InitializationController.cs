using System;
using Microsoft.SPOT;
using ATFS5.src.MediumsListLib;
using ATFS5.src.OperationProgressTrackerLib;
using ATFS5.src.PouringProfilesListLib;
using ATFS5.src.UtilitiesLib;
using ATFS5.src.AtfsSettingsLib;
using NetMf.CommonExtensions;
using ATFS5.src.ProductsListLib;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Controls the system during initialization. Initializes services offered by controllers context such as mediums list,
    /// products list, operation progress tracker and pouring profiles list.
    /// </summary>
    public class InitializationController : Controller
    {

        private const string _initializationDisplayMessageFormat = "INICJALIZACJA\r\nKROK {0} z {1}\r\n \r\n ";
        private int _initializationStep = 1;
        private const int _initializationSteps = 4;

        /// <summary>
        /// Initializes a new instance of the InitializationController class using the specified controller context.
        /// </summary>
        /// <param name="context">Controller context.</param>
        public InitializationController(ControllersContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Throws SystemInitializingException because the system cannot respond to remote commands before initialization.
        /// </summary>
        /// <returns>Throws SystemInitializingException.</returns>
        public override SystemStatusData RemoteCommandGetSystemStatusData()
        {
            throw new SystemInitializingException();
        }

        /// <summary>
        /// Throws SystemInitializingException because the system cannot respond to remote commands before initialization.
        /// </summary>
        /// <param name="newClockSetting">Ignored.</param>
        /// <exception cref="LocalInterfaceActiveException">Thrown always.</exception>
        public override void RemoteCommandSetSystemClock(DateTime newClockSetting)
        {
            throw new SystemInitializingException();
        }

        /// <summary>
        /// Throws SystemInitializingException because the system cannot respond to remote commands before initialization.
        /// </summary>
        /// <param name="waterToPourKg">Ignored.</param>
        /// <param name="mediumToPourKg">Ignored.</param>
        /// <param name="timeToMixSeconds">Ignored.</param>
        /// <param name="mediumId">Ignored.</param>
        /// <param name="productId">Ignored.</param>
        /// <param name="operationOrderInSeries">Ignored.</param>
        /// <exception cref="LocalInterfaceActiveException">Thrown always.</exception>
        public override void RemoteCommandPour(int waterToPourKg, int mediumToPourKg, int timeToMixSeconds,
            int mediumId, int productId, OperationOrderInSeries operationOrderInSeries)
        {
            throw new SystemInitializingException();
        }

        /// <summary>
        /// Throws SystemInitializingException because the system cannot respond to remote commands before initialization.
        /// </summary>
        public override void RemoteCommandResumeUnfinishedOperation()
        {
            throw new SystemInitializingException();
        }

        /// <summary>
        /// Starts controller.
        /// </summary>
        public override void Start()
        {
            Context.Hardware.Display.Write(StringUtility.Format(_initializationDisplayMessageFormat,_initializationStep,_initializationSteps));
            MediumsList mediumsList= new MediumsList(
                Resources.GetString(Resources.StringResources.MediumsListFilePathname),
                Resources.GetString(Resources.StringResources.MediumsListWebServiceEndpointAddress),
                (Utilities.ProgramSettings as AtfsSettings).MediumsListServiceCheckIntervalSeconds);
            Context.InitializeMediumsList(mediumsList);
            _initializationStep++;
                        
            Context.Hardware.Display.Write(StringUtility.Format(_initializationDisplayMessageFormat, _initializationStep, _initializationSteps));
            ProductsList productsList = new ProductsList(
                Resources.GetString(Resources.StringResources.ProductsListFilePathname),
                Resources.GetString(Resources.StringResources.ProductsListWebServiceEndpointAddress),
                (Utilities.ProgramSettings as AtfsSettings).ProductsListServiceCheckIntervalSeconds);
            Context.InitializeProductsList(productsList);
            _initializationStep++;

            Context.Hardware.Display.Write(StringUtility.Format(_initializationDisplayMessageFormat, _initializationStep, _initializationSteps));
            OperationProgressTracker tracker = new OperationProgressTracker(Resources.GetString(Resources.StringResources.OperationProgressTrackFilePathname));
            Context.InitializeOperationProgressTracker(tracker);
            _initializationStep++;

            Context.Hardware.Display.Write(StringUtility.Format(_initializationDisplayMessageFormat, _initializationStep, _initializationSteps));
            PouringProfilesList profilesList = new PouringProfilesList(Resources.GetString(Resources.StringResources.PouringProfilesListFilePathname));
            Context.InitializePouringProfilesList(profilesList);

            //Set main menu controller
            Context.SetController(new MainMenuController(Context));
        }

        /// <summary>
        /// Stops controller
        /// </summary>
        public override void Stop()
        {
            
        }
    }
}
