using System;
using Microsoft.SPOT;
using ATFS5.src.Screens20x4;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents a base class for system controlers. Analogue to abstractState in State design pattern.
    /// </summary>
    public abstract class Controller
    {
        private EventHandler _rightHandler;
        private EventHandler _leftHandler;
        private EventHandler _upHandler;
        private EventHandler _downHandler;
        private ControllersContext _context;

        /// <summary>
        /// Gets controller context.
        /// </summary>
        protected ControllersContext Context
        {
            get
            {
                return _context;
            }
        }

        /// <summary>
        /// Initializes a new instance of the Controller class using the specified controller context.
        /// </summary>
        /// <param name="context">Controller context.</param>
        public Controller(ControllersContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Starts controller.
        /// </summary>
        public abstract void Start();

        /// <summary>
        /// Stops controller
        /// </summary>
        public abstract void Stop();
        
        /// <summary>
        /// Gets system status data requested by remote client.
        /// </summary>
        /// <returns>System status data.</returns>
        public abstract SystemStatusData RemoteCommandGetSystemStatusData();

        /// <summary>
        /// Executes remote command to set system clock.
        /// </summary>
        /// <param name="newClockSetting">New system clock setting.</param>
        public abstract void RemoteCommandSetSystemClock(DateTime newClockSetting);

        /// <summary>
        /// Executes remote command to begin pouring operation.
        /// </summary>
        /// <param name="waterToPourKg">Mass of water in kilograms to be poured.</param>
        /// <param name="mediumToPourKg">Mass of medium in kilograms to be poured.</param>
        /// <param name="timeToMixSeconds">Time to mix in seconds.</param>
        /// <param name="mediumId">ID of medium to be poured.</param>
        /// <param name="productId">ID of product to be produced.</param>
        /// <param name="operationOrderInSeries">Operation position in operations series.</param>
        public abstract void RemoteCommandPour(int waterToPourKg, int mediumToPourKg,int timeToMixSeconds,
            int mediumId, int productId, OperationOrderInSeries operationOrderInSeries);


        /// <summary>
        /// Executes remote command to cancel pouring operation.
        /// </summary>
        public virtual void RemoteCommandCancelPouringOperation()
        {
            //Ignore command
        }

        /// <summary>
        /// Executes remote command to resume unfinished operation.
        /// </summary>
        public abstract void RemoteCommandResumeUnfinishedOperation();
        

        /// <summary>
        /// Registers up, down, left and right keyboard keys events to appropriate handlers exposed by Screen20x4 object.
        /// </summary>
        /// <param name="screen">Screen20x4 object.</param>
        protected void RegisterKeyboardEventHandlersToScreen(Screen20x4 screen)
        {
            _rightHandler = (o, e) =>
            {
                screen.RightPressedReaction();
            };

            _leftHandler = (o, e) =>
            {
                screen.LeftPressedReaction();
            };

            _upHandler = (o, e) =>
            {
                screen.UpPressedReaction();
            };

            _downHandler = (o, e) =>
            {
                screen.DownPressedReaction();
            };

            Context.Hardware.Keyboard.RightPressed += _rightHandler;
            Context.Hardware.Keyboard.LeftPressed += _leftHandler;
            Context.Hardware.Keyboard.UpPressed += _upHandler;
            Context.Hardware.Keyboard.DownPressed += _downHandler;
        }

        /// <summary>
        /// Unregisters up, down, left and right keyboard keys events
        /// </summary>
        protected void UnRegisterKeyEventHandlers()
        {
            Context.Hardware.Keyboard.RightPressed -= _rightHandler;
            Context.Hardware.Keyboard.LeftPressed -= _leftHandler;
            Context.Hardware.Keyboard.UpPressed -= _upHandler;
            Context.Hardware.Keyboard.DownPressed -= _downHandler;
        }
    }
}
