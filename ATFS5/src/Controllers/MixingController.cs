using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.Threading;
using ATFS5.src.OperationProgressTrackerLib;
using ATFS5.src.UtilitiesLib;
using ATFS5.src.AtfsSettingsLib;
using NetMf.CommonExtensions;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents system controller which controls the system during mixing. Analogue to State in State design pattern.
    /// </summary>
    public class MixingController : SystemBusyController
    {
        private const int _mixingCheckPeriodMiliseconds = 1000;

        private int _timeToMixInSeconds = 0;
        private int _timeMixedInSeconds = 0;
        private int _timeMixedPreviouslyInUnfinishedOperation = 0;
        private int _mediumId;
        private int _productId;
        private OperationOrderInSeries _operationOrderInSeries;
        private uint _operationId;
        private uint _seriesId;

        private bool _mixingCheckTimerStopFlag = false;
        private Timer _mixingCheckTimer;
        private object _lockObject;

        private int _waterMassPoured;
        private int _mediumMassPoured;
        private bool _mixing=false;
        private TimeSpan _mixingStartTime;

        /// <summary>
        /// Initializes a new instance of the MixingController class using the specified controller context and pouring operation data.
        /// </summary>
        /// <param name="context">Controller context</param>
        /// <param name="timeToMixSeconds">Time to mix in seconds.</param>
        /// <param name="mediumId">ID of medium to be poured.</param>
        /// <param name="productId">ID of product to be produced.</param>
        /// <param name="operationOrderInSeries">Operation position in operations series.</param>
        /// <param name="waterPoured">Mass of water in kilograms that was poured.</param>
        /// <param name="mediumPoured">Mass of medium in kilograms that was poured.</param>
        /// <param name="operationId">Operation ID.</param>
        /// <param name="seriesId">Series ID.</param>
        /// <param name="timeMixedSeconds">Mixing time already elapsed in seconds. Default is 0.</param>
        public MixingController(ControllersContext context, int timeToMixSeconds,
           int mediumId, int productId, OperationOrderInSeries operationOrderInSeries, int waterPoured, int mediumPoured,
           uint operationId, uint seriesId, int timeMixedSeconds = 0)
            : base(context)
        {
            _lockObject = new object();
            _timeToMixInSeconds= timeToMixSeconds;
            _mediumId = mediumId;
            _productId = productId;
            _operationOrderInSeries = operationOrderInSeries;
            _waterMassPoured = waterPoured;
            _mediumMassPoured = mediumPoured;
            _operationId = operationId;
            _seriesId = seriesId;
            _timeMixedPreviouslyInUnfinishedOperation = timeMixedSeconds;
        }

        /// <summary>
        /// Starts controller and mixing.
        /// </summary>
        public override void Start()
        {
            lock (_lockObject)
            {
                _mixing = ((_timeToMixInSeconds > 0) && (_timeMixedInSeconds < _timeToMixInSeconds));
                if (_mixing)
                {
                    Context.Hardware.Display.Write("MIESZANIE\n\n\nCANCEL PRZERYWA");
                    Context.Hardware.ATFSOutputsController.TurnMixerOn();
                    _mixingStartTime = Utility.GetMachineTime();
                    _mixingCheckTimer = new Timer(OnCheckMixingTimer, null, 0, _mixingCheckPeriodMiliseconds);
                    Context.Hardware.Keyboard.ShiftPressed += CancelMixing;
                }
                else
                {
                    FinishOperation();
                }
            }
        }

        /// <summary>
        /// Stops controller
        /// </summary>
        public override void Stop()
        {
            lock (_lockObject)
            {
                Context.Hardware.Keyboard.ShiftPressed -= CancelMixing;
                Context.Hardware.Keyboard.EnterPressed -= ReturnToMainMenuAfterConfirmingSuccess;
                Context.Hardware.Keyboard.ShiftPressed -= ReturnToMainMenuAfterConfirmingSuccess;
                Context.Hardware.Keyboard.EnterPressed -= ReturnToMainMenuAfterCanceling;
                Context.Hardware.Keyboard.ShiftPressed -= ReturnToMainMenuAfterCanceling;
                if (_mixing)
                {
                    _mixingCheckTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    _mixingCheckTimerStopFlag = false;
                    Context.Hardware.ATFSOutputsController.TurnMixerOff();
                    _mixing = false;                    
                }
            }
        }

        /// <summary>
        /// Gets system status data requested by remote client.
        /// </summary>
        /// <returns>System status data.</returns>
        public override SystemStatusData RemoteCommandGetSystemStatusData()
        {
            SystemStatusData statusData = new SystemStatusData();
            statusData.StatusDate = DateTime.Now;
            statusData.MachineTime = Utility.GetMachineTime();
            statusData.State = SystemStatusData.SystemState.Mixing;
            statusData.WaterFlowKgPerMin = 0;
            statusData.MediumFlowKgPerMin = 0;
            statusData.MediumId = _mediumId;
            statusData.WaterToPourKg = _waterMassPoured;
            statusData.MediumToPourKg = _mediumMassPoured;
            statusData.WaterPouredKg = _waterMassPoured;
            statusData.MediumPouredKg = _mediumMassPoured;
            statusData.TimeToMixSeconds = _timeToMixInSeconds;
            statusData.TimeMixedSeconds = _timeMixedInSeconds;
            statusData.UnfinishedOperationExists = false;
            statusData.UnfinishedOperationData = null;
            //HACK Safety button always false.
            statusData.SafetyButtonActive = false;
            statusData.CurrentWaterFlowError = FlowError.NoError;
            statusData.CurrentMediumFlowError = FlowError.NoError;
            statusData.LastWaterFlowPauseCauseError = FlowError.NoError;
            statusData.LastMediumFlowPauseCauseError = FlowError.NoError;
            //HACK Overflow sensor always false.
            statusData.EmptyingPumpActive = false;
            statusData.OperationId = _operationId;
            statusData.SeriesId = _seriesId;
            statusData.ProductId = _productId;
            statusData.OperationOrderInSeries = _operationOrderInSeries;

            return statusData;
        }
        
        /// <summary>
        /// Executes remote command to cancel pouring operation.
        /// </summary>
        public override void RemoteCommandCancelPouringOperation()
        {            
            CancelMixing(null, EventArgs.Empty);
        }

        

        private void OnCheckMixingTimer(object state)
        {
            lock (_lockObject)
            {
                try
                {
                    if (_mixingCheckTimerStopFlag)
                        return;

                    if (_mixing)
                    {
                        TimeSpan timeMixed = Utility.GetMachineTime() - _mixingStartTime;
                        _timeMixedInSeconds = _timeMixedPreviouslyInUnfinishedOperation +
                            timeMixed.Minutes * 60 + timeMixed.Seconds;

                        if (_timeMixedInSeconds >= _timeToMixInSeconds)
                        {
                            _mixing = false;
                            Context.Hardware.ATFSOutputsController.TurnMixerOff();
                        }
                    }

                    //Save operation progress to file
                    Context.ContextOperationProgressTrack.SaveProgress(new OperationProgressTrack(_waterMassPoured, _mediumMassPoured, _waterMassPoured,
                        _mediumMassPoured, _timeToMixInSeconds, _timeMixedInSeconds, _mediumId, _seriesId, _operationId, _operationOrderInSeries, _productId));


                    //Display control
                    if (Utility.GetMachineTime().Seconds % 3 == 0)
                    {
                        
                        Context.Hardware.Display.WriteToLine(StringUtility.Format("{0} SEK. Z {1}",_timeMixedInSeconds,_timeToMixInSeconds), 1);
                    }

                    //Stop timer if mixing finished
                    if(!_mixing)
                    {
                        _mixingCheckTimerStopFlag = true;
                        _mixingCheckTimer.Change(Timeout.Infinite, Timeout.Infinite);
                        FinishOperation();
                    }
                }
                catch (Exception ex)
                {                    
                    Utilities.HandleUnforeseenException(ex);
                }
            }
        }

        private void FinishOperation()
        {
            Context.Hardware.Keyboard.ShiftPressed -= CancelMixing;
            Context.ContextOperationProgressTrack.DeleteTrack();

            Context.Hardware.Display.Write("ZAKONCZONO NALEWANIE\nNACISNIJ ENTER LUB\nCANCEL\n");
            if ((Utilities.ProgramSettings as AtfsSettings).SirenEnabledSetting)
                Context.Hardware.ATFSOutputsController.TurnSirenOn();
            Context.Hardware.Keyboard.EnterPressed += ReturnToMainMenuAfterConfirmingSuccess;
            Context.Hardware.Keyboard.ShiftPressed += ReturnToMainMenuAfterConfirmingSuccess;
        }

        private void CancelMixing(object o, EventArgs e)
        {
            Context.Hardware.Keyboard.ShiftPressed -= CancelMixing;
            lock (_lockObject)
            {
                if (_mixing)
                {
                    _mixingCheckTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    _mixingCheckTimerStopFlag = false;
                    Context.Hardware.ATFSOutputsController.TurnMixerOff();
                    _mixing = false;
                    Context.Hardware.Display.Write("PRZERWANO MIESZANIE\nNACISNIJ ENTER\nLUB CANCEL\n");
                    Context.Hardware.Keyboard.EnterPressed += ReturnToMainMenuAfterCanceling;
                    Context.Hardware.Keyboard.ShiftPressed += ReturnToMainMenuAfterCanceling;
                }
                else
                {
                    //Ignore
                }
            }
        }

        private void ReturnToMainMenuAfterCanceling(object o, EventArgs e)
        {
            Context.Hardware.Keyboard.EnterPressed -= ReturnToMainMenuAfterCanceling;
            Context.Hardware.Keyboard.ShiftPressed -= ReturnToMainMenuAfterCanceling;
            Context.SetController(new MainMenuController(Context));
        }

        private void ReturnToMainMenuAfterConfirmingSuccess(object o, EventArgs e)
        {
            Context.Hardware.Keyboard.EnterPressed -= ReturnToMainMenuAfterConfirmingSuccess;
            Context.Hardware.Keyboard.ShiftPressed -= ReturnToMainMenuAfterConfirmingSuccess;
            Context.Hardware.ATFSOutputsController.TurnSirenOff();
            Context.SetController(new MainMenuController(Context));

        }
    }
}
