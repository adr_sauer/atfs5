using System;
using Microsoft.SPOT;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// The exception that is thrown when remote command is received when the system is still initializing.
    /// </summary>
    public class SystemInitializingException : SystemException
    {
    }
}
