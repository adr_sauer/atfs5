using System;
using Microsoft.SPOT;
using ATFS5.src.Screens20x4;
using ATFS5.src.PouringProfilesListLib;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents system controller which controls the system when user is adding new pouring profile. Analogue to State in State design pattern.
    /// </summary>
    public class AddProfileController : LocalInterfaceActiveController
    {
        private ValuePromptWithPositionLineScreen _screen;

        private PouringProfile _profile;

        /// <summary>
        /// Initializes a new instance of the AddProfileController class using the specified controller context.
        /// </summary>
        /// <param name="context">Controller context.</param>
        public AddProfileController(ControllersContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Starts controller.
        /// </summary>
        public override void Start()
        {
            _profile = new PouringProfile();
            //Minus 1 to ensure that there is place for at least 1 kg on medium.
            _screen = new UnsignedIntegerValuePromptScreen(0, PouringProfile.MaxTotalMassKg - 1, 0, "PODAJ MASE WODY", " KG");
            RegisterKeyboardEventHandlersToScreen(_screen);
            RegisterScreenEventsHandlers();

            Context.Hardware.Display.Write(_screen.FullScreenString);
            Context.Hardware.Keyboard.EnterPressed += ConfirmWaterMass;
            Context.Hardware.Keyboard.ShiftPressed += Cancel;
        }

        /// <summary>
        /// Stops controller
        /// </summary>
        public override void Stop()
        {
            UnRegisterKeyEventHandlers();
            Context.Hardware.Keyboard.ShiftPressed -= Cancel;
            Context.Hardware.Keyboard.EnterPressed -= ConfirmWaterMass;
            Context.Hardware.Keyboard.EnterPressed -= ConfirmMediumMass;
            Context.Hardware.Keyboard.EnterPressed -= ConfirmMixingTime;
            Context.Hardware.Keyboard.EnterPressed -= ConfirmProfileName;
        }

        private void ConfirmWaterMass(object o, EventArgs e)
        {
            UnRegisterKeyEventHandlers();
            Context.Hardware.Keyboard.EnterPressed -= ConfirmWaterMass;
            _profile.WaterMass = (_screen as UnsignedIntegerValuePromptScreen).Value;

            _screen = new UnsignedIntegerValuePromptScreen(0, PouringProfile.MaxTotalMassKg - _profile.WaterMass, 0, "PODAJ MASE MEDIUM", " KG");
            RegisterKeyboardEventHandlersToScreen(_screen);
            RegisterScreenEventsHandlers();

            Context.Hardware.Display.Write(_screen.FullScreenString);
            Context.Hardware.Keyboard.EnterPressed += ConfirmMediumMass;

        }

        private void ConfirmMediumMass(object o, EventArgs e)
        {
            UnRegisterKeyEventHandlers();
            Context.Hardware.Keyboard.EnterPressed -= ConfirmMediumMass;
            _profile.MediumMass = (_screen as UnsignedIntegerValuePromptScreen).Value;
            _screen = new UnsignedIntegerValuePromptScreen(0, PouringProfile.MaxMixingTimeInMinutes, 0, "PODAJ CZAS MIESZANIA", " MIN");
            RegisterKeyboardEventHandlersToScreen(_screen);
            RegisterScreenEventsHandlers();

            Context.Hardware.Display.Write(_screen.FullScreenString);
            Context.Hardware.Keyboard.EnterPressed += ConfirmMixingTime;
        }

        private void ConfirmMixingTime(object o, EventArgs e)
        {
            UnRegisterKeyEventHandlers();
            Context.Hardware.Keyboard.EnterPressed -= ConfirmMixingTime;
            _profile.MixingTimeInMinutes = (_screen as UnsignedIntegerValuePromptScreen).Value;
            _screen = new StringValuePromptScreen(PouringProfile.MaxNameLength, "NAZWA PROFILU");
            RegisterKeyboardEventHandlersToScreen(_screen);
            RegisterScreenEventsHandlers();
            Context.Hardware.Display.Write(_screen.FullScreenString);
            Context.Hardware.Keyboard.EnterPressed += ConfirmProfileName;
        }

        private void ConfirmProfileName(object o, EventArgs e)
        {
            UnRegisterKeyEventHandlers();
            Context.Hardware.Keyboard.EnterPressed -= ConfirmProfileName;
            _profile.Name = (_screen as StringValuePromptScreen).Value;
            try
            {
                Context.ContextPouringProfilesList.Add(_profile);
            }
            catch (Exception ex)
            {
                Utilities.ProgramLogger.LogError("Exception occured when adding profile");
                Utilities.ProgramLogger.LogException(ex);
            }
            Context.SetController(new MainMenuController(Context));
        }

        private void Cancel(object o, EventArgs e)
        {
            Context.SetController(new MainMenuController(Context));
        }

        private void RegisterScreenEventsHandlers()
        {
            _screen.ValueChanged += (o1, e1) =>
            {
                Context.Hardware.Display.WriteToLine(_screen.ValueLineString, _screen.ValueLineNumber);
            };

            _screen.CursorPositionChanged += (o1, e1) =>
            {
                Context.Hardware.Display.WriteToLine(_screen.PositionLineString, _screen.PositionLineNumber);
            };
        }
    }
}
