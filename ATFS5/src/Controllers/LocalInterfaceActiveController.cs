using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents a base class for system controlers which control the system when the user is accesing system through local interface. 
    /// </summary>
    public abstract class LocalInterfaceActiveController : Controller
    {
        /// <summary>
        /// Initializes a new instance of the LocalInterfaceActiveController class using the specified controller context.
        /// </summary>
        /// <param name="context">Controller context.</param>
        public LocalInterfaceActiveController(ControllersContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Gets system status data requested by remote client.
        /// </summary>
        /// <returns>System status data.</returns>
        public override SystemStatusData RemoteCommandGetSystemStatusData()
        {
            SystemStatusData statusData = new SystemStatusData();
            statusData.StatusDate = DateTime.Now;
            statusData.MachineTime = Utility.GetMachineTime();
            statusData.State = SystemStatusData.SystemState.LocalInterfaceActive;
            statusData.WaterFlowKgPerMin = 0;
            statusData.MediumFlowKgPerMin = 0;
            statusData.MediumId = 0;
            statusData.WaterToPourKg = 0;
            statusData.MediumToPourKg = 0;
            statusData.WaterPouredKg = 0;
            statusData.MediumPouredKg = 0;
            statusData.TimeToMixSeconds = 0;
            statusData.TimeMixedSeconds = 0;
            statusData.UnfinishedOperationExists = Context.ContextOperationProgressTrack.TrackExists;
            statusData.UnfinishedOperationData = Context.ContextOperationProgressTrack.ProgressTrack;
            //HACK Safety button always false.
            statusData.SafetyButtonActive = false;
            statusData.CurrentWaterFlowError = FlowError.NoError;
            statusData.CurrentMediumFlowError = FlowError.NoError;
            statusData.LastWaterFlowPauseCauseError = FlowError.NoError;
            statusData.LastMediumFlowPauseCauseError = FlowError.NoError;
            //HACK Overflow sensor always false.
            statusData.EmptyingPumpActive = false;
            statusData.OperationId = 0;
            statusData.SeriesId = 0;
            statusData.ProductId = 0;
            statusData.OperationOrderInSeries = OperationOrderInSeries.Begin;

            return statusData;
        }

        /// <summary>
        /// Throws LocalInterfaceActiveException because setting system clock is not allowed when system is not idle.
        /// </summary>
        /// <param name="newClockSetting">Ignored.</param>
        /// <exception cref="LocalInterfaceActiveException">Thrown always.</exception>
        public override void RemoteCommandSetSystemClock(DateTime newClockSetting)
        {
            throw new LocalInterfaceActiveException();
        }

        /// <summary>
        /// Throws LocalInterfaceActiveException because remotely starting a pouring operation is not allowed when system is not idle.
        /// </summary>
        /// <param name="waterToPourKg">Ignored.</param>
        /// <param name="mediumToPourKg">Ignored.</param>
        /// <param name="timeToMixSeconds">Ignored.</param>
        /// <param name="mediumId">Ignored.</param>
        /// <param name="productId">Ignored.</param>
        /// <param name="operationOrderInSeries">Ignored.</param>
        /// <exception cref="LocalInterfaceActiveException">Thrown always.</exception>
        public override void RemoteCommandPour(int waterToPourKg, int mediumToPourKg,int timeToMixSeconds,
            int mediumId, int productId, OperationOrderInSeries operationOrderInSeries)
        {
            throw new LocalInterfaceActiveException();
        }

        /// <summary>
        /// Throws LocalInterfaceActiveException because remotely resuming an unfinished pouring operation is not allowed when system is not idle.
        /// </summary>
        /// <exception cref="LocalInterfaceActiveException">Thrown always.</exception>
        public override void RemoteCommandResumeUnfinishedOperation()
        {
            throw new LocalInterfaceActiveException();
        }
        
    }
}
