using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.Threading;
using ATFS5.src.OperationProgressTrackerLib;
using ATFS5.src.UtilitiesLib;
using ATFS5.src.AtfsSettingsLib;
using NetMf.CommonExtensions;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents system controller which controls the system during pouring. Analogue to State in State design pattern.
    /// </summary>
    public class PouringController : SystemBusyController
    {
        private int _mediumToFill = 0;
        private int _waterToFill = 0;
        private int _timeToMixInSeconds = 0;
        private int _mediumId;
        private int _productId;
        private OperationOrderInSeries _operationOrderInSeries;
        private uint _operationId;
        private uint _seriesId;

        private int _initialWaterMassKg;
        private int _initialMediumMassKg;
        private int _initialMixingTimeSeconds;

        const int filling_check_interval = 1000;

        private object _lockObject;
        private MassFlowOverTimeIntegrator _mediumMassFlowIntegrator;
        private MassFlowOverTimeIntegrator _waterMassFlowIntegrator;

        private bool _fillingMedium = false;
        private bool _fillingWater = false;
        private bool _fillingCheckTimerStopFlag = false;

        private int _mediumLastMassPouredValue = 0;
        private int _waterLastMassPouredValue = 0;

        double _lastMediumFlowKgPerMin;
        double _lastWaterFlowKgPerMin;

        private TimeSpan _mediumAlarmStartTimeStamp = TimeSpan.MaxValue;
        private TimeSpan _waterAlarmStartTimeStamp = TimeSpan.MaxValue;

        private FlowError _lastMediumFlowError;
        private FlowError _lastWaterFlowError;
        private FlowError _lastMediumFlowPauseError;
        private FlowError _lastWaterFlowPauseError;

        private bool _pouringPausedBecauseOfError = false;
        Timer filling_check_timer;

        /// <summary>
        /// Initializes a new instance of the PouringController class using the specified controller context and pouring operation data.
        /// </summary>
        /// <param name="context">Controller context.</param>
        /// <param name="waterToPourKg">Mass of water in kilograms to be poured.</param>
        /// <param name="mediumToPourKg">Mass of medium in kilograms to be poured.</param>
        /// <param name="timeToMixSeconds">Time to mix in seconds.</param>
        /// <param name="mediumId">ID of medium to be poured.</param>
        /// <param name="productId">ID of product to be produced.</param>
        /// <param name="operationOrderInSeries">Operation position in operations series.</param>
        /// <param name="waterPoured">Mass of water in kilograms that was already poured. Default is 0.</param>
        /// <param name="mediumPoured">Mass of medium in kilograms that was already poured. Default is 0.</param>
        /// <param name="timeMixedSeconds">Mixing time already elapsed in seconds. Default is 0.</param>
        /// <param name="operationId">Operation ID. Default is new operation ID.</param>
        public PouringController(ControllersContext context, int waterToPourKg, int mediumToPourKg, int timeToMixSeconds,
            int mediumId, int productId, OperationOrderInSeries operationOrderInSeries, int waterPoured = 0,
            int mediumPoured = 0, int timeMixedSeconds = 0, uint operationId = 0)
            : base(context)
        {
            _lockObject = new object();
            _waterToFill = waterToPourKg;
            _mediumToFill = mediumToPourKg;
            _timeToMixInSeconds = timeToMixSeconds;
            _mediumId = mediumId;
            _productId = productId;
            _operationOrderInSeries = operationOrderInSeries;
            _waterMassFlowIntegrator = new MassFlowOverTimeIntegrator();
            _mediumMassFlowIntegrator = new MassFlowOverTimeIntegrator();
            _initialWaterMassKg = waterPoured;
            _initialMediumMassKg = mediumPoured;
            _initialMixingTimeSeconds = timeMixedSeconds;
            if (operationId == 0)
                _operationId = NewOperationID();
            else
                _operationId = operationId;

            if (_operationOrderInSeries == OperationOrderInSeries.Begin)
            {
                _seriesId = NewOperationID();
                (Utilities.ProgramSettings as AtfsSettings).LastSeriesId = _seriesId;
                (Utilities.ProgramSettings as AtfsSettings).LastProductId = _productId;
            }
            else
                _seriesId = (Utilities.ProgramSettings as AtfsSettings).LastSeriesId;

        }

        /// <summary>
        /// Initializes a new instance of the PouringController class using the specified controller context and operation progress track.
        /// </summary>
        /// <param name="context">Controller context.</param>
        /// <param name="operationProgressTrack">Operation progress track.</param>
        public PouringController(ControllersContext context, OperationProgressTrack operationProgressTrack)
            : base(context)
        {
            _lockObject = new object();
            _waterToFill = operationProgressTrack.WaterMassToPourKg;
            _mediumToFill = operationProgressTrack.MediumMassToPourKg;
            _timeToMixInSeconds = operationProgressTrack.TimeToMixInSeconds;
            _mediumId = operationProgressTrack.MediumId;
            _productId = operationProgressTrack.ProductId;
            _operationOrderInSeries = operationProgressTrack.OperationOrderInSeriesTrack;
            _waterMassFlowIntegrator = new MassFlowOverTimeIntegrator();
            _mediumMassFlowIntegrator = new MassFlowOverTimeIntegrator();
            _initialWaterMassKg = operationProgressTrack.WaterMassPouredKg;
            _initialMediumMassKg = operationProgressTrack.MediumMassPouredKg;
            _initialMixingTimeSeconds = operationProgressTrack.TimeMixedInSeconds;
            _operationId = operationProgressTrack.OperationId;
            _seriesId = operationProgressTrack.SeriesId;
        }

        /// <summary>
        /// Gets system status data requested by remote client.
        /// </summary>
        /// <returns>System status data.</returns>
        public override SystemStatusData RemoteCommandGetSystemStatusData()
        {
            lock (_lockObject)
            {
                SystemStatusData statusData = new SystemStatusData();
                statusData.StatusDate = DateTime.Now;
                statusData.MachineTime = Utility.GetMachineTime();

                if (_fillingWater && _fillingMedium)
                    statusData.State = SystemStatusData.SystemState.FillingBoth;
                if (_fillingWater)
                    statusData.State = SystemStatusData.SystemState.FillingWater;
                if (_fillingMedium)
                    statusData.State = SystemStatusData.SystemState.FillingMedium;
                else
                    statusData.State = SystemStatusData.SystemState.Waiting;

                statusData.WaterFlowKgPerMin = _lastWaterFlowKgPerMin;
                statusData.MediumFlowKgPerMin = _lastMediumFlowKgPerMin;
                statusData.MediumId = _mediumId;
                statusData.WaterToPourKg = _waterToFill;
                statusData.MediumToPourKg = _mediumToFill;
                statusData.WaterPouredKg = _waterLastMassPouredValue;
                statusData.MediumPouredKg = _mediumLastMassPouredValue;
                statusData.TimeToMixSeconds = _timeToMixInSeconds;
                statusData.TimeMixedSeconds = _initialMixingTimeSeconds;
                statusData.UnfinishedOperationExists = false;
                statusData.UnfinishedOperationData = null;
                //HACK Safety button always false.
                statusData.SafetyButtonActive = false;
                statusData.CurrentWaterFlowError = _lastWaterFlowError;
                statusData.CurrentMediumFlowError = _lastMediumFlowError;
                statusData.LastWaterFlowPauseCauseError = _lastWaterFlowPauseError;
                statusData.LastMediumFlowPauseCauseError = _lastMediumFlowPauseError;
                //HACK Overflow sensor always false.
                statusData.EmptyingPumpActive = false;
                statusData.OperationId = _operationId;
                statusData.SeriesId = _seriesId;
                statusData.ProductId = _productId;
                statusData.OperationOrderInSeries = _operationOrderInSeries;

                return statusData;
            }
        }

        /// <summary>
        /// Executes remote command to cancel pouring operation.
        /// </summary>
        public override void RemoteCommandCancelPouringOperation()
        {
            CancelPouring(null, EventArgs.Empty);
        }

        /// <summary>
        /// Starts controller and pouring operation.
        /// </summary>
        public override void Start()
        {
            lock (_lockObject)
            {
                _fillingMedium = ((_mediumToFill > 0) && (_initialMediumMassKg < _mediumToFill));
                _fillingWater = ((_waterToFill > 0) && (_initialWaterMassKg < _waterToFill));


                if (_fillingWater)
                {
                    Context.Hardware.ATFSOutputsController.TurnWaterFlowOn();
                    _waterMassFlowIntegrator.Start(_waterToFill, _initialWaterMassKg);
                }

                if (_fillingMedium)
                {
                    Context.Hardware.ATFSOutputsController.TurnMediumFlowOn();
                    _mediumMassFlowIntegrator.Start(_mediumToFill, _initialMediumMassKg);
                }

                if (_fillingWater || _fillingMedium)
                {
                    Context.Hardware.Display.Write("NALANO\n\n\nCANCEL PRZERYWA");
                    filling_check_timer = new Timer(OnCheckPouringTimer, null, 0, filling_check_interval);
                    Context.Hardware.Keyboard.ShiftPressed += CancelPouring;
                }
                else
                {
                    Context.SetController(new MixingController(Context, _timeToMixInSeconds, _mediumId, _productId,
                        _operationOrderInSeries, _initialWaterMassKg, _initialMediumMassKg, _operationId, _seriesId, _initialMixingTimeSeconds));
                }



            }
        }

        /// <summary>
        /// Stops controller
        /// </summary>
        public override void Stop()
        {
            lock (_lockObject)
            {
                Context.Hardware.Keyboard.ShiftPressed -= CancelPouring;
                Context.Hardware.Keyboard.EnterPressed -= ClearFlowError;
                Context.Hardware.Keyboard.EnterPressed -= ReturnToMainMenuAfterCanceling;
                Context.Hardware.Keyboard.ShiftPressed -= ReturnToMainMenuAfterCanceling;
                if (_fillingWater || _fillingMedium)
                {
                    filling_check_timer.Change(Timeout.Infinite, Timeout.Infinite);
                    _fillingCheckTimerStopFlag = true;
                    if (_fillingWater)
                    {
                        _fillingWater = false;
                        Context.Hardware.ATFSOutputsController.TurnWaterFlowOff();
                    }
                    if (_fillingMedium)
                    {
                        _fillingMedium = false;
                        Context.Hardware.ATFSOutputsController.TurnMediumFlowOff();
                    }
                }
            }
        }


        private void OnCheckPouringTimer(object state)
        {
            lock (_lockObject)
            {
                try
                {
                    if (_fillingCheckTimerStopFlag)
                        return;


                    bool pauseBecauseOfMediumFlowError = false;
                    if (_fillingMedium)
                    {
                        FlowmeterErrorStatus mediumFlowmeterErrorStatus;
                        Context.Hardware.MediumFlowmeter.GetFlowAndErrorStatus(out _lastMediumFlowKgPerMin, out mediumFlowmeterErrorStatus);

                        FlowError newFlowError = FlowError.NoError;
                        newFlowError = newFlowError.Union(mediumFlowmeterErrorStatus);

                        if (Context.Hardware.MediumFlowSensor.IsActive)
                            newFlowError |= FlowError.FlowSensor;

                        int currentMassPouredValue = _mediumMassFlowIntegrator.Mass;
                        if (currentMassPouredValue == _mediumLastMassPouredValue)
                            newFlowError |= FlowError.NoChangeInMassIntegral;
                        _lastMediumFlowError = newFlowError;
                        _mediumLastMassPouredValue = currentMassPouredValue;

                        if (_lastMediumFlowError != FlowError.NoError)
                        {
                            if (_mediumAlarmStartTimeStamp == TimeSpan.MaxValue)
                                _mediumAlarmStartTimeStamp = Utility.GetMachineTime();
                        }
                        else
                            _mediumAlarmStartTimeStamp = TimeSpan.MaxValue;

                        TimeSpan currentLiquidLackAlarmDelay = new TimeSpan(0, 0, (Utilities.ProgramSettings as AtfsSettings).LiquidLackAlarmDelaySecondsSetting);

                        if (Utility.GetMachineTime() - _mediumAlarmStartTimeStamp > currentLiquidLackAlarmDelay)
                        {
                            pauseBecauseOfMediumFlowError = true;
                        }
                        else
                        {
                            if (_mediumMassFlowIntegrator.AddFlowMeasurementAndCheckMassSum(_lastMediumFlowKgPerMin))
                            {
                                _fillingMedium = false;
                                Context.Hardware.ATFSOutputsController.TurnMediumFlowOff();
                            }
                        }
                    }
                    bool pauseBecauseOfWaterFlowError = false;
                    if (_fillingWater)
                    {
                        FlowmeterErrorStatus waterFlowmeterErrorStatus;
                        Context.Hardware.WaterFlowmeter.GetFlowAndErrorStatus(out _lastWaterFlowKgPerMin, out waterFlowmeterErrorStatus);

                        FlowError newFlowError = FlowError.NoError;
                        newFlowError = newFlowError.Union(waterFlowmeterErrorStatus);

                        if (Context.Hardware.WaterFlowSensor.IsActive)
                            newFlowError |= FlowError.FlowSensor;

                        int currentMassPouredValue = _waterMassFlowIntegrator.Mass;
                        if (currentMassPouredValue == _waterLastMassPouredValue)
                            newFlowError |= FlowError.NoChangeInMassIntegral;
                        _lastWaterFlowError = newFlowError;
                        _waterLastMassPouredValue = currentMassPouredValue;

                        if (_lastWaterFlowError != FlowError.NoError)
                        {
                            if (_waterAlarmStartTimeStamp == TimeSpan.MaxValue)
                                _waterAlarmStartTimeStamp = Utility.GetMachineTime();
                        }
                        else
                            _waterAlarmStartTimeStamp = TimeSpan.MaxValue;

                        TimeSpan currentLiquidLackAlarmDelay = new TimeSpan(0, 0, (Utilities.ProgramSettings as AtfsSettings).LiquidLackAlarmDelaySecondsSetting);

                        if (Utility.GetMachineTime() - _waterAlarmStartTimeStamp > currentLiquidLackAlarmDelay)
                        {
                            pauseBecauseOfWaterFlowError = true;
                        }
                        else
                        {
                            if (_waterMassFlowIntegrator.AddFlowMeasurementAndCheckMassSum(_lastWaterFlowKgPerMin))
                            {
                                _fillingWater = false;
                                Context.Hardware.ATFSOutputsController.TurnWaterFlowOff();
                            }
                        }
                    }

                    if(pauseBecauseOfMediumFlowError)
                        PausePouringBecauseOfMediumFlowError();
                    if (pauseBecauseOfWaterFlowError)
                        PausePouringBecauseOfWaterFlowError();

                    //zapisanie do pliku stanu nalewania
                    Context.ContextOperationProgressTrack.SaveProgress(new OperationProgressTrack(_waterToFill, _mediumToFill, _waterMassFlowIntegrator.Mass,
                        _mediumMassFlowIntegrator.Mass, _timeToMixInSeconds, 0, _mediumId, _seriesId, _operationId, _operationOrderInSeries, _productId));


                    //kontrola wyświetlania postępów nalewania
                    if (Utility.GetMachineTime().Seconds % 3 == 0)
                    {
                        Context.Hardware.Display.WriteToLine(StringUtility.Format("{0:D5} KG WOD P:{1:F1}", _waterMassFlowIntegrator.Mass, _lastWaterFlowKgPerMin), 1);
                        Context.Hardware.Display.WriteToLine(StringUtility.Format("{0:D5} KG MED P:{1:F1}", _mediumMassFlowIntegrator.Mass, _lastMediumFlowKgPerMin), 2);
                    }

                    //wyjscie z timera jesli zakonczono nalewanie
                    if (!(_fillingMedium || _fillingWater))
                    {
                        _fillingCheckTimerStopFlag = true;
                        filling_check_timer.Change(Timeout.Infinite, Timeout.Infinite);
                        FinishPouringAndBeginMixing();
                    }
                }
                catch (Exception ex)
                {
                    Utilities.HandleUnforeseenException(ex);
                }

            }
        }

        private void PausePouringCommon()
        {
            _pouringPausedBecauseOfError = true;
            _fillingCheckTimerStopFlag = true;
            filling_check_timer.Change(Timeout.Infinite, Timeout.Infinite);
            if (_fillingMedium)
            {
                Context.Hardware.ATFSOutputsController.TurnMediumFlowOff();
            }

            if (_fillingWater)
            {
                Context.Hardware.ATFSOutputsController.TurnWaterFlowOff();
            }
        }

        private void PausePouringBecauseOfWaterFlowError()
        {
            _lastWaterFlowPauseError = _lastWaterFlowError;
            PausePouringCommon();
            Context.Hardware.Display.Write("BRAK WODY   " + _lastWaterFlowPauseError.ToSymbolString() +
                "\nROZWIAZ PROBLEM I\nNACISNIJ ENTER LUB\nANULUJ NALEWANIE");
            Context.Hardware.Keyboard.EnterPressed += ClearFlowError;
        }

        private void PausePouringBecauseOfMediumFlowError()
        {
            _lastMediumFlowPauseError = _lastMediumFlowError;
            PausePouringCommon();
            Context.Hardware.Display.Write("BRAK MEDIUM " + _lastMediumFlowPauseError.ToSymbolString() +
                "\nROZWIAZ PROBLEM I\nNACISNIJ ENTER LUB\nANULUJ NALEWANIE");
            Context.Hardware.Keyboard.EnterPressed += ClearFlowError;
        }

        private void ClearFlowError(object o, EventArgs e)
        {
            Context.Hardware.Keyboard.EnterPressed -= ClearFlowError;
            lock (_lockObject)
            {
                if (_pouringPausedBecauseOfError)
                    RestartFlow();
                else
                {
                    //Ignore
                }
            }
        }

        void RestartFlow()
        {
            _fillingCheckTimerStopFlag = false;
            _pouringPausedBecauseOfError = false;
            _lastWaterFlowPauseError = FlowError.NoError;
            _lastMediumFlowPauseError = FlowError.NoError;
            if (_fillingMedium)
            {
                Context.Hardware.ATFSOutputsController.TurnMediumFlowOn();
                _mediumMassFlowIntegrator.Resume();
                _mediumAlarmStartTimeStamp = TimeSpan.MaxValue;
            }
            if (_fillingWater)
            {
                Context.Hardware.ATFSOutputsController.TurnWaterFlowOn();
                _waterMassFlowIntegrator.Resume();
                _waterAlarmStartTimeStamp = TimeSpan.MaxValue;
            }

            Context.Hardware.Display.Write("NALANO\n\n\nCANCEL PRZERYWA");
            filling_check_timer.Change(0, filling_check_interval);

        }

        private void FinishPouringAndBeginMixing()
        {
            Context.SetController(new MixingController(Context, _timeToMixInSeconds, _mediumId, _productId,
                        _operationOrderInSeries, _waterMassFlowIntegrator.Mass, _mediumMassFlowIntegrator.Mass,
                        _operationId, _seriesId, _initialMixingTimeSeconds));
        }


        private void CancelPouring(object o, EventArgs e)
        {
            //Called by pressing cancel button
            Context.Hardware.Keyboard.ShiftPressed -= CancelPouring;
            lock (_lockObject)
            {
                if (_fillingWater || _fillingMedium)
                {
                    filling_check_timer.Change(Timeout.Infinite, Timeout.Infinite);
                    _fillingCheckTimerStopFlag = true;
                    if (_fillingWater)
                    {
                        _fillingWater = false;
                        Context.Hardware.ATFSOutputsController.TurnWaterFlowOff();
                    }
                    if (_fillingMedium)
                    {
                        _fillingMedium = false;
                        Context.Hardware.ATFSOutputsController.TurnMediumFlowOff();
                    }
                    Context.Hardware.Display.Write(
                        StringUtility.Format("PRZERWANO NALEWANIE\n{0:D5} KG MD. NALAN\n{1:D5} KG WODY NALAN\nENTER LUB CANCEL",
                        _mediumMassFlowIntegrator.Mass, _waterMassFlowIntegrator.Mass));
                    Context.Hardware.Keyboard.EnterPressed += ReturnToMainMenuAfterCanceling;
                    Context.Hardware.Keyboard.ShiftPressed += ReturnToMainMenuAfterCanceling;
                }
                else
                {
                    //Ignore
                }
            }
        }

        private void ReturnToMainMenuAfterCanceling(object o, EventArgs e)
        {
            Context.Hardware.Keyboard.EnterPressed -= ReturnToMainMenuAfterCanceling;
            Context.Hardware.Keyboard.ShiftPressed -= ReturnToMainMenuAfterCanceling;
            Context.SetController(new MainMenuController(Context));
        }

        static uint NewOperationID()
        {
            //uint is enough to store a period of 136 * 365 days (about 136 years) in seconds or about 68 years in opidtocks
            //One opidtock=500ms
            //operation id will overflow on 19/01/2083 3:14:07
            const long offset = ((long)31536000 * 414L * 2L) + (8640000L * 2L); //calculated offset in opidtocks for 1/1/2015 00:00:00. At this date operationid was equal to 0;
            long date = DateTime.Now.Ticks; //current date in ticks from 1/1/1601
            date /= 10000000L / 2L; // calculate to opidtocks from 1/1/1601
            date -= offset;

            return (uint)date;
        }
    }
}
