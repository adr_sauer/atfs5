using System;
using Microsoft.SPOT;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents a base class for system controlers which control the system when the system is pouring or mixing. 
    /// </summary>
    public abstract class SystemBusyController : Controller
    {
        /// <summary>
        /// Initializes a new instance of the SystemBusyController class using the specified controller context.
        /// </summary>
        /// <param name="context">Controller context.</param>
        public SystemBusyController(ControllersContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Throws SystemBusyException because setting system clock is not allowed when system is not idle.
        /// </summary>
        /// <param name="newClockSetting">Ignored.</param>
        /// <exception cref="SystemBusyException">Thrown always.</exception>
        public override void RemoteCommandSetSystemClock(DateTime newClockSetting)
        {
            throw new SystemBusyException();
        }

        /// <summary>
        /// Throws SystemBusyException because remotely starting a pouring operation is not allowed when system is not idle.
        /// </summary>
        /// <param name="waterToPourKg">Ignored.</param>
        /// <param name="mediumToPourKg">Ignored.</param>
        /// <param name="timeToMixSeconds">Ignored.</param>
        /// <param name="mediumId">Ignored.</param>
        /// <param name="productId">Ignored.</param>
        /// <param name="operationOrderInSeries">Ignored.</param>
        /// <exception cref="SystemBusyException">Thrown always.</exception>
        public override void RemoteCommandPour(int waterToPourKg, int mediumToPourKg, int timeToMixSeconds,
            int mediumId, int productId, OperationOrderInSeries operationOrderInSeries)
        {
            throw new SystemBusyException();
        }

        /// <summary>
        /// Throws SystemBusyException because remotely resuming an unfinished pouring operation is not allowed when system is not idle.
        /// </summary>
        /// <exception cref="SystemBusyException">Thrown always.</exception>
        public override void RemoteCommandResumeUnfinishedOperation()
        {
            throw new SystemBusyException();
        }
    }
}
