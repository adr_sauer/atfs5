using System;
using Microsoft.SPOT;
using ATFS5.src.Screens20x4;
using Microsoft.SPOT.Hardware;

namespace ATFS5.src.Controllers
{
    /// <summary>
    /// Represents system controller which controls the system when main menu is displayed. Analogue to State in State design pattern.
    /// </summary>
    public class MainMenuController : Controller
    {
        private const string _menuItemAddProfile = "DODAJ PROFIL";
        private const string _menuItemDeleteProfile = "USUN PROFIL";
        private const string _menuItemChooseProfile = "WYBIERZ PROFIL";
        private const string _menuItemResumeUnfinishedOperation = "PRZERWANA OPER.";



        MenuChoicePromptScreen _mainMenuScreen;

        /// <summary>
        /// Initializes a new instance of the MainMenuController class using the specified controller context.
        /// </summary>
        /// <param name="context">Controller context.</param>
        public MainMenuController(ControllersContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Gets system status data requested by remote client.
        /// </summary>
        /// <returns>System status data.</returns>
        public override SystemStatusData RemoteCommandGetSystemStatusData()
        {
            SystemStatusData statusData = new SystemStatusData();
            statusData.StatusDate = DateTime.Now;
            statusData.MachineTime = Utility.GetMachineTime();
            statusData.State = SystemStatusData.SystemState.Waiting;
            statusData.WaterFlowKgPerMin = 0;
            statusData.MediumFlowKgPerMin = 0;
            statusData.MediumId = 0;
            statusData.WaterToPourKg = 0;
            statusData.MediumToPourKg = 0;
            statusData.WaterPouredKg = 0;
            statusData.MediumPouredKg = 0;
            statusData.TimeToMixSeconds = 0;
            statusData.TimeMixedSeconds = 0;
            statusData.UnfinishedOperationExists = Context.ContextOperationProgressTrack.TrackExists;
            statusData.UnfinishedOperationData = Context.ContextOperationProgressTrack.ProgressTrack;
            //HACK Safety button always false.
            statusData.SafetyButtonActive = false;
            statusData.CurrentWaterFlowError = FlowError.NoError;
            statusData.CurrentMediumFlowError = FlowError.NoError;
            statusData.LastWaterFlowPauseCauseError = FlowError.NoError;
            statusData.LastMediumFlowPauseCauseError = FlowError.NoError;
            //HACK Overflow sensor always false.
            statusData.EmptyingPumpActive = false;
            statusData.OperationId = 0;
            statusData.SeriesId = 0;
            statusData.ProductId = 0;
            statusData.OperationOrderInSeries = OperationOrderInSeries.Begin;

            return statusData;
        }

        /// <summary>
        /// Executes remote command to set system clock.
        /// </summary>
        /// <param name="newClockSetting">New system clock setting.</param>
        public override void RemoteCommandSetSystemClock(DateTime newClockSetting)
        {
            //TODO Set clock
        }

        /// <summary>
        /// Executes remote command to begin pouring operation.
        /// </summary>
        /// <param name="waterToPourKg">Mass of water in kilograms to be poured.</param>
        /// <param name="mediumToPourKg">Mass of medium in kilograms to be poured.</param>
        /// <param name="timeToMixSeconds">Time to mix in seconds.</param>
        /// <param name="mediumId">ID of medium to be poured.</param>
        /// <param name="productId">ID of product to be produced.</param>
        /// <param name="operationOrderInSeries">Operation position in operations series.</param>
        public override void RemoteCommandPour(int waterToPourKg, int mediumToPourKg, int timeToMixSeconds,
            int mediumId, int productId, OperationOrderInSeries operationOrderInSeries)
        {
            Context.SetController(new PouringController(Context, waterToPourKg, mediumToPourKg, timeToMixSeconds, mediumId, productId, operationOrderInSeries));
        }

        /// <summary>
        /// Executes remote command to resume unfinished operation.
        /// </summary>
        public override void RemoteCommandResumeUnfinishedOperation()
        {
            if (Context.ContextOperationProgressTrack.TrackExists)
                Context.SetController(new PouringController(Context, Context.ContextOperationProgressTrack.ProgressTrack));
            else
                throw new NoUnfinishedOperationException();
        }

        /// <summary>
        /// Starts controller.
        /// </summary>
        public override void Start()
        {
            if (Context.ContextOperationProgressTrack.TrackExists)
                _mainMenuScreen = new MenuChoicePromptScreen(_menuItemAddProfile, _menuItemDeleteProfile,
                    _menuItemChooseProfile, _menuItemResumeUnfinishedOperation);
            else
                _mainMenuScreen = new MenuChoicePromptScreen(_menuItemAddProfile, _menuItemDeleteProfile,
                    _menuItemChooseProfile);
            RegisterKeyboardEventHandlersToScreen(_mainMenuScreen);
            Context.Hardware.Display.Write(_mainMenuScreen.FullScreenString);
            _mainMenuScreen.PositionChanged += MenuPositionChanged;
            Context.Hardware.Keyboard.EnterPressed += ChooseMenuItem;

        }

        /// <summary>
        /// Stops controller
        /// </summary>
        public override void Stop()
        {
            UnRegisterKeyEventHandlers();
            Context.Hardware.Keyboard.EnterPressed -= ChooseMenuItem;

        }

        private void ChooseMenuItem(object o, EventArgs e)
        {

            switch (_mainMenuScreen.SelectedItem)
            {
                case _menuItemAddProfile:
                    Context.SetController(new AddProfileController(Context));
                    break;
                case _menuItemDeleteProfile:
                    Context.SetController(new DeleteProfileController(Context));
                    break;
                case _menuItemChooseProfile:
                    Context.SetController(new OperationSetupController(Context));
                    break;
                case _menuItemResumeUnfinishedOperation:
                    Context.SetController(new PouringController(Context, Context.ContextOperationProgressTrack.ProgressTrack));
                    break;
            }
        }

        private void MenuPositionChanged(object o, EventArgs e)
        {
            Context.Hardware.Display.Write(_mainMenuScreen.FullScreenString);
        }

    }
}
