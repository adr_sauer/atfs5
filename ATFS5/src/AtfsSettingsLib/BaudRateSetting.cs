using System;
using Microsoft.SPOT;

namespace ATFS5.src.AtfsSettingsLib
{
    /// <summary>
    /// Represents serial port baud rate setting.
    /// </summary>
    public partial struct BaudRateSetting
    {
        private int _baudrate;

        /// <summary>
        /// Gets or sets baud rate as an integer code in range from 0 to 4.
        /// </summary>
        public int BaudRateCodeInt
        {
            get
            {
                return BaudRateToBaudRateCode(_baudrate);
            }
            set
            {
                _baudrate = BaudRateCodeToBaudRate(value);
            }
        }

        /// <summary>
        /// Gets or sets baud rate.
        /// </summary>
        public int BaudRate
        {
            get 
            {
                return _baudrate;
            }
            set 
            {
                if(!IsBaudRateSettingValid(value))
                    throw new ArgumentException("Baud rate setting not valid.");
                _baudrate = value; 
            }
        }
                
        /// <summary>
        /// Initializes a new instance of the BaudRateSetting class using the specified baud rate.
        /// </summary>
        /// <param name="baudRate">Baud rate.</param>
        public BaudRateSetting(int baudRate)
        {
            if (!IsBaudRateSettingValid(baudRate))
                throw new ArgumentException("Baud rate setting not valid.");
            _baudrate = baudRate; 
        }

        /// <summary>
        /// Initializes a new instance of the BaudRateSetting class using the specified baud rate string representation.
        /// </summary>
        /// <param name="baudRateSettingString"></param>
        public BaudRateSetting(string baudRateSettingString)
        {
            _baudrate = ParseBaudRateSettingString(baudRateSettingString);
        }

        /// <summary>
        /// Returns a string representation of baud rate setting.
        /// </summary>
        /// <returns>String representation of baud rate setting</returns>
        public override string ToString()
        {
            return _baudrate.ToString();
        }
    }
}
