using System;
using Microsoft.SPOT;
using ATFS5.src.UtilitiesLib.SettingsLib;

namespace ATFS5.src.AtfsSettingsLib
{
    /// <summary>
    /// Represents a provider of program wide settings in Automatic Tank Filling System.
    /// </summary>
    public class AtfsSettings : Settings
    {
        private const string _sirenEnabledSettingKey = "SirenEnabled";
        //private const string _diagnosticsModeSettingKey = "DiagnosticsMode";
        private const string _serialPortBaudRateSettingKey = "SerialPortBaudRate";
        private const string _flowmeterStatusInputIgnoreSettingKey = "FlowmeterStatusIgnore";
        private const string _lcdBacklightEnabledSettingKey = "LCDBacklightEnabled";
        //private const string _modbusResponseDelayMilisecondsSettingKey = "ModbusResponseDelayMiliseconds";
        private const string _liquidLackAlarmDelaySecondsSettingKey = "LiquidLackAlarmDelaySeconds";
        //private const string _diagnostiscOutputIntervalMilisecondsSettingKey = "DiagnosticsIntervalMiliseconds";
        private const string _flowSensorInputIgnoreSettingKey = "FlowSensorInputIgnore";
        private const string _mediumsListServiceCheckIntervalSeconds = "MediumsListServiceCheckIntervalSeconds";
        private const string _productsListServiceCheckIntervalSeconds = "ProductsListServiceCheckIntervalSeconds";
        private const string _lastSeriesIdSettingKey = "LastSeriesId";
        private const string _lastProductIdSettingKey = "LastProductId";

        /// <summary>
        /// Gets or sets whether the siren is enabled.
        /// </summary>
        public bool SirenEnabledSetting
        {
            get
            {
                return ParseBooleanString(Data.ReadSettingValueAsString(_sirenEnabledSettingKey));
            }
            set
            {
                Data.SaveSettingValue(_sirenEnabledSettingKey, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets system's serial port baud rate.
        /// </summary>
        public BaudRateSetting SerialBaudRateSetting
        {
            get
            {
                return new BaudRateSetting(Data.ReadSettingValueAsString(_serialPortBaudRateSettingKey));
            }
            set
            {
                Data.SaveSettingValue(_serialPortBaudRateSettingKey, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets whether the system should ignore errors indicated by flowmeter status error input.
        /// </summary>
        public bool FlowmeterStatusInputIgnoreSetting
        {
            get
            {
                return ParseBooleanString(Data.ReadSettingValueAsString(_flowmeterStatusInputIgnoreSettingKey));
            }
            set
            {
                Data.SaveSettingValue(_flowmeterStatusInputIgnoreSettingKey, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets whether the LCD display backlight is enabled.
        /// </summary>
        public bool LcdBacklightEnabledSetting
        {
            get
            {
                return ParseBooleanString(Data.ReadSettingValueAsString(_lcdBacklightEnabledSettingKey));
            }
            set
            {
                Data.SaveSettingValue(_lcdBacklightEnabledSettingKey, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the liquid lack alarm delay in seconds. If a flow error (for example input indicating no flow from flow sensor)
        /// persists longer than this pouring operation will be paused.
        /// </summary>
        public int LiquidLackAlarmDelaySecondsSetting
        {
            get
            {
                return Int32.Parse(Data.ReadSettingValueAsString(_liquidLackAlarmDelaySecondsSettingKey));
            }
            set
            {
                Data.SaveSettingValue(_liquidLackAlarmDelaySecondsSettingKey, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets whether the system should ignore errors indicated by flow sensors inputs.
        /// </summary>
        public bool FlowSensorInputIgnoreSetting
        {
            get
            {
                return ParseBooleanString(Data.ReadSettingValueAsString(_flowSensorInputIgnoreSettingKey));
            }
            set
            {
                Data.SaveSettingValue(_flowSensorInputIgnoreSettingKey, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets mediums list web service checking interval setting. Interval unit is second.
        /// </summary>
        public int MediumsListServiceCheckIntervalSeconds
        {
            get
            {
                return Int32.Parse(Data.ReadSettingValueAsString(_mediumsListServiceCheckIntervalSeconds));
            }
            set
            {
                Data.SaveSettingValue(_mediumsListServiceCheckIntervalSeconds, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets products list web service checking interval setting. Interval unit is second.
        /// </summary>
        public int ProductsListServiceCheckIntervalSeconds
        {
            get
            {
                return Int32.Parse(Data.ReadSettingValueAsString(_productsListServiceCheckIntervalSeconds));
            }
            set
            {
                Data.SaveSettingValue(_productsListServiceCheckIntervalSeconds, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets last series ID setting.
        /// </summary>
        public uint LastSeriesId
        {
            get
            {
                return UInt32.Parse(Data.ReadSettingValueAsString(_lastSeriesIdSettingKey));
            }
            set
            {
                Data.SaveSettingValue(_lastSeriesIdSettingKey, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets last product ID setting.
        /// </summary>
        public int LastProductId
        {
            get
            {
                return Int32.Parse(Data.ReadSettingValueAsString(_lastProductIdSettingKey));
            }
            set
            {
                Data.SaveSettingValue(_lastProductIdSettingKey, value.ToString());
            }
        }

        /// <summary>
        /// Initializes a new instance of the AtfsSettings class using the specified settings ini file path.
        /// </summary>
        /// <param name="settingsFilePathname">Pathname of the ini file.</param>
        public AtfsSettings(string settingsFilePathname)
            : base(CreateIniFileSettingsData(settingsFilePathname))
        {
        }

        static private SettingsData CreateIniFileSettingsData(string settingsFilePathname)
        {
            var data = new IniFileSettingsData(settingsFilePathname);
            return data;
        }


    }
}
