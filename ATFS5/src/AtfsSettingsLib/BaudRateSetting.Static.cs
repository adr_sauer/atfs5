using System;
using Microsoft.SPOT;

namespace ATFS5.src.AtfsSettingsLib
{
    public partial struct BaudRateSetting
    {
        private const string _9600BaudRateString = "9600";
        private const string _19200BaudRateString = "19200";
        private const string _38400BaudRateString = "38400";
        private const string _57600BaudRateString = "57600";
        private const string _115200BaudRateString = "115200";

        private const int _9600BaudRateCode = 0;
        private const int _19200BaudRateCode = 1;
        private const int _38400BaudRateCode = 2;
        private const int _57600BaudRateCode = 3;
        private const int _115200BaudRateCode = 4;

        private const int _9600BaudRate = 9600;
        private const int _19200BaudRate = 19200;
        private const int _38400BaudRate = 38400;
        private const int _57600BaudRate = 57600;
        private const int _115200BaudRate = 115200;

        /// <summary>
        /// Gets maximum baud rate integer code.
        /// </summary>
        public static int MaxBaudRateCode
        {
            get
            {
                return _115200BaudRateCode;
            }
        }

        /// <summary>
        /// Translates baud rate integer code to an actual baud rate.
        /// </summary>
        /// <param name="baudRateCode">Baud rate integer code.</param>
        /// <returns>
        ///     <list type="bullet">
        ///     <item>
        ///     <description>9600 for 0 code</description>
        ///     </item>
        ///     <item>
        ///     <description>19200 for 1 code</description>
        ///     </item>
        ///     <item>
        ///     <description>38400 for 2 code</description>
        ///     </item>
        ///     <item>
        ///     <description>57600 for 3 code</description>
        ///     </item>
        ///     <item>
        ///     <description>115200 for 4 code</description>
        ///     </item>
        ///     </list>
        /// </returns>
        public static int BaudRateCodeToBaudRate(int baudRateCode)
        {
            int baudRate;
            switch (baudRateCode)
            {
                case _9600BaudRateCode:
                    baudRate = _9600BaudRate;
                    break;
                case _19200BaudRateCode:
                    baudRate = _19200BaudRate;
                    break;
                case _38400BaudRateCode:
                    baudRate = _38400BaudRate;
                    break;
                case _57600BaudRateCode:
                    baudRate = _57600BaudRate;
                    break;
                case _115200BaudRateCode:
                    baudRate = _115200BaudRate;
                    break;
                default:
                    throw new ArgumentException("Wrong baud rate code.");
            }

            return baudRate;
        }

        /// <summary>
        /// Translates an actual baud rate to baud rate integer code.
        /// </summary>
        /// <param name="baudRate">Baud rate</param>
        /// <returns>
        ///     <list type="bullet">
        ///     <item>
        ///     <description>0 code for 9600 baud rate.</description>
        ///     </item>
        ///     <item>
        ///     <description>0 code for 19200 baud rate.</description>
        ///     </item>
        ///     <item>
        ///     <description>0 code for 38400 baud rate.</description>
        ///     </item>
        ///     <item>
        ///     <description>0 code for 57600 baud rate.</description>
        ///     </item>
        ///     <item>
        ///     <description>0 code for 115200 baud rate.</description>
        ///     </item>
        ///     </list>
        /// </returns>
        public static int BaudRateToBaudRateCode(int baudRate)
        {
            int baudRateCode;
            switch (baudRate)
            {
                case _9600BaudRate:
                    baudRateCode = _9600BaudRateCode;
                    break;
                case _19200BaudRate:
                    baudRateCode = _19200BaudRateCode;
                    break;
                case _38400BaudRate:
                    baudRateCode = _38400BaudRateCode;
                    break;
                case _57600BaudRate:
                    baudRateCode = _57600BaudRateCode;
                    break;
                case _115200BaudRate:
                    baudRateCode = _115200BaudRateCode;
                    break;
                default:
                    throw new ArgumentException("Wrong baud rate.");
            }

            return baudRateCode;
        }

        private static int ParseBaudRateSettingString(string baudRateSettingString)
        {
            int baudRate = 0;
            switch (baudRateSettingString)
            {
                case _9600BaudRateString:
                    baudRate = _9600BaudRate;
                    break;
                case _19200BaudRateString:
                    baudRate = _19200BaudRate;
                    break;
                case _38400BaudRateString:
                    baudRate = _38400BaudRate;
                    break;
                case _57600BaudRateString:
                    baudRate = _57600BaudRate;
                    break;
                case _115200BaudRateString:
                    baudRate = _115200BaudRate;
                    break;
                default:
                    throw new ArgumentException("Baud rate setting string parsing error.");
            }
            return baudRate;
        }

        private static bool IsBaudRateSettingValid(int baudRate)
        {
            bool allowed = false;

            switch (baudRate)
            {
                case _9600BaudRate:
                    allowed = true;
                    break;
                case _19200BaudRate:
                    allowed = true;
                    break;
                case _38400BaudRate:
                    allowed = true;
                    break;
                case _57600BaudRate:
                    allowed = true;
                    break;
                case _115200BaudRate:
                    allowed = true;
                    break;
            }
            return allowed;
        }
    }
}
