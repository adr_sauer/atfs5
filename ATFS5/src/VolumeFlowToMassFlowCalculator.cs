using System;
using Microsoft.SPOT;

namespace ATFS5.src
{
    /// <summary>
    /// Represents a general interface for volume flow to mass flow calculators.
    /// </summary>
    public abstract class VolumeFlowToMassFlowCalculator
    {
        /// <summary>
        /// Calculates mass flow using specified volume flow and temperature.
        /// </summary>
        /// <param name="volumeFlow">Liquid volume flow in decimeters per minute.</param>
        /// <param name="temperature">Liquid temperature. Default is 10 degrees Celcius.</param>
        /// <returns>Liquid mass flow in kilograms per minute.</returns>
        public abstract double CalculateMassFlow(double volumeFlow, double temperature = 10);
    }
}
