using System;
using Microsoft.SPOT;
using System.Threading;
using ATFS5.src.UtilitiesLib;
using ATFS5.src.Communication;
using ATFS5.src.AtfsSettingsLib;
using Microsoft.SPOT.Hardware;

namespace ATFS5.src
{
    //NOTE MassPouringController is not used
    /*
    /// <summary>
    /// Represents a controller of mass pouring. 
    /// </summary>    
    public class MassPouringController
    {
        public struct CheckResult
        {
            public CheckResult(FlowError flowError, double flowInKgPerMinute, bool finished)
            {
                _finished = finished;
                _flowError = flowError;
                _flowInKgPerMinute = flowInKgPerMinute; 
            }

            public FlowError _flowError;
            public double _flowInKgPerMinute;
            bool _finished;
        }
        
        private enum MeasurementType { DirectMassFlowValue, WaterMassFromVolumeAndTemperature };
        private MassFlowOverTimeIntegrator _massFlowTimeIntegrator;
        VolumeFlowToMassFlowCalculator _massFlowCalculator;
        
        private int _flowCheckInterval;
        private Action _turnFlowOnAction;
        private Action _turnflowOffAction;
        private IFlowmeter _flowmeter;
        private BinaryInputSimple _flowSensor;
        private MeasurementType _measurementType;
        private bool _pouring = false;
        private bool _paused = false;
        private int _lastMassPouredValue = 0;
        private TimeSpan _alarmStartTimeStamp = TimeSpan.MaxValue;
        private FlowError _currentFlowError;
        private object _lockObject;

        /// <summary>
        /// Initializes a new instance of the MassPouringController class using the specified mass flowmeter, flow sensor, delegates to call to turn flow on and off and flow checking interval.
        /// </summary>
        /// <param name="flowmeter">Mass flowmeter.</param>
        /// <param name="flowSensor">Flow sensor.</param>
        /// <param name="turnFlowOnAction">Delegate to call to turn flow on.</param>
        /// <param name="turnflowOffAction">Delegate to call to turn flow off.</param>
        /// <param name="flowCheckInterval">Interval at which liquid flow will be measured and controller will check for flow errors.</param>
        public MassPouringController(MassFlowmeter flowmeter, BinaryInputSimple flowSensor, Action turnFlowOnAction, Action turnflowOffAction, int flowCheckInterval)
        {
            _massFlowCalculator = null;
            _lockObject = new object();
            _massFlowTimeIntegrator = new MassFlowOverTimeIntegrator();
            _flowCheckInterval = flowCheckInterval;
            _turnFlowOnAction = turnFlowOnAction;
            _turnflowOffAction = turnflowOffAction;
            _flowmeter = flowmeter;            
            _flowSensor = flowSensor;
            _measurementType = MeasurementType.DirectMassFlowValue;
        }

        /// <summary>
        /// Initializes a new instance of the MassPouringController class using the specified volume flowmeter, mass flow calculator, flow sensor, delegates to call to turn flow on and off and flow checking interval.
        /// </summary>
        /// <param name="flowmeter">Volume flowmeter.</param>
        /// <param name="massFlowCalculator">Mass flow calculator.</param>
        /// <param name="flowSensor">Flow sensor.</param>
        /// <param name="turnFlowOnAction">Delegate to call to turn flow on.</param>
        /// <param name="turnflowOffAction">Delegate to call to turn flow off.</param>
        /// <param name="flowCheckInterval">Interval at which liquid flow will be measured and controller will check for flow errors.</param>
        public MassPouringController(VolumeFlowmeter flowmeter, VolumeFlowToMassFlowCalculator massFlowCalculator, BinaryInputSimple flowSensor, Action turnFlowOnAction, Action turnflowOffAction, int flowCheckInterval)
        {
            _massFlowCalculator = massFlowCalculator;
            _lockObject = new object();
            _massFlowTimeIntegrator = new MassFlowOverTimeIntegrator();
            _flowCheckInterval = flowCheckInterval;
            _turnFlowOnAction = turnFlowOnAction;
            _turnflowOffAction = turnflowOffAction;
            _flowmeter = flowmeter;            
            _flowSensor = flowSensor;
            _measurementType = MeasurementType.DirectMassFlowValue;
        }

        /// <summary>
        /// Start pouring given liquid mass.
        /// </summary>
        /// <param name="targetMass">Mass to pour.</param>
        /// <param name="initialValue">Mass which has been poured already.</param>
        public void StartPouring(int targetMass, int initialValue = 0)
        {
            lock (_lockObject)
            {
                if (_pouring)
                    throw new InvalidOperationException("Controller already pouring.");
                _massFlowTimeIntegrator.Start(targetMass, initialValue);
                //_flowErrorAlarmCounter = 0;
                _alarmStartTimeStamp = TimeSpan.MaxValue;
                _turnFlowOnAction();
                _pouring = true;
            }

        }


        /// <summary>
        /// Resume pouring.
        /// </summary>
        public void ResumePouring()
        {
            lock (_lockObject)
            {
                if (!_pouring)
                    throw new InvalidOperationException("Controller not pouring.");
                if (!_paused)
                    throw new InvalidOperationException("Controller not paused.");
                _massFlowTimeIntegrator.Resume();
                //_flowErrorAlarmCounter = 0;
                _alarmStartTimeStamp = TimeSpan.MaxValue;
                _turnFlowOnAction();
                _paused = false;
            }
        }

        /// <summary>
        /// Abort pouring.
        /// </summary>
        public void AbortPouring()
        {
            lock (_lockObject)
            {
                FinishPouring();
            }
        }

        public void PausePouring()
        {
            lock (_lockObject)
            {
                if (!_pouring)
                    throw new InvalidOperationException("Controller not pouring.");
                if (_paused)
                    throw new InvalidOperationException("Controller already paused.");
                _turnflowOffAction();
                _paused = true;
            }
        }

        private void FinishPouring()
        {
            if (!_pouring)
                throw new InvalidOperationException("Controller not pouring.");
            _turnflowOffAction();
            _pouring = false;
        }

        public CheckResult CheckPouringStatus()
        {
            lock (_lockObject)
            {
                if ((!_pouring) || (_paused))
                    throw new InvalidOperationException("Controller not pouring.");
                else
                {
                    UpdateAlarmStartTimeStamp();

                    TimeSpan currentLiquidLackAlarmDelay = new TimeSpan(0, 0, (Utilities.ProgramSettings as AtfsSettings).LiquidLackAlarmDelaySecondsSetting);

                    if (Utility.GetMachineTime() - _alarmStartTimeStamp > currentLiquidLackAlarmDelay)
                    {
                        PausePouring();
                        return new CheckResult(_currentFlowError, 0, false);
                    }
                    else
                    {
                        double massFlow=0;
                        switch (_measurementType)
                        {
                            case MeasurementType.DirectMassFlowValue:
                                massFlow = _flowmeter.FlowPerMinute;
                                if (_massFlowTimeIntegrator.AddFlowMeasurementAndCheckMassSum(massFlow))
                                {
                                    FinishPouring();
                                    return new CheckResult(_currentFlowError, massFlow, true);
                                }
                                else
                                {
                                    return new CheckResult(_currentFlowError, massFlow, false);
                                }
                            case MeasurementType.WaterMassFromVolumeAndTemperature:
                                massFlow =_massFlowCalculator.CalculateMassFlow(_flowmeter.FlowPerMinute);
                                if (_massFlowTimeIntegrator.AddFlowMeasurementAndCheckMassSum(massFlow))
                                {
                                    FinishPouring();
                                    return new CheckResult(_currentFlowError, massFlow, true);
                                }
                                else
                                {
                                    return new CheckResult(_currentFlowError, massFlow, false);
                                }
                            default:
                                throw new ArgumentException("Measurement type not supported.");
                        }
                    }
                }
            }
        }


        
        private void UpdateAlarmStartTimeStamp()
        {
            UpdateFlowError();

            if (_currentFlowError != FlowError.NoError)
            {
                if (_alarmStartTimeStamp == TimeSpan.MaxValue)
                    _alarmStartTimeStamp = Utility.GetMachineTime();
            }
            else
                _alarmStartTimeStamp = TimeSpan.MaxValue;
        }

        private void UpdateFlowError()
        {
            FlowError newFlowError = FlowError.NoError;
            newFlowError = newFlowError.Union(_flowmeter.ErrorStatus);

            if (_flowSensor.IsActive)
                newFlowError |= FlowError.FlowSensor;

            int currentMassPouredValue = _massFlowTimeIntegrator.Mass;
            if (currentMassPouredValue == _lastMassPouredValue)
                newFlowError |= FlowError.NoChangeInMassIntegral;
            _currentFlowError = newFlowError;
        }

        
    }

    */
}
