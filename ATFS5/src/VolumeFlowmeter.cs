using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using ATFS5.src.Communication;

namespace ATFS5.src
{
    /// <summary>
    /// Represents a volume flowmeter.
    /// </summary>
    public class VolumeFlowmeter : IFlowmeter, IDisposable
    {
        /// <summary>
        /// Flow in cubic decimeters per minute.
        /// </summary>
        public double FlowPerMinute
        {
            get 
            {
                lock (_lockObject)
                    return (_currentInput.Value.ValueOn0To1Scale * _flowRange) + _minFlow;
            }
        }

        /// <summary>
        /// Flowmeter error status.
        /// </summary>
        public FlowmeterErrorStatus ErrorStatus
        {
            get 
            {
                lock (_lockObject)
                {
                    FlowmeterErrorStatus errorStatus = FlowmeterErrorStatus.None;
                    if (_currentInput.Value.ErrorStatus == CurrentInputErrorStatus.HighCurrentError)
                        errorStatus |= FlowmeterErrorStatus.CurrentHighError;
                    else if (_currentInput.Value.ErrorStatus == CurrentInputErrorStatus.LowCurrentError)
                        errorStatus |= FlowmeterErrorStatus.CurrentLowError;
                    if (_statusInput.Read())
                        errorStatus |= FlowmeterErrorStatus.StatusInputError;
                    return errorStatus;
                }
            }
        }

        private InputPort _statusInput;
        private object _lockObject;
        private double _flowRange;
        private double _minFlow;
        private double _maxFlow;
        private CurrentInput _currentInput;
        private bool _disposed = false;

        /// <summary>
        /// Initializes a new instance of the VolumeFlowmeter class using the specified current input, minimum flow, maximum flow and status input pin.
        /// </summary>
        /// <param name="currentInput">Current input representing flow value.</param>
        /// <param name="minFlow">Minimum of scale for flow value.</param>
        /// <param name="maxFlow">Maximum of scale for flow value.</param>
        /// <param name="statusInputPin">Flowmeter status input pin.</param>
        public VolumeFlowmeter(CurrentInput currentInput, double minFlow, double maxFlow, Cpu.Pin statusInputPin)
        {
            _lockObject = new object();
            _disposed = false;

            if (maxFlow <= minFlow)
                throw new ArgumentException("Minimum flow must be lower than maximum flow");
            if (currentInput == null)
                throw new ArgumentNullException("currentInput");
            if (minFlow < 0)
                throw new ArgumentException("Minimum flow must not be negative number");
            _currentInput = currentInput;
            _minFlow = minFlow;
            _maxFlow = maxFlow;
            _flowRange = _maxFlow - _minFlow;

            _statusInput = new InputPort(statusInputPin, false, Port.ResistorMode.PullUp);
        }

        /// <summary>
        /// Gets flow in units per minute and flowmeter error status.
        /// </summary>
        /// <param name="flowPerMinute">Output parameter. Flow in cubic decimeters per minute.</param>
        /// <param name="errorStatus">Output parameter. Flowmeter error status.</param>
        public void GetFlowAndErrorStatus(out double flowPerMinute, out FlowmeterErrorStatus errorStatus)
        {
            lock (_lockObject)
            {
                flowPerMinute = (_currentInput.Value.ValueOn0To1Scale * _flowRange) + _minFlow;
                errorStatus = FlowmeterErrorStatus.None;
                if (_currentInput.Value.ErrorStatus == CurrentInputErrorStatus.HighCurrentError)
                    errorStatus |= FlowmeterErrorStatus.CurrentHighError;
                else if (_currentInput.Value.ErrorStatus == CurrentInputErrorStatus.LowCurrentError)
                    errorStatus |= FlowmeterErrorStatus.CurrentLowError;
                if (_statusInput.Read())
                    errorStatus |= FlowmeterErrorStatus.StatusInputError;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes of status input port.
        /// </summary>
        /// <param name="disposing">True if called from Dispose() method. False otherwise.</param>
        protected void Dispose(bool disposing)
        {
            lock (_lockObject)
            {
                if (_disposed)
                    return;
                if (disposing)
                {
                    _statusInput.Dispose();
                }

                _disposed = true;
            }
        }
    }
}
