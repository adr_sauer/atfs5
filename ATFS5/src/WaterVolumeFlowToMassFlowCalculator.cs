using System;
using Microsoft.SPOT;

namespace ATFS5.src
{
    /// <summary>
    /// Represents a calculator of water mass flow.
    /// </summary>
    public class WaterVolumeFlowToMassFlowCalculator : VolumeFlowToMassFlowCalculator
    {
        /// <summary>
        /// Water mass flow is calculated using simplified method. Water density is considered to be equal to 1 kg/dm3 regardless of temperature.
        /// Therefore volume flow is equal to mass flow.
        /// </summary>
        /// <param name="volumeFlow">Water volume flow in decimeters per minute.</param>
        /// <param name="temperature">Water temperature. Default is 10 degrees Celcius.</param>
        /// <returns>Water mass flow in kilograms per minute.</returns>
        public override double CalculateMassFlow(double volumeFlow, double temperature = 10)
        {
            return volumeFlow;
        }
    }
}
