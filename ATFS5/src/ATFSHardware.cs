using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using ATFS5.src.UserInterface;
using ATFS5.src.UtilitiesLib;
using System.Collections;
using ATFS5.src.Communication;
using ATFS5.src.AtfsSettingsLib;

namespace ATFS5.src
{
    /// <summary>
    /// Represents Automatic Tank Filling System hardware.
    /// </summary>
    public sealed class ATFSHardware : IDisposable
    {
        const int waterAdcValueForMinFlow = 5243; 
        const int waterAdcValueForMaxFlow = 26217; 
        const int waterAdcValueForLowCurrentError = 2620; 
        const int waterAdcValueForHighCurrentError = 32745;

        const int mediumAdcValueForMinFlow = 5243;
        const int mediumAdcValueForMaxFlow = 26217;
        const int mediumAdcValueForLowCurrentError = 2620;
        const int mediumAdcValueForHighCurrentError = 32745; 

        const int mediumMinFlow = 0;
        const int mediumMaxFlow = 250;
        const int waterMinFlow = 0;
        const int waterMaxFlow = 150;

        private OutputsController _outputsController;
        private IATFSKeyboard _keyboard;
        private AlphanumericLCDDisplay _display;
        private I2CDevice _i2cBus = null;
        private AnalogToDigitalConverter _waterFlowAdc;
        private AnalogToDigitalConverter _mediumFlowAdc;
        private CurrentInput _waterFlowCurrentInput;
        private CurrentInput _mediumFlowCurrentInput;
        private VolumeFlowmeter _waterFlowmeter;
        private MassFlowmeter _mediumFlowmeter;
        private BinaryInputSimple _waterFlowSensor;
        private BinaryInputSimple _mediumFlowSensor;
        private Stack _disposablesList;
        private bool _initialized = false;


        /// <summary>
        /// Gets system keyboard.
        /// </summary>
        public IATFSKeyboard Keyboard
        {
            get
            {
                return _keyboard;
            }
        }

        /// <summary>
        /// Gets system display.
        /// </summary>
        public AlphanumericLCDDisplay Display
        {
            get
            {
                return _display;
            }
        }

        /// <summary>
        /// Gets water flow sensor.
        /// </summary>
        public BinaryInputSimple WaterFlowSensor
        {
            get
            {
                return _waterFlowSensor;
            }
        }

        /// <summary>
        /// Gets medium flow sensor.
        /// </summary>
        public BinaryInputSimple MediumFlowSensor
        {
            get
            {
                return _mediumFlowSensor;
            }
        }

        /// <summary>
        /// Gets outputs controller.
        /// </summary>
        public OutputsController ATFSOutputsController
        {
            get
            {
                return _outputsController;
            }
        }

        /// <summary>
        /// Gets water flowmeter.
        /// </summary>
        public VolumeFlowmeter WaterFlowmeter
        {
            get
            {
                return _waterFlowmeter;
            }
        }

        /// <summary>
        /// Gets medium flowmeter.
        /// </summary>
        public MassFlowmeter MediumFlowmeter
        {
            get
            {
                return _mediumFlowmeter;
            }
        }

        /// <summary>
        /// Initializes a new instance of the ATFSHardware class.
        /// </summary>
        public ATFSHardware()
        {
            _disposablesList = new Stack();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Initializes system hardware.
        /// </summary>
        public void InitializeHardware()
        {
            if (!_initialized)
            {
                _outputsController = new OutputsController(CpuPins.WaterValvePin, CpuPins.MediumValvePin, CpuPins.WaterPumpPin,
                    CpuPins.MediumPumpPin, CpuPins.MechanicalMixerPin, CpuPins.SirenPin, CpuPins.Enable24VPin, CpuPins.LcdDisplayBacklightPin);
                _disposablesList.Push(_outputsController);
                
                _keyboard = new STD3407Keyboard(CpuPins.KeyboardColumn1Pin, CpuPins.KeyboardColumn2Pin, CpuPins.KeyboardColumn3Pin,
                    CpuPins.KeyboardRow1Pin, CpuPins.KeyboardRow2Pin, CpuPins.KeyboardRow3Pin, CpuPins.KeyboardRow4Pin);
                _disposablesList.Push(_keyboard);
               
                _display = new DEM20486SYH(CpuPins.LcdDisplayRsPin, CpuPins.LcdDisplayRwPin, CpuPins.LcdDisplayEPin,
                    DEM20486SYH.DataBusWidth.W8bit,
                    CpuPins.LcdDisplayDb0Pin, CpuPins.LcdDisplayDb1Pin, CpuPins.LcdDisplayDb2Pin, CpuPins.LcdDisplayDb3Pin,
                    CpuPins.LcdDisplayDb4Pin, CpuPins.LcdDisplayDb5Pin, CpuPins.LcdDisplayDb6Pin, CpuPins.LcdDisplayDb7Pin);
                _disposablesList.Push(_display);

                _waterFlowAdc = new MCP3425(ref _i2cBus, MCP3425.DeviceAddress.A0, 400, MCP3425.Gain.G1,
                    MCP3425.SampleRate.Rate15Sps16Bits, MCP3425.ConversionMode.Continuous);
                _disposablesList.Push(_waterFlowAdc);
                _mediumFlowAdc = new MCP3425(ref _i2cBus, MCP3425.DeviceAddress.A1, 400, MCP3425.Gain.G1,
                    MCP3425.SampleRate.Rate15Sps16Bits, MCP3425.ConversionMode.Continuous);
                _disposablesList.Push(_mediumFlowAdc);
                _waterFlowCurrentInput = new CurrentInput(_waterFlowAdc, waterAdcValueForMinFlow, waterAdcValueForMaxFlow,
                    waterAdcValueForLowCurrentError, waterAdcValueForHighCurrentError);
                _mediumFlowCurrentInput = new CurrentInput(_mediumFlowAdc, mediumAdcValueForMinFlow, mediumAdcValueForMaxFlow,
                    mediumAdcValueForLowCurrentError, mediumAdcValueForHighCurrentError);
                _waterFlowmeter = new VolumeFlowmeter(_waterFlowCurrentInput, waterMinFlow, waterMaxFlow, CpuPins.WaterFlowmeterStatusInputPin);
                _disposablesList.Push(_waterFlowmeter);
                _mediumFlowmeter = new MassFlowmeter(_mediumFlowCurrentInput, mediumMinFlow, mediumMaxFlow, CpuPins.MediumFlowmeterStatusInputPin);
                _disposablesList.Push(_mediumFlowmeter);
                _waterFlowSensor = new BinaryInputSimple(CpuPins.WaterFlowSensorPin, true);
                _disposablesList.Push(_waterFlowSensor);
                _mediumFlowSensor = new BinaryInputSimple(CpuPins.MediumFlowSensorPin, true);
                _disposablesList.Push(_mediumFlowSensor);

                _initialized = true;
            }
        }
                
        private void Dispose(bool disposing)
        {
            if (disposing)
            {                
                foreach (object disposable in _disposablesList)
                {
                    (disposable as IDisposable).Dispose();
                }
            }
        }

    }
}
