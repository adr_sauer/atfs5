using System;
using Microsoft.SPOT;

namespace ATFS5.src
{
    /// <summary>
    /// General interface for flowmeters.
    /// </summary>
    public interface IFlowmeter
    {
        /// <summary>
        /// Flow in units per minute.
        /// </summary>
        double FlowPerMinute { get; }

        /// <summary>
        /// Flowmeter error status.
        /// </summary>
        FlowmeterErrorStatus ErrorStatus { get; }

        /// <summary>
        /// Gets flow in units per minute and flowmeter error status.
        /// </summary>
        /// <param name="flowPerMinute">Output parameter. Flow in units per minute.</param>
        /// <param name="errorStatus">Output parameter. Flowmeter error status.</param>
        void GetFlowAndErrorStatus(out double flowPerMinute, out FlowmeterErrorStatus errorStatus);
        
    }
}
