using System;
using Microsoft.SPOT;

namespace ATFS5.src.PouringProfilesListLib
{
    /// <summary>
    /// Represents an abstract base class for classes representing various file formats containing pouring profiles data.
    /// </summary>
    public abstract class PouringProfilesListFile
    {
        /// <summary>
        /// Pathname to file containing pouring profiles data.
        /// </summary>
        protected string _filePathname;

        /// <summary>
        /// Initializes a new instance of the PouringProfilesListFile class using the specified pouring profiles data file pathname.
        /// </summary>
        /// <param name="filePathname">Pathname to file containing pouring profiles data.</param>
        public PouringProfilesListFile(string filePathname)
        {
            _filePathname = filePathname;
        }
        
        /// <summary>
        /// Reads pouring profiles data from file.
        /// </summary>
        /// <returns>An array of PouringProfile objects.</returns>
        public abstract PouringProfile[] ReadFromFile();

        /// <summary>
        /// Saves pouring profiles data to file.
        /// </summary>
        /// <param name="pouringProfiles">An array of PouringProfile objects to save.</param>
        public abstract void SaveToFile(PouringProfile[] pouringProfiles);
    }
}
