using System;
using Microsoft.SPOT;
using ATFS5.src.Screens20x4;

namespace ATFS5.src.PouringProfilesListLib
{
    /// <summary>
    /// Represents pouring profile data.
    /// Object values should not be modified while the object is contained in a collection that relies on its hash code
    /// </summary>
    [Serializable]
    public class PouringProfile : ICloneable, ITwoLineDisplayable
    {
        private const int _maxNameLength = 20;
        private const int _maxTotalMassKg = 25000;
        private const int _maxMixingTimeInSeconds = 1200;
        private string _name = String.Empty;
        private int _mediumMass = 0;
        private int _waterMass = 0;
        private int _mixingTimeInSeconds = 0;

        /// <summary>
        /// Gets maximum length of pouring profile name equal to 20.
        /// </summary>
        static public int MaxNameLength
        {
            get { return _maxNameLength; }
        }

        /// <summary>
        /// Gets maximum summed mass of water and medium equal to 25000 kg.
        /// </summary>
        static public int MaxTotalMassKg
        {
            get { return _maxTotalMassKg; }
        }

        /// <summary>
        /// Gets maximum mixing time in seconds equal to 1200.
        /// </summary>
        static public int MaxMixingTimeInSeconds
        {
            get { return _maxMixingTimeInSeconds; }
        }

        /// <summary>
        /// Gets maximum mixing time in minutes equal to 20.
        /// </summary>
        static public int MaxMixingTimeInMinutes
        {
            get { return _maxMixingTimeInSeconds / 60; }
        }

        /// <summary>
        /// Gets or sets water mass to be poured.
        /// </summary>
        public int WaterMass
        {
            get
            {
                return _waterMass;
            }
            set
            {
                if (value + _mediumMass >_maxTotalMassKg)
                    throw new ArgumentException("Total mass above allowed.");
                if (value < 0)
                    throw new ArgumentException("Water mass must not be negative");
                _waterMass=value;
            }
        }

        /// <summary>
        /// Gets or sets medium mass to be poured.
        /// </summary>
        public int MediumMass
        {
            get
            {
                return _mediumMass;
            }
            set
            {
                if (value + _waterMass > _maxTotalMassKg)
                    throw new ArgumentException("Total mass above allowed.");
                if (value < 0)
                    throw new ArgumentException("Medium mass must not be negative");
                _mediumMass = value;
            }
        }

        /// <summary>
        /// Gets or sets mixing time in seconds.
        /// </summary>
        public int MixingTimeInSeconds
        {
            get
            {
                return _mixingTimeInSeconds;
            }           
            set
            {
                if (value > _maxMixingTimeInSeconds)
                    throw new ArgumentException("Mixing time above maximum allowed.");
                if (value < 0)
                    throw new ArgumentException("Mixing time must not be negative");
                _mixingTimeInSeconds = value;
            }
            
        }

        /// <summary>
        /// Gets or sets mixing time in minutes. Rounded down.
        /// </summary>
        public int MixingTimeInMinutes
        {
            get
            {
                return _mixingTimeInSeconds / 60;
            }
            set
            {
                value *= 60;
                if (value > _maxMixingTimeInSeconds)
                    throw new ArgumentException("Mixing time above maximum allowed.");
                if (value < 0)
                    throw new ArgumentException("Mixing time must not be negative");
                _mixingTimeInSeconds = value;
            }
        }

        /// <summary>
        /// Gets or sets profile name.
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length > _maxNameLength)
                    throw new ArgumentException("Maximum profile name length is 20");
                if (value == null)
                    throw new ArgumentException("Profile name must not be null.");
                _name = value;
            }
        }

        /// <summary>
        /// Gets medium percent string in XX.X% form where X are calculated medium percent digits.
        /// </summary>
        public string MediumPercentString
        {
            get
            {
                double percent = (double)_mediumMass / (_waterMass + _mediumMass) * 100.0;
                return (percent).ToString("F1") + "%";
            }
        }

        /// <summary>
        /// Gets summed mass of water and medium.
        /// </summary>
        public int TotalMass
        {
            get
            {
                return _waterMass + _mediumMass;
            }
        }

        /// <summary>
        /// Initializes a new instance of the PouringProfile class.
        /// </summary>
        public PouringProfile()
        {
        }

        /// <summary>
        /// Initializes a new instance of the PouringProfile class using the specified medium mass, water mass, mixing time in seconds and profile name.
        /// </summary>
        /// <param name="mediumMass">Medium mass to be poured.</param>
        /// <param name="waterMass">Water mass to be poured.</param>
        /// <param name="mixingTimeSeconds">Mixing time in seconds.</param>
        /// <param name="name">Profile name.</param>
        public PouringProfile(int mediumMass, int waterMass, int mixingTimeSeconds, string name)
        {
           MediumMass = mediumMass;
           WaterMass = waterMass;
           Name = name;
           MixingTimeInSeconds = mixingTimeSeconds;
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns><b>true</b> if the specified object is equal to the current object; otherwise, <b>false</b>.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            PouringProfile profile = obj as PouringProfile;
            if ((object)profile == null)
                return false;

            return ((profile._name == _name) &&
                (profile._waterMass == _waterMass) &&
                (profile._mediumMass == _mediumMass) &&
                (profile._mixingTimeInSeconds == _mixingTimeInSeconds));
        }

        /// <summary>
        /// Gets hash code for the current object.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return _name.GetHashCode() ^ _waterMass.GetHashCode() ^ _mediumMass.GetHashCode() ^ _mixingTimeInSeconds.GetHashCode();
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            return new PouringProfile(_mediumMass, _waterMass, _mixingTimeInSeconds, _name);
        }

        /// <summary>
        /// Returns two strings which will represent product on display. For example in menu which displays one object at a time.
        /// </summary>
        /// <param name="line1">First line in format: "WODA XXXXX KG YY MIN" where XXXXX is water mass to pour and YY is mixing time in minutes. Output parameter.</param>
        /// <param name="line2">First line in format: "MEDIUM XXXXX KG" where XXXXX is medium mass to pour. Output parameter.</param>
        public void GetTwoDisplayableLines(out string line1, out string line2)
        {
            // 5 + 5 + 4 +2 + 4 = 20 characters
            line1 = "WODA: " + _waterMass.ToString() + " KG "  + MixingTimeInMinutes.ToString() + " MIN";
            line2 = "MEDIUM: " + _mediumMass.ToString() + " KG ";
        }
    }
}
