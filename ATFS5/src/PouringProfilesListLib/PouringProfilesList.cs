using System;
using Microsoft.SPOT;
using System.Collections;

namespace ATFS5.src.PouringProfilesListLib
{
    /// <summary>
    /// Represents a list of pouring operations profiles.
    /// Data is stored in a local file.
    /// </summary>
    public class PouringProfilesList
    {
        private ArrayList _pouringProfilesList;
        private PouringProfilesListFile _pouringProfilesListFile;
        private object _lockObject;

        /// <summary>
        /// Gets PouringProfile at given index.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <returns>PouringProfile object which was a given index.</returns>
        public PouringProfile this[int index]
        {
            get
            {
                lock(_lockObject)
                    return _pouringProfilesList[index] as PouringProfile;
            }
        }

        /// <summary>
        /// Initializes a new instance of the PouringProfilesList class using the specified pouring profiles data file pathname.
        /// </summary>
        /// <param name="filePathname">Pathname to file containing pouring profiles data.</param>
        public PouringProfilesList(string filePathname)
        {
            _lockObject = new object();
            _pouringProfilesList = new ArrayList();
            _pouringProfilesListFile = new PouringProfilesListXmlFile(filePathname);
            ReadListFromFile();
        }

        /// <summary>
        /// Removes the first occurrence of a specific PouringProfile from the PouringProfilesList.
        /// </summary>
        /// <param name="profile">The PouringProfile to remove from the PouringProfilesList. The value can be null.</param>
        public void Remove(PouringProfile profile)
        {
            lock (_lockObject)
            {
                _pouringProfilesList.Remove(profile);
                SaveListToFile();
            }
        }

        /// <summary>
        /// Adds an PouringProfile to the end of the PouringProfilesList.
        /// </summary>
        /// <param name="profile">The PouringProfile to be added to the end of the PouringProfilesList. The value can't be null.</param>
        /// <returns>The PouringProfilesList index at which the value has been added.</returns>
        public int Add(PouringProfile profile)
        {
            lock (_lockObject)
            {
                if (profile == null)
                    throw new ArgumentNullException("profile");
                int ret = _pouringProfilesList.Add(profile);
                SaveListToFile();
                return ret;
            }
        }

        /// <summary>
        /// Removes the element at the specified index of the PouringProfilesList.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        public void RemoveAt(int index)
        {
            lock (_lockObject)
            {
                _pouringProfilesList.RemoveAt(index);
                SaveListToFile();
            }
        }

        /// <summary>
        /// Saves pouring profiles data to file.
        /// </summary>
        public void SaveListToFile()
        {
            lock (_lockObject)
                _pouringProfilesListFile.SaveToFile(ToArray());
        }

        /// <summary>
        /// Copies the elements of the PouringProfilesList to a new array.
        /// </summary>
        /// <returns>An array containing copies of the elements of the PouringProfilesList.</returns>
        public PouringProfile[] ToArray()
        {
            lock (_lockObject)
            {
                PouringProfile[] arrayCopy = new PouringProfile[_pouringProfilesList.Count];
                for (int i = 0; i < _pouringProfilesList.Count; i++)
                    arrayCopy[i] = (PouringProfile)((PouringProfile)_pouringProfilesList[i]).Clone();

                return arrayCopy;
            }
        }

        private void ReadListFromFile()
        {
            PouringProfile[] fileData = _pouringProfilesListFile.ReadFromFile();
            _pouringProfilesList.Clear();
            _pouringProfilesList.Capacity = fileData.Length;
            foreach (var profile in fileData)
                _pouringProfilesList.Add(profile);
        }
    }
}
