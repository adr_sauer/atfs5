using System;
using Microsoft.SPOT;
using System.IO;
using System.Xml;
using System.Ext.Xml;
using System.Collections;

namespace ATFS5.src.PouringProfilesListLib
{
    /// <summary>
    /// Represents an XML file containing pouring profiles data.
    /// XML has to conform to XML schema defined in PouringProfilesListData.xsd file.
    /// </summary>
    public class PouringProfilesListXmlFile : PouringProfilesListFile
    {
        private const string _pouringProfilesListTag = "PouringProfiles";
        private const string _pouringProfileNodeTag = "PouringProfile";
        private const string _pouringProfileNameNodeTag = "Name";
        private const string _pouringProfileWaterMassNodeTag = "WaterMass";
        private const string _pouringProfileMediumMassNodeTag = "MediumMass";
        private const string _pouringProfileMixingTimeSecondsNodeTag = "MixingTimeSeconds";

        /// <summary>
        /// Initializes a new instance of the PouringProfilesListXmlFile class using the specified pouring profiles data file pathname.
        /// </summary>
        /// <param name="filePathname">Pathname to XML file containing pouring profiles data</param>
        public PouringProfilesListXmlFile(string filePathname)
            : base(filePathname)
        {
        }

        /// <summary>
        /// Reads pouring profiles data from XML file.
        /// </summary>
        /// <returns>An array of PouringProfile objects.</returns>
        public override PouringProfile[] ReadFromFile()
        {
            var profilesList = new ArrayList();

            using (FileStream file = File.Open(_filePathname, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                var xmlSettings = new XmlReaderSettings();
                xmlSettings.IgnoreComments = true;
                xmlSettings.IgnoreWhitespace = true;

                using (XmlReader reader = XmlReader.Create(file,xmlSettings))
                {
                    if (!reader.ReadToFollowing(_pouringProfilesListTag))
                        throw new XmlException("Cannot find PouringProfiles node.");

                    while (reader.ReadToFollowing(_pouringProfileNodeTag))
                    {
                        PouringProfile profile = new PouringProfile();

                        if (!reader.ReadToFollowing(_pouringProfileNameNodeTag))
                            throw new XmlException("XML malformed. No Name node or wrong placement.");
                        profile.Name = reader.ReadString();

                        if (!reader.ReadToFollowing(_pouringProfileWaterMassNodeTag))
                            throw new XmlException("XML malformed. No WaterMass node or wrong placement.");
                        profile.WaterMass = Int32.Parse(reader.ReadString());

                        if (!reader.ReadToFollowing(_pouringProfileMediumMassNodeTag))
                            throw new XmlException("XML malformed. No MediumMass node or wrong placement.");
                        profile.MediumMass = Int32.Parse(reader.ReadString());

                        if (!reader.ReadToFollowing(_pouringProfileMixingTimeSecondsNodeTag))
                            throw new XmlException("XML malformed. No MixingTimeSeconds node or wrong placement.");
                        profile.MixingTimeInSeconds = Int32.Parse(reader.ReadString());

                        profilesList.Add(profile);
                    }
                }
            }

            return TransformArrayListToPouringProfileArray(profilesList);
        }

        /// <summary>
        /// Saves pouring profiles data to XML file.
        /// </summary>
        /// <param name="pouringProfiles">An array of PouringProfile objects to save.</param>
        public override void SaveToFile(PouringProfile[] pouringProfiles)
        {
            using (FileStream file = File.Open(_filePathname, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (XmlWriter writer = XmlWriter.Create(file))
                {
                    writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                    writer.WriteStartElement(_pouringProfilesListTag); //Start PouringProfiles node (root element)                    

                    foreach (var profile in pouringProfiles)
                    {
                        WritePouringProfileElement(writer, profile);
                    }
                    
                    writer.WriteEndElement(); //End PouringProfiles node
                    writer.Flush();
                }
            }
        }

        private static PouringProfile[] TransformArrayListToPouringProfileArray(ArrayList list)
        {
            PouringProfile[] array = new PouringProfile[list.Count];
            for (int i = 0; i < list.Count; i++)
                array[i] = list[i] as PouringProfile;
            return array;
        }


        private void WritePouringProfileElement(XmlWriter writer, PouringProfile pouringProfile)
        {
            writer.WriteStartElement(_pouringProfileNodeTag); //Start PouringProfile node

            writer.WriteStartElement(_pouringProfileNameNodeTag); //Start Name node
            writer.WriteString(pouringProfile.Name);
            writer.WriteEndElement(); //End Name node

            writer.WriteStartElement(_pouringProfileWaterMassNodeTag); //Start WaterMass node
            writer.WriteString(pouringProfile.WaterMass.ToString());
            writer.WriteEndElement(); //End WaterMass node

            writer.WriteStartElement(_pouringProfileMediumMassNodeTag); //Start MediumMass node
            writer.WriteString(pouringProfile.MediumMass.ToString());
            writer.WriteEndElement(); //End MediumMass node

            writer.WriteStartElement(_pouringProfileMixingTimeSecondsNodeTag); //Start MixingTimeSeconds node
            writer.WriteString(pouringProfile.MixingTimeInSeconds.ToString());
            writer.WriteEndElement(); //End MixingTimeSeconds node

            writer.WriteEndElement(); //End PouringProfile node
        }
    }
}
