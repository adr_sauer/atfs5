using System;
using Microsoft.SPOT;
using NetMf.CommonExtensions;
using ATFS5.src.UtilitiesLib;

namespace ATFS5.src.Screens20x4
{
    /// <summary>
    /// Represents a screen displaying menu items to choose from. Each item is represented by a screen line. 
    /// Cursor indicating which one has been chosen is displayed to the right of the item.
    /// </summary>
    public class MenuChoicePromptScreen : Screen20x4
    {
        

        private int _index = 0;
        private int _screen = 0;
        private int _onScreenIndex = 0;
        private int _maxIndex = 0;
        private string[] _originalItems;
        private string[] _items;

        private object _lockObject;

        /// <summary>
        /// Occurs when cursor position is changed.
        /// </summary>
        public event EventHandler PositionChanged;


        /// <summary>
        /// Gets chosen menu position index.
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                lock (_lockObject)                
                    return _index;
            }
        }

        /// <summary>
        /// Gets string assigned to chosen menu position.
        /// </summary>
        public string SelectedItem
        {
            get
            {
                lock (_lockObject)
                    return _originalItems[_index];
            }
        }

        /// <summary>
        /// Gets maximum position index.
        /// </summary>
        public int MaxIndex
        {
            get
            {
                lock (_lockObject)
                    return _maxIndex;
            }
        }

        /// <summary>
        /// Gets string representing full screen contents.
        /// </summary>
        public override string FullScreenString
        {
            get
            {
                lock (_lockObject)
                {
                    return CalculateScreenContents();
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the MenuChoicePromptScreen class using the specified menu items.
        /// </summary>
        /// <param name="items">Menu items.</param>
        public MenuChoicePromptScreen(params string[] items)
        {
            _lockObject = new object();
            _originalItems = items;
            _items = new string[items.Length];
            _originalItems.CopyTo(_items, 0);
            for (int i = 0; i < _items.Length; i++)
            {
                _items[i] = _items[i].PadRight(_displayWidth - 2);
                _items[i] = _items[i].Substring(0, _displayWidth - 2);
            }

            _maxIndex = _items.Length - 1;
        }

        /// <summary>
        /// Moves cursor to previous menu item.
        /// </summary>
        public override void UpPressedReaction()
        {
            PreviousItem();
        }

        /// <summary>
        /// Moves cursor to next menu item.
        /// </summary>
        public override void DownPressedReaction()
        {
            NextItem();
        }

        private void NextItem()
        {
            lock (_lockObject)
            {
                _index = (_index + 1) % _items.Length;
                _onScreenIndex = _index % _displayLines;
                _screen = _index / _displayLines;
            }
            OnPositionChanged(EventArgs.Empty);
        }

        private void PreviousItem()
        {
            lock (_lockObject)
            {
                _index = ((_index - 1) < 0) ? (_items.Length - 1) : (_index - 1);
                _onScreenIndex = _index % _displayLines;
                _screen = _index / _displayLines;
            }
            OnPositionChanged(EventArgs.Empty);
        }

        private string CalculateScreenContents()
        {
            
            var lines = new string[_displayLines];
            int line1Index = _screen * _displayLines;

            for (int i = 0; i < _displayLines; i++)
            {
                if (line1Index + i <= _maxIndex)
                {
                    if (_onScreenIndex == i)
                        lines[i] = _items[line1Index + i] + " X";
                    else
                        lines[i] = _items[line1Index + i] + "  ";
                }
                else
                {
                    lines[i] = String.Empty.PadRight(_displayWidth);
                }
            }
            
            return lines[0] + "\n" + lines[1] + "\n" + lines[2] + "\n" + lines[3];
        }

        private void OnPositionChanged(EventArgs e)
        {
            EventHandler handler = PositionChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
