using System;
using Microsoft.SPOT;

namespace ATFS5.src.Screens20x4
{
    /// <summary>
    /// Represents an abstract base class for classes representing various display screens which
    /// are meant to be displayed on a 4 line, 20 characters wide, alphanumeric display.
    /// </summary>
    public abstract class Screen20x4
    {
        /// <summary>
        /// Display lines count - 4.
        /// </summary>
        protected const int _displayLines = 4;

        /// <summary>
        /// Display width - 20 characters
        /// </summary>
        protected const int _displayWidth = 20;

        /// <summary>
        /// Display's reaction to pressing up button.
        /// </summary>
        public virtual void UpPressedReaction()
        {
        }

        /// <summary>
        /// Display's reaction to pressing down button.
        /// </summary>
        public virtual void DownPressedReaction()
        {
        }

        /// <summary>
        /// Display's reaction to pressing left button.
        /// </summary>
        public virtual void LeftPressedReaction()
        {
        }

        /// <summary>
        /// Display's reaction to pressing right button.
        /// </summary>
        public virtual void RightPressedReaction()
        {
        }

        /// <summary>
        /// Gets string representing full screen contents.
        /// </summary>
        public abstract string FullScreenString { get; }

    }
}
