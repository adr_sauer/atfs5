using System;
using Microsoft.SPOT;

namespace ATFS5.src.Screens20x4
{
    /// <summary>
    /// Represents an abstract base class for classes representing display screens in which the user can input a value one position at a time.
    /// </summary>
    public abstract class ValuePromptWithPositionLineScreen : Screen20x4
    {
        /// <summary>
        /// Occurs when cursor position has been changed.
        /// </summary>
        public event EventHandler CursorPositionChanged;

        /// <summary>
        /// Occurs when input value has been changed.
        /// </summary>
        public event EventHandler ValueChanged;

        /// <summary>
        /// Gets the current contents of display line containing the input value.
        /// </summary>
        public abstract string ValueLineString { get; }

        /// <summary>
        /// Gets the current contents of display line on which the cursor is displayed.
        /// </summary>
        public abstract string PositionLineString { get; }

        /// <summary>
        /// Gets the number of the display line containing the input value.
        /// </summary>
        public abstract int ValueLineNumber { get; }

        /// <summary>
        /// Gets the number of the display line on which the cursor is displayed.
        /// </summary>
        public abstract int PositionLineNumber { get; }

        /// <summary>
        /// Called when CursorPosition event occurs.
        /// </summary>
        /// <param name="e">EventArgs object. Usually EventArgs.Empty</param>
        protected void OnCursorPositionChanged(EventArgs e)
        {
            EventHandler handler = CursorPositionChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Called when ValueChanged event occurs.
        /// </summary>
        /// <param name="e">EventArgs object. Usually EventArgs.Empty</param>
        protected void OnValueChanged(EventArgs e)
        {
            EventHandler handler = ValueChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
