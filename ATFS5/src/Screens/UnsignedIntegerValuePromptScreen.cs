using System;
using Microsoft.SPOT;

namespace ATFS5.src.Screens20x4
{
    /// <summary>
    /// Represents a screen where the user can input an unsigned integer value. Integer can be modified one digit at a time.
    /// </summary>
    public class UnsignedIntegerValuePromptScreen : ValuePromptWithPositionLineScreen
    {
        const string _screenLine4 = @"ENTER ZATWIERDZA";

        private string _valueSuffix;
        private string _title;

        private int _maxValue;        
        private int _digitsCount;
        private int _minValue;
        private int _value;
        private int[] _digits;
        private int _cursorPosition = 0;        
        private object _lockObject;

        /// <summary>
        /// Gets minimum allowed value of input integer.
        /// </summary>
        public int MinValue
        {
            get
            {                
                return _minValue;
            }
        }

        /// <summary>
        /// Gets maximum allowed value of input integer.
        /// </summary>
        public int MaxValue
        {
            get
            {
                return _maxValue;
            }
        }

        /// <summary>
        /// Gets current value of input integer.
        /// </summary>
        public int Value
        {
            get
            {
                lock (_lockObject)
                {
                    return CalculateValue(_digits);
                }
            }
        }

        /// <summary>
        /// Gets string representing full screen contents.
        /// </summary>
        public override string FullScreenString
        {
            get
            {
                lock (_lockObject)
                {
                    return _title + "\n" + CalculateValueLine() + "\n" + CalculatePositionLine() + "\n" + _screenLine4;
                }
            }
        }

        /// <summary>
        /// Gets the current contents of display line containing the integer the user has input.
        /// </summary>
        public override string ValueLineString
        {
            get
            {
                lock (_lockObject)
                {
                    return  CalculateValueLine();
                }
            }
        }

        /// <summary>
        /// Gets the current contents of display line on which the cursor is displayed.
        /// </summary>
        public override string PositionLineString
        {
            get
            {
                lock (_lockObject)
                {
                    return CalculatePositionLine();
                }
            }
        }

        /// <summary>
        /// Gets the number of the display line containing the integer the user has input.
        /// </summary>
        public override int ValueLineNumber
        {
            get { return 1; }
        }

        /// <summary>
        /// Gets the number of the display line on which the cursor is displayed.
        /// </summary>
        public override int PositionLineNumber
        {
            get { return 2; }
        }

        /// <summary>
        /// Initializes a new instance of the UnsignedIntegerValuePromptScreen class using the specified minimum and maximum allowed values, 
        /// start value, menu title and value suffix.
        /// </summary>
        /// <param name="minValue">Minimum allowed value of input integer.</param>
        /// <param name="maxValue">Maximum allowed value of input integer.</param>
        /// <param name="startValue">Input integer starting value.</param>
        /// <param name="title">>Menu title.</param>
        /// <param name="valueSuffix">Value suffix. Usually value unit.</param>
        public UnsignedIntegerValuePromptScreen(int minValue, int maxValue,int startValue,string title,string valueSuffix)
        {
            if (minValue < 0)
                throw new ArgumentException("");            
            if (minValue >= maxValue)
                throw new ArgumentException("");

            _lockObject = new object();

            _minValue = minValue;
            _maxValue = maxValue;
            _value = startValue;
            _valueSuffix = valueSuffix;
            _title = title;

            _digitsCount=(int)System.Math.Ceiling(System.Math.Log10(maxValue));

            //if log10 is equal to ceiling(log10) than one more digit is needed
            if (System.Math.Ceiling(System.Math.Log10(maxValue)) == System.Math.Log10(maxValue))
                _digitsCount++;
            _digits = new int[_digitsCount];
        }

        /// <summary>
        /// Increases value of chosen digit by one.
        /// </summary>
        public override void UpPressedReaction()
        {
            IncreaseDigit();
        }

        /// <summary>
        /// Decreases value of chosen digit by one.
        /// </summary>
        public override void DownPressedReaction()
        {
            DecreaseDigit();
        }

        /// <summary>
        /// Moves cursor to the left.
        /// </summary>
        public override void LeftPressedReaction()
        {
            MoveCursorToLeft();
        }

        /// <summary>
        /// Moves cursor to the right.
        /// </summary>
        public override void RightPressedReaction()
        {
            MoveCursorToRight();
        }

        private void MoveCursorToLeft()
        {
            lock(_lockObject)
                _cursorPosition = ((_cursorPosition - 1) < 0) ? (_digitsCount - 1) : (_cursorPosition - 1);
            OnCursorPositionChanged(EventArgs.Empty);
        }

        private void MoveCursorToRight()
        {
            lock (_lockObject)
                _cursorPosition = (_cursorPosition + 1) % _digitsCount;
            OnCursorPositionChanged(EventArgs.Empty);
        }

        private void IncreaseDigit()
        {
            bool raiseEvent = false;
            lock (_lockObject)
            {
                int[] newDigits = new int[_digitsCount];
                _digits.CopyTo(newDigits, 0);
                newDigits[_cursorPosition] = (_digits[_cursorPosition] == 9) ? 0 : (_digits[_cursorPosition] + 1);
                int newValue = CalculateValue(newDigits);
                
                if ((newValue >= _minValue) && (newValue <= _maxValue))
                {
                    newDigits.CopyTo(_digits, 0);
                    raiseEvent = true;
                }
            }
            if (raiseEvent)
                OnValueChanged(EventArgs.Empty);
           
        }

        private void DecreaseDigit()
        {          
            bool raiseEvent = false;
            lock (_lockObject)
            {
                int[] newDigits = new int[_digitsCount];
                _digits.CopyTo(newDigits, 0);
                newDigits[_cursorPosition] = (_digits[_cursorPosition] == 0) ? 9 : (_digits[_cursorPosition] - 1);
                int newValue = CalculateValue(newDigits);
                if ((newValue >= _minValue) && (newValue <= _maxValue))
                {
                    newDigits.CopyTo(_digits, 0);
                    raiseEvent = true;
                }
            }
            if (raiseEvent)
                OnValueChanged(EventArgs.Empty);
        }

        static private int CalculateValue(int[] digits)
        {
            int value = 0;
            //Increase position in digits while decreasing power of 10 to multiply by
            for (int i = 0, mult = digits.Length - 1; i < digits.Length - 1; i++, mult--)
                value += digits[i] * (int)System.Math.Pow(10, mult);
            value += digits[digits.Length - 1];
            return value;
        }

        private string CalculatePositionLine()
        {
            char[] positionLineChars = new char[_digitsCount];
            int i = 0;
            for (i = 0; i < _cursorPosition; i++)
                positionLineChars[i]=' ';
            positionLineChars[i] = 'X';
            for (i = _cursorPosition + 1; i < _digitsCount; i++)
                positionLineChars[i] = ' ';
            return new String(positionLineChars);
        }

        private string CalculateValueLine()
        {
            string line = "";
            for (int i = 0; i < _digitsCount; i++)
                line += _digits[i];

            line += _valueSuffix;
            return line;
        }

        
    }
}
