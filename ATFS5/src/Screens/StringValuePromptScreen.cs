using System;
using Microsoft.SPOT;

namespace ATFS5.src.Screens20x4
{


    /// <summary>
    /// Represents a screen where the user can input a string. String can be modified one character at a time.
    /// </summary>
    public class StringValuePromptScreen : ValuePromptWithPositionLineScreen
    {
        private const string _screenLine4 = @"ENTER ZATWIERDZA";

        private static readonly char[] _availableCharacters = { ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
                                                          'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2',
                                                          '3', '4', '5', '6', '7', '8', '9' };

        private int[] _characters;
        private string _title;
        private int _valueLength;
        private int _cursorPosition = 0;

        private object _lockObject;

        /// <summary>
        /// Gets the current value of the string the user has input.
        /// </summary>
        public string Value
        {
            get
            {
                lock (_lockObject)
                {
                    return CalculateValueLine();
                }
            }
        }

        /// <summary>
        /// Gets the current contents of display line containing the string the user has input.
        /// </summary>
        public override string ValueLineString
        {
            get
            {
                lock (_lockObject)
                {
                    return CalculateValueLine();
                }
            }
        }

        /// <summary>
        /// Gets the current contents of display line on which the cursor is displayed.
        /// </summary>
        public override string PositionLineString
        {
            get
            {
                lock (_lockObject)
                {
                    return CalculatePositionLine();
                }
            }
        }

        /// <summary>
        /// Gets string representing full screen contents.
        /// </summary>
        public override string FullScreenString
        {
            get
            {
                lock (_lockObject)
                {
                    return _title + "\n" + CalculateValueLine() + "\n" + CalculatePositionLine() + "\n" + _screenLine4;
                }
            }
        }

        /// <summary>
        /// Gets the number of the display line containing the string the user has input.
        /// </summary>
        public override int ValueLineNumber
        {
            get { return 1; }
        }

        /// <summary>
        /// Gets the number of the display line on which the cursor is displayed.
        /// </summary>
        public override int PositionLineNumber
        {
            get { return 2; }
        }

        /// <summary>
        /// Initializes a new instance of the StringValuePromptScreen class using the specified input string length and menu title.
        /// </summary>
        /// <param name="valueLength">Input string lenght.</param>
        /// <param name="title">Menu title.</param>
        public StringValuePromptScreen(int valueLength, string title)
        {
            if (valueLength < 1)
                throw new ArgumentException("");

            _lockObject = new object();

            _title = title;
            _valueLength = valueLength;
            _characters = new int[valueLength];
        }

        /// <summary>
        /// Changes chosen string character to the next one of available characters.
        /// </summary>
        public override void UpPressedReaction()
        {
            NextCharacter();
        }

        /// <summary>
        /// Changes chosen string character to the previous one of available characters.
        /// </summary>
        public override void DownPressedReaction()
        {
            PreviousCharacter();
        }

        /// <summary>
        /// Moves cursor to the left.
        /// </summary>
        public override void LeftPressedReaction()
        {
            MoveCursorToLeft();
        }

        /// <summary>
        /// Moves cursor to the right.
        /// </summary>
        public override void RightPressedReaction()
        {
            MoveCursorToRight();
        }

        private void MoveCursorToLeft()
        {
            lock (_lockObject)
                _cursorPosition = ((_cursorPosition - 1) < 0) ? (_valueLength - 1) : (_cursorPosition - 1);
            OnCursorPositionChanged(EventArgs.Empty);
        }

        private void MoveCursorToRight()
        {
            lock (_lockObject)
                _cursorPosition = (_cursorPosition + 1) % _valueLength;
            OnCursorPositionChanged(EventArgs.Empty);
        }

        private void NextCharacter()
        {
            lock (_lockObject)
                _characters[_cursorPosition] = (_characters[_cursorPosition] + 1) % _availableCharacters.Length;
            OnValueChanged(EventArgs.Empty);
        }

        private void PreviousCharacter()
        {
            lock (_lockObject)
                _characters[_cursorPosition] = ((_characters[_cursorPosition] - 1) < 0) ? (_availableCharacters.Length - 1) : (_characters[_cursorPosition] - 1);
            OnValueChanged(EventArgs.Empty);
        }               

        private string CalculatePositionLine()
        {
            char[] positionLineChars = new char[_valueLength];
            int i = 0;
            for (i = 0; i < _cursorPosition; i++)
                positionLineChars[i] = ' ';
            positionLineChars[i] = 'X';
            for (i = _cursorPosition + 1; i < _valueLength; i++)
                positionLineChars[i] = ' ';
            return new String(positionLineChars);
        }

        private string CalculateValueLine()
        {
            char[] valueChars = new char[_valueLength];
            for(int i=0;i<_valueLength;i++)
                valueChars[i]=_availableCharacters[_characters[i]];
            return new String(valueChars);
        }
    }
}
