using System;
using System.Collections;
using Microsoft.SPOT;

namespace ATFS5.src.Screens20x4
{
    /// <summary>
    /// Represents a screen displaying menu items to choose from. Each item is represented on one screen. 
    /// Item description is displayed on two lines.
    /// </summary>
    public class TwoLinePerDisplayListChoicePromptScreen : Screen20x4
    {
        private ArrayList _list;

        private string _title;
        private string _confirmationInfo;
        private int _index = 0;
        private object _lockObject;

        /// <summary>
        /// Occurs when chosen menu item index has been changed.
        /// </summary>
        public event EventHandler PositionChanged;

        /// <summary>
        /// Initializes a new instance of the TwoLinePerDisplayListChoicePromptScreen class using the specified items, menu title and confirmation info.
        /// </summary>
        /// <param name="items">Items to choose from.</param>
        /// <param name="title">Menu title.</param>
        /// <param name="confirmationInfo">String describing what happens on item choice confirmation.</param>
        public TwoLinePerDisplayListChoicePromptScreen(ITwoLineDisplayable[] items,string title, string confirmationInfo)
        {
            _lockObject = new object();
            _list = new ArrayList();
            _title = title;
            _confirmationInfo = confirmationInfo;
            foreach (ITwoLineDisplayable item in items)
                _list.Add(item);
        }

        /// <summary>
        /// Gets chosen item index.
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                lock (_lockObject)
                    return _index;
            }
        }

        /// <summary>
        /// Gets selected item.
        /// </summary>
        public ITwoLineDisplayable SelectedItem
        {

            get
            {
                lock (_lockObject)
                    return _list[_index] as ITwoLineDisplayable;
            }
        }

        /// <summary>
        /// Gets maximum position index.
        /// </summary>
        public int MaxIndex
        {
            get
            {
                lock (_lockObject)
                    return _list.Count;
            }
        }

        /// <summary>
        /// Gets string representing full screen contents.
        /// </summary>
        public override string FullScreenString
        {
            get 
            {
                lock (_lockObject)
                {
                    string descriptionLine1, descriptionLine2;
                    (_list[_index] as ITwoLineDisplayable).GetTwoDisplayableLines(out descriptionLine1, out descriptionLine2);

                    return _title + "\n" + descriptionLine1 + "\n" + descriptionLine2 + "\n" + _confirmationInfo;
                }
            }
        }

        /// <summary>
        /// Moves to the previous item.
        /// </summary>
        public override void UpPressedReaction()
        {
            PreviousItem();
        }

        /// <summary>
        /// Moves to the next item.
        /// </summary>
        public override void DownPressedReaction()
        {
            NextItem();
        }

        private void NextItem()
        {
            lock (_lockObject)
            {
                _index = (_index + 1) % _list.Count;
            }
            OnPositionChanged(EventArgs.Empty);
        }

        private void PreviousItem()
        {
            lock (_lockObject)
            {
                _index = ((_index - 1) < 0) ? (_list.Count - 1) : (_index - 1);
            }
            OnPositionChanged(EventArgs.Empty);
        }

        private void OnPositionChanged(EventArgs e)
        {
            EventHandler handler = PositionChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }

    /// <summary>
    /// Exposes a method that gets a two line display representation of an object.
    /// </summary>
    public interface ITwoLineDisplayable
    {
        /// <summary>
        /// Returns two strings which will represent object on display. For example in menu which displays one object at a time.
        /// </summary>
        /// <param name="line1">First line. Output parameter.</param>
        /// <param name="line2">Second line. Output parameter.</param>
        void GetTwoDisplayableLines(out string line1, out string line2);
    }
}
