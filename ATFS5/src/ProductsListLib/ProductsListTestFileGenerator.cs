using System;
using System.IO;
using Microsoft.SPOT;

namespace ATFS5.src.ProductsListLib
{
    static class ProductsListTestFileGenerator
    {
        public static void Generate()
        {
            Product[] products = { 
                                     new Product { Name="Plyn do spryskiwaczy -22 metanol",Id=1 }, 
                                     new Product { Name="Plyn do spryskiwaczy -22 etanol",Id=2 }, 
                                     new Product { Name="Plyn do chlodnic",Id=3 }, 
                                 };

            string pathname=Resources.GetString(Resources.StringResources.ProductsListFilePathname);

            byte[] productsByteData = Reflection.Serialize(products, typeof(Product[]));

            using (var productsFileStream = File.Open(pathname, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                productsFileStream.Write(productsByteData, 0, productsByteData.Length);
                productsFileStream.Flush();
            }

        }
    }
}
