using System;
using Microsoft.SPOT;
using ATFS5.src.Screens20x4;

namespace ATFS5.src.ProductsListLib
{
    /// <summary>
    /// Represents product data.
    /// </summary>
    [Serializable]
    public class Product : ICloneable, ITwoLineDisplayable
    {
        private const int _maxNameLength = 40;
        private string _name = String.Empty;
        private int _id = 0;

        /// <summary>
        /// Maximum length of product name equal to 40.
        /// </summary>
        static public int MaxNameLength
        {

            get { return _maxNameLength; }
        }

        /// <summary>
        /// Initializes a new instance of the Product class.
        /// </summary>
        public Product()
        {
        }

        /// <summary>
        ///  Initializes a new instance of the Product class using the specified id and name.
        /// </summary>
        /// <param name="id">Product ID. Must be a positive integer.</param>
        /// <param name="name">Product name. Must not be null and up to 40 characters long.</param>
        /// <exception cref="ArgumentException">ID not a positive integer, medium name null or longer than 40 characters.</exception>
        public Product(int id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <summary>
        /// Gets or sets product name. Must not be null and up to 40 characters long.
        /// </summary>
        /// <exception cref="ArgumentException">Product name null or longer than 40 characters.</exception>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length > _maxNameLength)
                    throw new ArgumentException("Maximum product name length is 20");
                if (value == null)
                    throw new ArgumentException("Product name must not be null.");
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets product ID. Must be a positive integer.
        /// </summary>
        /// <exception cref="ArgumentException">ID not a positive integer.</exception>        
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("Product ID must be a positive integer.");
                _id = value;
            }
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            return new Product(_id, _name);
        }

        /// <summary>
        /// Returns two strings which will represent product on display. For example in menu which displays one object at a time.
        /// </summary>
        /// <param name="line1">First line - first 20 characters of product name. Output parameter.</param>
        /// <param name="line2">Second line - next 20 characters of product name. Output parameter.</param>
        public void GetTwoDisplayableLines(out string line1, out string line2)
        {
            if (_name.Length > 20)
            {
                line1 = _name.Substring(0, 20);
                line2 = _name.Substring(20, _name.Length - 20);
            }
            else
            {
                line1 = _name;
                line2 = String.Empty;
            }
        }
    }
}
