using System;
using System.IO;
using Microsoft.SPOT;

namespace ATFS5.src.ProductsListLib
{
    /// <summary>
    /// Represents a NET Micro Framework binary serialization file containing backup products data.
    /// </summary>
    public class ProductsListNETMFBinarySerializedFile : ProductsListFile
    {
        /// <summary>
        /// Initializes a new instance of the ProductsListNETMFBinarySerializedFile class using the specified products data file pathname.
        /// </summary>
        /// <param name="filePathname"></param>
        public ProductsListNETMFBinarySerializedFile(string filePathname)
            : base(filePathname)
        {
        }

        /// <summary>
        /// Reads products data from NET Micro Framework binary serialization file.
        /// </summary>
        /// <returns>An array of Product objects.</returns>
        public override Product[] ReadFromFile()
        {
            byte[] productsByteData = null;
            using (var productsFileStream = File.Open(_filePathname, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                if (productsFileStream.Length > Int32.MaxValue)
                    throw new IOException("Mediums list file too long.");
                productsByteData = new byte[(int)productsFileStream.Length];
                productsFileStream.Read(productsByteData, 0, productsByteData.Length);
            }
            return Reflection.Deserialize(productsByteData, typeof(Product[])) as Product[];
        }

        /// <summary>
        /// Saves products data to NET Micro Framework binary serialization file.
        /// </summary>
        /// <param name="mediums">An array of Product objects to save.</param>
        public override void SaveToFile(Product[] mediums)
        {
            byte[] productsByteData = Reflection.Serialize(mediums, typeof(Product[]));

            using (var productsFileStream = File.Open(_filePathname, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                productsFileStream.Write(productsByteData, 0, productsByteData.Length);
                productsFileStream.Flush();
            }
        }
    }
}
