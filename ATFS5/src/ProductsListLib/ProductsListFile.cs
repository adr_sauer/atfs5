using System;
using Microsoft.SPOT;

namespace ATFS5.src.ProductsListLib
{    
    /// <summary>
    /// Represents an abstract base class for classes representing various file formats containing backup products data.
    /// </summary>
    public abstract class ProductsListFile
    {
        /// <summary>
        /// Pathname to file containing backup products data.
        /// </summary>
        protected string _filePathname;

        /// <summary>
        /// Initializes a new instance of the ProductsListFile class using the specified products data file pathname.
        /// </summary>
        /// <param name="filePathname">Pathname to file containing backup products data.</param>
        public ProductsListFile(string filePathname)
        {
            _filePathname = filePathname;
        }

        /// <summary>
        /// Reads products data from file.
        /// </summary>
        /// <returns>An array of Product objects.</returns>
        public abstract Product[] ReadFromFile();

        /// <summary>
        /// Saves products data to file.
        /// </summary>
        /// <param name="mediums">An array of Product objects to save.</param>
        public abstract void SaveToFile(Product[] mediums);

    }
}
