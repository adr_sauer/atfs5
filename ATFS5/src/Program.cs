using System;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using ATFS5.src.UtilitiesLib;
using ATFS5.src.UtilitiesLib.SettingsLib;
using ATFS5.src.AtfsSettingsLib;
using ATFS5.src.Controllers;
using ATFS5.src.Modbus;


namespace ATFS5.src
{
#if UNIT_TESTING
    public class Program : MFUnit.TestApplication
    {
        /// <summary>
        /// Gets system controllers context.
        /// </summary>
        public static ControllersContext ProgramControllersContext
        {
            get
            {
                return null;
            }
        }
        public static void Main()
        {
            new Program().Run();
        }
    }

#else
    /// <summary>
    /// Main program class.
    /// </summary>
    public class Program
    {
        private const int _startupWait = 500; //times 10 ms
        private const int _watchdogTimeoutInMiliseconds = 5000;
        private const int _maximumLogSizeInMegabytes = 2;

        private static bool _emulated = true;
        private static string _programVersion = "";
        private static ATFSHardware _hardware;
        private static ControllersContext _controllersContext = null;
        private static StopFlag _programStopFlag;


        /// <summary>
        /// <b>True</b> if program is being run on an emulator. <b>False</b> otherwise.
        /// </summary>
        public static bool Emulated
        {
            get { return _emulated; }
        }

        /// <summary>
        /// Gets system controllers context.
        /// </summary>
        public static ControllersContext ProgramControllersContext
        {
            get
            {
                return _controllersContext;
            }
        }


        /// <summary>
        /// Program entry point. 
        /// </summary>
        public static void Main()
        {
            Debug.EnableGCMessages(false);
            _programStopFlag = new StopFlag();
            _programVersion = GetProgramVersionNumberString();
#if DEVICE_DEPLOY
            emulated = false;
            StartupDeploymentWait();
#elif EMUL_DEPLOY
            _emulated = true;
#endif
            DateTime lastKnownDate = new DateTime(2015, 03, 18, 9, 31, 0);


            //_ledSignaller = LedSignaller.ConstructSingleLEDSignaller(CpuPins.BoardLedPin);
            //SDCard.OpenCard();
            //_logger = FileLogger.ConstructSingleLogger(_ledSignaller, _maximumLogSizeInMegabytes, Resources.GetString(Resources.StringResources.LogFilePath));
            Utilities.InitializeLedSignaller(LedSignaller.ConstructSingleLEDSignaller(CpuPins.BoardLedPin));
            Utilities.InitializeSDCard();
            Utilities.InitializeLogger(FileLogger.ConstructSingleLogger(_maximumLogSizeInMegabytes, Resources.GetString(Resources.StringResources.LogFilePath)));
            Utilities.InitializeUnforeseenExceptionHandler(HandleUnforeseenException);


            //Test();


            using (_hardware = new ATFSHardware())
            {
                try
                {
                    Utilities.InitializeSettings(new AtfsSettings(Resources.GetString(Resources.StringResources.SettingsFilePathname)));
                    _hardware.InitializeHardware();
                    _controllersContext = new ControllersContext(_hardware);

                    ModbusServer modbusServer = new ModbusUDPServer(null);
                    modbusServer.StartModbusServer();

                    _hardware.Keyboard.F2Pressed += (o, e) =>
                    {
                        try
                        {
                            _controllersContext.RemoteCommandPour(200, 200, 0, 0, 0, OperationOrderInSeries.Begin);
                        }
                        catch (Exception ex)
                        {
                            Debug.Print(ex.GetType().FullName);
                        }
                    };

                    _hardware.Keyboard.F3Pressed += (o, e) =>
                    {
                        try
                        {
                            SystemStatusData data= _controllersContext.RemoteCommandGetSystemStatusData();
                            Debug.Print(data.ToString());
                        }
                        catch (Exception ex)
                        {
                            Debug.Print(ex.GetType().FullName);
                        }
                    };

                    _hardware.Keyboard.F4Pressed += (o, e) =>
                    {
                        try
                        {
                            _controllersContext.RemoteCommandCancelPouringOperation();
                        }
                        catch (Exception ex)
                        {
                            Debug.Print(ex.GetType().FullName);
                        }
                    };

                    KeepProgramAlive();
                }
                catch (Exception e)
                {
                    HandleUnforeseenExceptionPrivate(e);
                }
            }

            Utilities.ProgramLogger.Dispose();
            Utilities.ProgramLedSignaller.Dispose();
        }

        /// <summary>
        /// Sets program stop flag.
        /// </summary>
        public static void StopProgram()
        {
            StopProgramPrivate();
        }

        /// <summary>
        /// Handles unforeseen exceptions that occured during program execution.
        /// </summary>
        /// <param name="e"></param>
        public static void HandleUnforeseenException(Exception e)
        {
            HandleUnforeseenExceptionPrivate(e);
        }

        private static void HandleUnforeseenExceptionPrivate(Exception e)
        {
            Utilities.ProgramLogger.LogException(e);
#if DEVICE_DEPLOY
            PowerState.RebootDevice(false);
#elif EMUL_DEPLOY
            StopProgramPrivate();
#endif
        }

        private static void StopProgramPrivate()
        {
            _programStopFlag.Stop();

        }

        private static void KeepProgramAlive()
        {
            bool a = true;
            while (_programStopFlag.KeepWorking)
            {
                a = !a;
                Thread.Sleep(100);
            }
        }

        private static void StartupDeploymentWait()
        {
            bool ledState = false;
            using (OutputPort led = new OutputPort(CpuPins.BoardLedPin, ledState))
            {
                for (int i = 0; i < _startupWait; i++)
                {
                    Thread.Sleep(10);
                    if (i % 5 == 0)
                    {
                        ledState = !ledState;
                        led.Write(ledState);
                    }
                }
            }
        }

        private static string GetProgramVersionNumberString()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        private static void Test()
        {
            //byte[] tb=Windows1250Encoding.Windows1250.GetBytes("Hello World! �󹜳����");

            //Debug.Print(new String(Windows1250Encoding.Windows1250.GetChars(tb)));
            Utilities.ProgramLogger.LogError("test error");
            Utilities.ProgramLogger.LogException(new Exception("test exception"));
            Utilities.ProgramLogger.LogInformation("test info");
            //_ledSignaller.BlinkLED(4, true);
        }
    }
#endif
}
