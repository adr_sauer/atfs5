using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace ATFS5.src
{
    /// <summary>
    /// Represents a calculator of mass flow over time integral checking if the result (mass poured during the measuring period) is higher or equal than target mass.
    /// Integral value is estimated using rectangle rule.
    /// </summary>
    public class MassFlowOverTimeIntegrator
    {

        private TimeSpan _lastMeasurementTimeStamp;
        private double _massFlowTimeIntegral;
        private double _targetMass;
        private object _lockObject;

        /// <summary>
        /// Gets the current result of integration (mass poured). Rounded down.
        /// </summary>
        public int Mass
        {
            get
            {
                lock (_lockObject)
                    return (int)_massFlowTimeIntegral;
            }
        }

        /// <summary>
        /// Initializes a new instance of the MassFlowOverTimeIntegrator class.
        /// </summary>
        public MassFlowOverTimeIntegrator()
        {
            _lockObject = new object();
            _lastMeasurementTimeStamp = TimeSpan.Zero;
            _massFlowTimeIntegral = 0;
            _targetMass = 0;
        }

        /// <summary>
        /// Starts calculation of mass flow over time integral.
        /// </summary>
        /// <param name="targetMass">Target integral result.</param>
        /// <param name="initialValue">Initial integral value.</param>
        public void Start(int targetMass, int initialValue = 0)
        {
            lock (_lockObject)
            {
                _lastMeasurementTimeStamp = Utility.GetMachineTime();
                _massFlowTimeIntegral = initialValue;
                _targetMass = targetMass;
            }
        }

        /// <summary>
        /// Resumes calculation of mass flow over time integral.
        /// </summary>
        public void Resume()
        {
            lock (_lockObject)
            {
                _lastMeasurementTimeStamp = Utility.GetMachineTime();
            }
        }

        /// <summary>
        /// Adds another mass flow value and checks if calculated integral is higher or equal to target mass.
        /// </summary>
        /// <param name="flowInKilogramsPerMinute">Mass flow in kilograms per minute.</param>
        /// <returns><b>True</b> if calculated integral is higher or equal to target mass. <b>False</b> otherwise.</returns>
        public bool AddFlowMeasurementAndCheckMassSum(double flowInKilogramsPerMinute)
        {
            lock (_lockObject)
            {
                TimeSpan currentMeasurementTimeStamp = Utility.GetMachineTime();
                TimeSpan timeSpanBetweenMeasurements = currentMeasurementTimeStamp - _lastMeasurementTimeStamp;
                _lastMeasurementTimeStamp = currentMeasurementTimeStamp;
                double minutesBetweenMeasurements = ((double)timeSpanBetweenMeasurements.Seconds / 60) + ((double)timeSpanBetweenMeasurements.Milliseconds / 60000);
                _massFlowTimeIntegral += flowInKilogramsPerMinute * minutesBetweenMeasurements;
                if (_massFlowTimeIntegral >= _targetMass)
                    return true;
                else
                    return false;
            }
        }

    }
}
